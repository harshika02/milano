# standard imports


# custom imports



# settings
NUMPIXELS       = 100
MINSOLUTIONS    = 1
MAXSOLUTIONS    = 4
SOLUTIONFOLDER  = "results/"
SESSION_ROOTFOLDER  = SOLUTIONFOLDER + "session_%d/"

## Enables the logging to file
LOG_ENABLE      = False
## Name of the file where to log
LOG_FILENAME    = "log_%d.txt"
## Enables the debugging logging
DEBUG_ENABLE    = False
## Flag that enables the print to console
PRINT_ENABLE    = False


## List of possible colors for agents
AGENTCOLORS     = [
    "#ffffff",
    "#ffcccc",
    "#ffb3b3",
    "#ff8080",
    "#ff4d4d",
    "#ff1a1a",
    "#e60000",
    "#b30000",
    "#4d0000",
    "#000000",
]

## Solution space pickle file
SOLUTION_SPACE_PICKLE_FILE  = "solution_space_5peaks_similar_V2.pkl"
SOLUTION_SPACE_PICKLE_FILE_EXP = "solution_space_5peaks_similar_V2.pkl"