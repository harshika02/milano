# standard imports
import matplotlib.pyplot as plt
from matplotlib.patches import Circle, Wedge

# custom imports
from abm_simulation.constants import *
from abm_simulation.utils import createFolder, plotArrow
from abm_simulation.utils import _print


## Class that implements the activity of Idea Generation
class IdeaGenerationActivity():
    agents          = []
    solutionSpace   = None
    numSession      = 0
    currentPath     = ""
    numSteps        = 20    # number of steps
    ## Constructor method
    # @param agents List of agents involved in the activity
    # @param solutionSpace number of the current session number
    # @param numSession current session number
    def __init__(self, agents, solutionSpace, numSession):
        # storing the agents
        self.agents             = agents
        # storing the current session
        self.numSession         = numSession
        # storing solution space
        self.solutionSpace      = solutionSpace
        # storing the limit
        self.limit              = self.solutionSpace.shape[0]
        # creating solution folder
        createFolder(SOLUTIONFOLDER)
    #
    ## Method for running Idea Generation steps
    def run(self):
        # checking if the folder exists
        self.currentPath  = SESSION_ROOTFOLDER % (self.numSession)
        # creating current path folder
        createFolder(self.currentPath)
        # Running the IG activity
        for i in range(self.numSteps):
            self.printStepInfo(i)
            # move agents
            for agent in self.agents:
                agent.run()
            # Plotting the agents positions
            self.plotAgentsPositions(i)
    #
    ## Method called before the beginning of an Idea Generation
    def onStart(self):
        # printing beginning string
        _print("###############################################")
        _print("[Session %d] Starting Idea Generation Activity"%(self.numSession))
    #
    ## Method after an Idea Generation activity is over
    def onEnd(self):
        # printing beginning string
        _print("[Session %d] Ending Idea Generation Activity"%(self.numSession))
        _print("###############################################\n")
        # storing the solution value in each agent
        for agent in self.agents:
            self.computeSolutionValues()
            # updating influence values
            agent.updateInfluencerValues()
        # plotting agents' itineraries
        self.computeAgentsItineraries()
    #
    ## Method for computing value of the proposed solution
    def computeSolutionValues(self):
        for agent in self.agents:
            # getting the position of the agents
            pos             = agent.getPosition()
            # getting the value of that position
            solutionValue   = self.solutionSpace[pos[0],pos[1]]
            # storing the solution value in agent
            agent.storeSolutionValue(solutionValue)
    #
    ###############################################
    # DATA EXPORT METHODS
    ## Method that prints the step status
    # @param stepNum Number of the current step
    def printStepInfo(self, stepNum):
        _print("[Step %d]" % (stepNum))
        # print agent by agent
        for agent in self.agents:
            baseString  = "Position of Agent %s: %s"
            printString = baseString % (str(agent.name), str(agent.getPosition()))
            _print(printString)
        # printing new line
        _print("")
    #
    ## Method that plots the agents position in given step
    # @param stepNum Number of the current steps
    def plotAgentsPositions(self, stepNum):
        # plotting solution space
        fig,ax  = plt.subplots(1)
        ax.matshow(self.solutionSpace)
        # plotting agents
        for j in range(len(self.agents)):
            agent = self.agents[j]
            patch=Circle(agent.getPosition(),radius=1.0, color=AGENTCOLORS[j])
            ax.add_patch(patch)
        imageName= self.currentPath + "step_%d.png" % (stepNum)
        plt.savefig(imageName)
        plt.close("all")
    #
    ## Method that plots the itinerary of agents during an Idea Generation Activity
    def computeAgentsItineraries(self):
        # plotting trial iterations for each agent
        for agent in self.agents:
            # clearing the plot
            plt.close("all")
            # plotting the solution space
            fig,ax  = plt.subplots(1)
            ax.matshow(self.solutionSpace, cmap=plt.get_cmap('viridis'))
            ####################################
            # PLOTTING SUCCESSES
            for recalledSuccess in agent.recalledSuccesses[self.numSession]:
                plt.plot(recalledSuccess.position[0], recalledSuccess.position[1], color="dimgrey", marker="X")
                #_print("Recalled Success value",recalledSuccess.value)
                #_print("position of recalled success",self.numSession,"x=",recalledSuccess.position[0],"y=",recalledSuccess.position[1])
            ####################################
            # PLOTTING FAILURES
            for recalledFailure in agent.recalledFailures[self.numSession]:
                # plotting the centre of the failure
                plt.plot(recalledFailure.position[0], recalledFailure.position[1], color="sandybrown", marker="X")
                #_print("Recalled Failure value",recalledFailure.value)
                #_print("position of recalled failure",self.numSession,"x=",recalledFailure.position[0],"y=",recalledFailure.position[1])
                # creating patch
                patch   = Circle(
                    recalledFailure.position,
                    radius  = agent.rCircle, 
                    color   = "sandybrown",
                    fill    = False
                )
                # adding patch 
                ax.add_patch(patch)
            ####################################
            # PLOTTING THE AGENT PATH
            # plotting the first point
            k       = 0
            plt.plot(agent.historyX[0],agent.historyY[0], 'ro')
            # plotting arrows
            for k in range(self.numSteps-1):
                # checking if it's the last point
                if k == self.numSteps-2:
                    lastPoint   = True
                else:
                    lastPoint   =  False
                    # creating the patch
                    patch=Circle(
                        (agent.historyX[k],agent.historyY[k]),
                        radius  = 0.5, 
                        color   = AGENTCOLORS[6]
                    )
                    # adding the patch
                    ax.add_patch(patch)
                # plotting the arrow in case it's the last point
                plotArrow(agent.historyX, agent.historyY, k, lastPoint)
                #print ("agent history x and y",agent.historyX,agent.historyY)
            # setting the limits to axes
            ax.set_xlim(0, self.limit-1)
            ax.set_ylim(self.limit-1, 0)
            # creating results folder
            itineraryFolder     = self.currentPath + "itineraries/"
            createFolder(itineraryFolder)
            # generating the image name
            imageName   = itineraryFolder + "agent_%s.png" % (agent.name) 
            # storing the image
            plt.savefig(imageName)
    #





if __name__ == "__main__":
    # importing solution space
    from solution_space import createSolutionSpace
    from agents.agent import Agent
    # creating solution space
    solutionSpace = createSolutionSpace()
    # setting the current session number
    currentSession = 0
    # creating a list of agents
    agents  = []
    for i in range(3):  
        # creating the agent
        agent   = Agent(            \
            name="Agent #"+str(i),  \
            initPosition=(50, 50), \
            limit=NUMPIXELS,        \
            maxNumSessions=3
        )
        # init the agent
        agent.onSessionStart(currentSession)
        # appending to list
        agents.append(agent)
    # create the activity
    iga = IdeaGenerationActivity(agents, solutionSpace, currentSession)
    # preparing activity 
    iga.onStart()
    # running activity
    iga.run()
    # closing activity and wrap up
    iga.onEnd()
