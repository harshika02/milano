# standard imports
import numpy as np
from sklearn.cluster import KMeans
import random
import os
# custom imports

# model imports
from models.base import databaseSession
from models.agent import Agent as ModelAgent, AgentCharacteristics as ModelAgentCharacteristics
from models.success import Success as ModelSuccess
from models.project import Project as ModelProject
from models.session import Session as ModelSession
from models.steps import IdeaGenerationStep as ModelIdeaGenerationStep
from models.selection import IdeaSelection as ModelIdeaSelection
from models.failure import Failure as ModelFailure

## Class that implements the activity of Idea Selection
class IdeaSelectionActivity():
    ## Constructor method
    # @param agents List of agents involved in the activity
    def __init__(self,agents,sessionOrmObject,solutionSpace):
        # storing the agents
        self.agents         = agents
        # storing the current session
        self.numSession     = sessionOrmObject.number
        # storing the orm object of the session
        self.sessionOrmObject = sessionOrmObject
        # storing the solution space
        self.solutionSpace      = solutionSpace
    # 
    ## Method for running Idea Selection
    def run(self):
        # selecting the agents
        self.selectedAgents    = self.selectAgents()
        # grouping agents
        self.agentsGroups    = self.clusterAgents()
        # compute proposed solutions
        self.computeProposedSolution()
        # computing agreement
        self.maxVoteAgents       = self.computeAgreement()
    #
    ## Method called before the beginning of an Idea Selection
    def onStart(self):
        for agent in self.agents:
            agent.setProposedSolution(agent.position)
    #
    ## Method after an Idea Selection activity is over
    def onEnd(self,agents):
        # updating self efficacy of selected agents
        self.updatingSelfEfficacy()
        # updating accepted solution counter
        self.updateAcceptedSolutions()
        # updating trust for the agents
        for agent in self.agents:
            agent.computeTrust()
            agent.setReputation()
        for selectedAgent in self.selectedAgents:
            selectedAgent.increaseProposedSolutions()
        self.updateEvents()
        # storing in the database
        self.storeInDatabase()
        #print("feedback from CA",self.computeControllerAgentFeedback())
    #
    ## Method for clustering the groups of agents
    # @return groups of agents
    def clusterAgents(self):
        # adding all the positions of the agents to a list
        allPositions = [agent.position for agent in self.agents]
        # grouping agents based on their k means
        k = self.computeKclusters()
        kmeans      = KMeans(n_clusters=k, random_state=0).fit(allPositions)
        centroids   = kmeans.cluster_centers_
        # to see how agents are grouped
        groups      = kmeans.fit_predict(allPositions)
        # number of groups formed
        numGroups   = len(list(set(groups)))
        # dictionary to store agents in the groups
        agentsByGroups          ={}
        # creating empty list for each groups
        for i in range(numGroups):
            agentsByGroups[i]   = []
        for i in agentsByGroups.keys():
            for j in range(len(self.agents)):
                group           = groups[j]
                agent           = self.agents[j]
                # adding agents to the groups
                if group == i:
                    agentsByGroups[i].append(agent)
        return agentsByGroups
    #
    ## Method that selects the agents ideas
    # @return List of selected agents
    def selectAgents(self):
        # computing the variance
        selfEfficacyVar     = np.std([agent.selfEfficacy for agent in self.agents])
        # random number of agents are selected if the variance of SE is low
        if selfEfficacyVar >= 0.0 and selfEfficacyVar < 0.020:
            n               = np.random.randint(2,len(self.agents))
            # selecting random number of agents
            agentsSelected  = np.random.choice(self.agents,n,replace=False)
        # else agents with high self-efficacy are selected
        else:
            # agents with max self efficacy in descending order
            sortedlist      = sorted(self.agents,key=lambda x: x.selfEfficacy, reverse=True)
            # first three agents with high SE selected
            agentsSelected  = sortedlist[0:3]
            # creating list of agents with low SE
            agentsLowSE     = sortedlist[3:]
            # random number of agents in the remaining list of low SE agents
            m               = np.random.randint(1,len(agentsLowSE))
            # selecting from the list of other agents who were not selected
            chooseAgentsLowSE   = np.random.choice(agentsLowSE,m,replace=False)
            # adding low SE agents to the selected high SE agent list
            agentsSelected.extend(chooseAgentsLowSE)
            #n               = np.random.randint(1,len(self.agents))
            #agentsSelected  = np.random.choice(self.agents,n,replace=False)
        # returning the list of agents
        return agentsSelected
    #
    ## Method to compute the centroid:
    # @param agentPositionsList list of (x,y) coordinates of the agent positions
    # @return centroid (x,y)
    def computeCentroid(self,agentPositionsList):
        length = len(agentPositionsList)
        # list of agent positions
        positionList = np.array(agentPositionsList)
        # summing x coordinates
        sum_x = np.sum(positionList[:, 0])
        # summing y coordinates
        sum_y = np.sum(positionList[:, 1])
        # computing the centroid 
        centroid_x = int(np.round(sum_x/length))
        centroid_y = int(np.round(sum_y/length))
        centroid = centroid_x,centroid_y
        return centroid
    #
    ## Method for checking if there are more than one selected agent in the same group
    def computeProposedSolution(self):
        # creating local variables from arrtibutes
        groups = self.agentsGroups
        selectedAgents = self.selectedAgents
        # creating a dictionary of centroid where keys are the group number and values are the solution of that group
        centroid = {}
        for groupKey, group in groups.items():
            newSelectedSolution = [0,0]
            c=0
            agentPositionList = []
            for agent in group:
                # counting the number of selected agents in the group
                if agent in selectedAgents:
                    c +=1
                    # adding their position to a list
                    agentPositionList.append(agent.position)
            # checking if there are more than 1 selected agent in the group
            if c > 1:
                # if there are more than one selected agent in the same group then updating the solution value as centroid
                centroid[groupKey]  = self.computeCentroid(agentPositionList)
                for selectedAgent in selectedAgents:
                    # updating the proposed solution value of the selected agents in the same group to centroid value
                    if selectedAgent in group:
                        selectedAgent.setProposedSolution(centroid[groupKey])        
                        # getting the updated position of the selected agents
                        pos                 = selectedAgent.getProposedSolution()
                        # updating selected agent's solution position 
                        selectedAgent.setPosition(pos)
                        solutionValue       = self.solutionSpace[pos[0],pos[1]]
                        # storing the updated solution
                        selectedAgent.storeSolutionValue(solutionValue)
    #
    # Method for computing optimal k cluster value
    # @return integer value of k clusters 
    def computeKclusters (self):
        #k = np.random.randint(2,len(self.agents)-2)
        k = np.random.randint(1,len(self.agents))
        return k
        #
    # Method computing agreement over the solution proposed based on self-efficacy
    # @return the agent(s) with the maximum votes or top 3 agents with maximum votes
    def computeAgreement(self):
        # creating local variables from arrtibutes
        groups          = self.agentsGroups
        selectedAgents  = self.selectedAgents
        maxVote         = 0
        maxVoteAgents   = []
        agreementsAll   = []
        # dict to store (keys) selcted agent and (value) agreement values related to them
        selectedAgentAgreementValues   = {}
        # dictionary to store agent orm id (keys) and agreement values (values) for storing in database
        selectedAgentOrmIdAgreementValues = {}
        # computing the agreement for each proposed solution by selected agents
        for selectedAgent in selectedAgents:
            agreement = self.computeSolutionAgreement(selectedAgent)
            # storing all the agreement values in a list
            agreementsAll.append(agreement)
            selectedAgentAgreementValues[selectedAgent]  = agreement
            selectedAgentOrmIdAgreementValues[selectedAgent.ormObject.id] = agreement
        self.selectedAgentAgreementValues = selectedAgentOrmIdAgreementValues
        # sorting (descending order) the selcted agents based on the solution agreement values (top 3)
        agreementValuesSorted = sorted(selectedAgentAgreementValues.items(), key=lambda x: x[1],reverse=True)[0:3]
        # computing the standard deviation of the agreement values
        agreementSTD    = np.std(agreementsAll)
        if agreementSTD == 0:
            maxVoteAgents = np.random.choice(selectedAgents,3)
        else:
            # generating a random number between 0 and agreementSTD *1.5
            p = np.round(np.random.uniform(0.0,(agreementSTD*1.30)),3)
            if p < agreementSTD:
                # storing the max vote agents
                if agreement == maxVote:
                    maxVoteAgents.append(selectedAgent)
                elif agreement > maxVote:
                    maxVoteAgents = [selectedAgent]
                    maxVote = agreement
                else:
                    pass
            else:
                #  when there are more top three solutions(maxVoteAgents)
                for key in (dict(agreementValuesSorted)).keys():
                    # storing the top 3 keys (selected agents)
                    maxVoteAgents.append(key)
        return maxVoteAgents
    #   
    ## Method for computing the agreement for one proposed solution /one selected agent
    # @param selectedAgent one selected agent
    # @return integer value of the agreement
    def computeSolutionAgreement(self,selectedAgent):
        # creating local variables from arrtibutes
        groups = self.agentsGroups
        agreeTotal       = 0
        for groupKey, group in groups.items():
            for agent in group:
                # checking if the group is only one agent in it
                if len(group) >1:
                    # computing the group mean of self-efficacy
                    sumGrpSE        = np.sum([agent.selfEfficacy for agent in group])
                    if sumGrpSE > selectedAgent.selfEfficacy:
                        pass
                    # the group agrees with the solution if the group SE is less than agent SE
                    else:
                        agree       = self.computeAgreementValue(agent,selectedAgent)
                        agreeTotal  = agree + agreeTotal
                else:
                    if agent.selfEfficacy > selectedAgent.selfEfficacy:
                        pass
                    else:
                        agree       = self.computeAgreementValue(agent,selectedAgent)
                        agreeTotal  = agree + agreeTotal
        return agreeTotal
    #
    ## Method for computing the agreement value of an agent
    # @param agent agent who is in the group
    # @param selectedAgent agent who is proposing the solution
    # @return agree float value based on an agent's prevAgree values and influence value
    def computeAgreementValue(self,agent,selectedAgent):
        # getting the agreement value selected agent had for agent
        agreeValue  = selectedAgent.getPreviousAgreements(agent)
        # computing aggreement of an agent
        newValue    = (agent.getInfluenceValues(selectedAgent)+ agreeValue)/2
        # updating the agreement dic of the agent
        agent.updatePreviousAgreements(newValue,selectedAgent)
        # checking if the dictionary is not empty
        newAgreeValue = agent.getPreviousAgreements(selectedAgent)
        return newAgreeValue
    #
    ## Method for updating the Self-Efficacy for selected agents
    # @param maxVoteAgents agents who got the maximum votes
    def updatingSelfEfficacy(self):
        # creating local variables from attributes
        selectedAgents = self.selectedAgents
        maxVoteAgents    = self.maxVoteAgents
        groups          = self.agentsGroups
        # dicts for storing whether or not maxVoteAgents are in the same group, key = agents, values = boolean 
        done            = {}
        # dicts for storing whether or not selected agent are in the same group of maxVoteAgents, key = agents, values = boolean 
        remained        = {}
        # storing the default values in the dictionary for selected agents
        for selectedAgent in selectedAgents:
            done[selectedAgent]     = False
            remained[selectedAgent] = True
        for maxVoteAgent in maxVoteAgents:
            for groupKey, group in groups.items():  
                # selecting the group with max vote agent
                if maxVoteAgent in group:
                    # checking if there are other selcted agents in the same group
                    for selectedAgent in selectedAgents:
                        if done[selectedAgent] == False:
                            if selectedAgent in group:
                                # updating the self efficacy of the agents in the same group when solution was accepted
                                isIncreaseSE = True
                                done[selectedAgent] = True
                                # increasing the self- efficacy
                                selectedAgent.updateSelfEfficacy(isIncreaseSE)
                                # updating to True as the agent was a part of a group with maxVote agent
                                remained[selectedAgent] = False
        # updating the self efficacy (decrease) of the remaining agents groups other than maxVote agents
        for selectedAgent in selectedAgents:
            # check if the selected agent was not in any group as of maxVote agents
            if remained[selectedAgent] == True:
                isIncreaseSE = False
                # decreasing the self- efficacy
                selectedAgent.updateSelfEfficacy(isIncreaseSE)                   
    #
    ## Method for controller agent decision making
    # @returm feedbackValue float value of the solution
    def computeControllerAgentFeedback(self):
        maxVoteAgents = self.maxVoteAgents
        solutionValues = []
        # dict to store values of the 3 solutions and positions
        finalSolutionValuePosition = {}
        # generating a random number
        p = np.random.random()
        # checking if there is one or multiple solutions
        if len(maxVoteAgents) ==1:
            maxVoteAgent = maxVoteAgents[0]
            solutionValue     = maxVoteAgent.solutionValue
            self.finalSolution = solutionValue
            # updating the final solution position
            self.finalPosition = maxVoteAgent.getProposedSolution()
            if p < solutionValue:
                accepted = True
            else:
                accepted = False
            if accepted:
                if solutionValue > 0.5:
                    feedbackValue = solutionValue
                else:
                    feedbackValue = 0.5
            else:
                if solutionValue < 0.4:
                    feedbackValue = solutionValue
                else:
                    feedbackValue = 0.4
        # when there are multiple maxVote agents
        else:
            # dict to store values of the 3 solutions and positions
            finalSolutionValuePosition = {}
            for maxVoteAgent in maxVoteAgents:
                solutionValue     = maxVoteAgent.solutionValue
                # adding all the selected solution values and their positions
                finalSolutionValuePosition[solutionValue] = maxVoteAgent.getProposedSolution()
                #solutionValues.append(solutionValue)
            # choosing the solution with maximum quality
            #feedbackValue = max(solutionValues)
            #self.finalSolution = feedbackValue
            # sorting (descending order) the solution values to get the best solution
            bestSolutionValuePosition  = sorted(finalSolutionValuePosition, reverse=True)[0:1]
            # storing the feedback as the solution value
            feedbackValue      = bestSolutionValuePosition[0]
            self.finalSolution = feedbackValue
            # updating the final solution position based on the solution value
            self.finalPosition = finalSolutionValuePosition.get(bestSolutionValuePosition[0])
        self.feedbackValue = feedbackValue
        return feedbackValue
    #
    ## Method for storing success and failure
    def updateEvents(self):
        # Updating success and failure of all agents in the group of Max vote agent
        '''# creating local variables from arrtibutes
        groups          = self.agentsGroups
        maxVoteAgent    = self.maxVoteAgent
        for groupKey, group in groups.items():
            # selecting the group with max vote agent
            if maxVoteAgent in group:
                # checking if there are other selcted agents in the same group
                for agent in group:
                    # calling the method to store feedback in agents
                    agent.storeFeedback(self.computeControllerAgentFeedback(),self.numSession)
                # breaking out of the loop
                break'''
        # updating success and failure of all agents
        for agent in self.agents:
            agent.storeFeedback(self.computeControllerAgentFeedback(),self.numSession)
    #
    ## Method for updating the accepted solutions and SE based on controller agent feedback
    # @param maxVoteAgent agent who got the maximum vote
    def updateAcceptedSolutions(self):
        # creating local variables from arrtibutes
        groups          = self.agentsGroups
        selectedAgents  = self.selectedAgents
        maxVoteAgents   = self.maxVoteAgents
        finalAgents     = []
        # a Dict to store max vote agents(keys) and teir solution values (values)
        maxVoteAgentsSolution   = {}
        if len(maxVoteAgents) ==1:
            finalAgent  = maxVoteAgents[0]
        else:      
            for maxVoteAgent in maxVoteAgents:
                maxVoteAgentsSolution[maxVoteAgent]=maxVoteAgent.solutionValue
             # sorting (descending order) the max vote agents agents based on the solution quality values
            solutionValuesSorted = sorted(maxVoteAgentsSolution.items(), key=lambda x: x[1],reverse=True)
            # storing the key (agent who's solution was selceted)
            for key in (dict(solutionValuesSorted)).keys():
                # storing the top 3 keys (agent who's solution was selected)
                finalAgents.append(key)
            # selecting the first one
            finalAgent = finalAgents[0]
        for groupKey, group in groups.items():
            # selecting the group with max vote agent
            if finalAgent in group:
                # checking if there are other selcted agents in the same group
                for selectedAgent in selectedAgents:              
                    if selectedAgent in group:
                        # checking the feedback value for accepted or rejected solutions
                        feedbackValue = self.computeControllerAgentFeedback()
                        if feedbackValue > 0.5:
                            # calling the method to increase the accepted solution
                            selectedAgent.increaseAcceptedSolutions()
                        else:
                            # reducing the self efficacy of the agents if the solution was rejected
                            isIncreaseSE = False
                            selectedAgent.updateSelfEfficacy(isIncreaseSE)
    #
    #########################################
    #           ORM METHODS 
    #########################################
    ## Method that creates a new idea selction instance in the database
    def storeInDatabase(self):
        # create a idea selection
        ideaSelection                   = ModelIdeaSelection()
        # storing in the ORM
        ideaSelection.agreementValues   = self.selectedAgentAgreementValues              
        ideaSelection.feedbackValue     = self.feedbackValue
        ideaSelection.sessionId         = self.sessionOrmObject.id
        ideaSelection.finalSolution     = self.finalSolution
        ideaSelection.finalSolutionPosition  = self.finalPosition
        ideaSelection.maxVotedAgents    = [maxVoteAgent.ormObject.id for maxVoteAgent in self.maxVoteAgents]
        # storing the ORM object in Python object
        self.ormObject                    = ideaSelection
        # adding the object to the session
        databaseSession.add(ideaSelection)
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()
#
if __name__ == "__main__":
    pass