# standard imports


# custom imports
from abm_simulation.constants import *
from abm_simulation.session import Session
#from abm_simulation.solution_space import createSolutionSpace
from abm_simulation.utils import _print
import os

# model imports
from models.base import databaseSession
from models.agent import Agent as ModelAgent
from models.success import Success as ModelSuccess
from models.session import Session as ModelSession
from models.project import Project as ModelProject
from models.steps import IdeaGenerationStep as ModelIdeaGenerationStep
from models.failure import Failure as ModelFailure



## class that implements the project
class Project():

    ## Constructor method
    # @param numSessions total number of sessions in a projects
    # @param numAgents Number of agents to be created
    # @param selfEfficacies Self-efficacy values to be set, if passed
    def __init__(self, numSessions,agents,solutionSpace):
        # project number needed to create a pool of exp agents
        self.projectNum     = 0
        self.numSessions    = numSessions
        # creating agents
        self.agents         = agents
        # creating solution space
        self.solutionSpace  = solutionSpace
        # storing in the database
        self.storeInDatabase()
    #
    ## Method that prepares the project before running it
    def onStart(self):
        # preparing the sequence of sessions & gaps to be run
        self.stages     = [] 
        for numSession in range(self.numSessions):
            # creating a session
            session     = Session(self.agents, self.solutionSpace, numSession)
            # appending the session
            self.stages.append(session)
            # attaching the session to the project
            self.ormObject.sessions.append(session.ormObject)
            # committing & flushing the database
            databaseSession.commit()
            databaseSession.flush()
    #
    ## Method that post-processes results after end of running
    def onEnd(self,NUM_PROJECTS_EXP):
        expAgents = []
        # creating a list of old experience agents from the previous projects
        if self.projectNum < NUM_PROJECTS_EXP:
            # agents with max self efficacy in descending order
            sortedlist      = sorted(self.agents,key=lambda x: x.selfEfficacy, reverse=True)
            # first 3 agents with high SE selected
            chosenAgents  = sortedlist[0:3]
            # adding the agents to the experience agent list
            for c in chosenAgents:
                c.initAgent(isExperienced = True)
                # adding to the list 
                expAgents.append(c)
            self.expAgents = expAgents
        else:
            pass
        # increating the number of project
        self.projectNum     += 1
    #
    ## Method that creates a list of agents both exp and new
    # @param ol
    ## Method for running the project
    def run(self):
        # running the sequence of stages
        for stage in self.stages:
            # running the stage
            stage.onStart()
            stage.run()
            stage.onEnd()
    #
    #########################################
    #           ORM METHODS 
    #########################################
    ## Method that creates a new project instance in the database
    def storeInDatabase(self):
        # creating the project
        prj             = ModelProject()
        prj.number      = 1
        prj.solutionSpace =  self.solutionSpace
        # setting the agents in the project
        prj.agents      = [agent.ormObject for agent in self.agents]
        # storing the ORM object in Python object
        self.ormObject  = prj
        # adding the object to the session
        databaseSession.add(prj)
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()
    # 


if __name__ == "__main__":
    pass

