# standard imports
import numpy as np
import os

# custom imports
from abm_simulation.idea_generation import IdeaGenerationActivity
from abm_simulation.idea_selection import IdeaSelectionActivity
from abm_simulation.utils import _print

# model imports
from models.base import databaseSession
from models.agent import Agent as ModelAgent, AgentCharacteristics as ModelAgentCharacteristics
from models.success import Success as ModelSuccess
from models.project import Project as ModelProject
from models.session import Session as ModelSession
from models.steps import IdeaGenerationStep as ModelIdeaGenerationStep
from models.failure import Failure as ModelFailure

# SETTINGS
# set session's team virtuality level where 4 is all virtual and 0 is all face to face
VIRTUALITY      = 0
# set sessions's effective technical mediation (where 0.3 = most effective and 0.7 = least)
ETM             = 0.3
# set constant for determining the shape of the communication effectiveness curve (where C = steep slope and 2C = gradual slope)
CURVESHAPE      = 10


## Class that implements the session
class Session():

    #ormObject   = None      # ORM object related to this Python object (for linking to db)
    ## Constructor method
    # @param agents list of agent objects
    # @param solutionSpace Matrix representing the problem and its solutions
    # @param numSession Counter of running session
    def __init__(self, agents, solutionSpace, numSession):
        # init the agents
        self.agents         = agents
        # init the solution space
        self.solutionSpace  = solutionSpace
        # init number of sessions
        self.numSession     = numSession
        # init the type of virtuality where 0 = facetoface and 4 = all virtual
        self.virtuality     = VIRTUALITY
        # init the type of ETM = effective technical mediation (where 0.3 = most effective and 0.7 = least)
        self.etm            = ETM
        # storing in the database
        self.storeInDatabase()
        # init an Idea Generation activity
        self.ideaGenerationActivity     = IdeaGenerationActivity(agents, solutionSpace, numSession)
        # init an Idea Selection activity
        self.ideaSelectionActivity      = IdeaSelectionActivity(agents,self.ormObject,solutionSpace)
    # 
    ## Method for running the session
    def run(self):
        # calling idea generation
        self.ideaGenerationActivity.onStart()
        self.ideaGenerationActivity.run()
        self.ideaGenerationActivity.onEnd()
        # calling idea selection
        self.ideaSelectionActivity.onStart()
        self.ideaSelectionActivity.run()
        self.ideaSelectionActivity.onEnd(self.agents)
    #
    ## Method called before the beginning of a session
    def onStart(self):
        # setting up the session
        # setting up the communication effectiveness when a session starts
        self.effectiveCommunication     = self.calculateComEffectiveness(self.virtuality,self.etm,CURVESHAPE)
        # setting agent positions when a session starts
        for agent in self.agents:
            # giving a random initial position
            randomPosition = (
                np.random.randint(0, self.solutionSpace.shape[0]), 
                np.random.randint(0, self.solutionSpace.shape[1]), 
            )
            agent.setPosition(randomPosition)
            # setting up the session
            agent.onSessionStart(self.numSession,self.agents,self.effectiveCommunication,self.virtuality)
        # adding agents in ORM
        self.addOrmAgents()
    #
    ## Method called after a session ends
    def onEnd(self):
        # on session end updating the agents
        for agent in self.agents:
            agent.onSessionEnd(agent.solutionValue, self.numSession)
        # checking if agents are retiring 
        agentsRemoved   = []
        for agent in self.agents:
            if agent.isAgentRetired():
                _print("Agent %s is retiring." % (agent.name))
                agentsRemoved.append(agent)
        # deleting the agents
        for agentDel in agentsRemoved:
            self.agents.remove(agentDel)
        # updating the counter
        self.numSession += 1
    #
    ## Method for calculating the communication effectiveness
    # @param virtualityValue virtual value of a session
    # @param etmValue effective technology mediation value of a session
    # @return comEffVal communication effectiveness float value
    def calculateComEffectiveness(self, virtualityValue, etmValue, curveShape):
        # calculating the communicaton effectiveness value
        if 0 < etmValue <=0.3:
            # gradual slope when the etm has a good value 
            c = 2*curveShape
            comEffVal = c/((c-1)+ np.exp(etmValue*virtualityValue))
        elif 0.3< etmValue <=0.5:
            # inbetween slope when the etm has an intermediate value 
            c = 1.5*curveShape
            comEffVal = c/((c-1)+ np.exp(etmValue*virtualityValue))
        else:
            # steep slope when the etm has a bad value 
            c = curveShape
            comEffVal = c/((c-1)+ np.exp(etmValue*virtualityValue))
        return comEffVal
    #
    #########################################
    #           ORM METHODS 
    #########################################
    ## Method that creates a new session instance in the database
    def storeInDatabase(self):
        # create a session
        session         = ModelSession()
        session.number  = self.numSession
        # storing the ORM object in Python object
        self.ormObject  = session
        # adding the object to the session
        databaseSession.add(session)
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()
    #
    ## Method that links the agents to the session in the ORM
    def addOrmAgents(self):
        # adding the connection with agents
        for agent in self.agents:
            # creating a new link ORM object
            agentSessionLink            = ModelAgentCharacteristics()
            agentSessionLink.agent_id   = agent.ormObject.id
            agentSessionLink.session_id = self.ormObject.id
            agentSessionLink.experience = agent.experience
            agentSessionLink.selfEfficacy   = agent.selfEfficacy
            agentSessionLink.reputation     = agent.reputation
            agentSessionLink.trust          = agent.trust
            agentSessionLink.familiarity    = agent.familiarity
            agentSessionLink.influencersValue       = agent.influencersValue
            databaseSession.add(agentSessionLink)
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()
    #
if __name__ == "__main__":
    # importing solution space, agent and constants
    from solution_space import createSolutionSpace
    from agents.agent import Agent
    from constants import NUMPIXELS
    # creating solution space
    solutionSpace = createSolutionSpace()
    # setting the current session number
    currentSession = 0
    # creating a list of agents
    agents  = []
    for i in range(6):  
        # creating the agent
        agent   = Agent(            \
            name="Agent #"+str(i),  \
            initPosition=(50, 50), \
            limit=NUMPIXELS,        \
            maxNumSessions=3
        )
        # appending to list
        agents.append(agent)
    ################################################
    # init the session
    session     = Session(agents, solutionSpace, currentSession)
    # preparing session 
    session.onStart()
    # running session
    session.run()
    # closing session and wrap up
    session.onEnd()
    # 