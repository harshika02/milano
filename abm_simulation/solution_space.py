# standard imports
import numpy as np
import pickle

# custom imports#
from abm_simulation.constants import *
#from constants import *


## function that dumps a solution space into a pickle file
# @param solutionSpace Solution space that has to be saved
def dumpSolutionSpace(solutionSpace):
    # dumping the solution space to file
    with open(SOLUTION_SPACE_PICKLE_FILE_EXP, "wb") as f:
        pickle.dump(solutionSpace, f)


## Function that loads a solution space from pickle file
# @return Solution space read from pickle
def loadSolutionSpace():
    # loading the solution space from file
    with open(SOLUTION_SPACE_PICKLE_FILE, "rb") as f:
        solutionSpace = pickle.load(f)
    # returning the solution space
    return solutionSpace
#
## Function that loads a anothersolution space from pickle file for experienced agents
# @return Solution space read from pickle
def loadSolutionSpaceExp():
    # loading the solution space from file
    with open(SOLUTION_SPACE_PICKLE_FILE_EXP, "rb") as f:
        solutionSpace = pickle.load(f)
    # returning the solution space
    return solutionSpace


## Function that computes the value based on position of point and nearest solution
# @param point (x,y) of a point
# @param solution (x,y) of a solution
# @return value at the given point
def landscapeFunction(point, solution):
    # computing the distance
    distance    = np.sqrt(np.power(point[0]-solution[0],2)+np.power(point[1]-solution[1],2))
    # passing through shaping function   ############# curved =3 normal =2 steep = 1
    value       = 1.0/(1.0 + np.exp((1.0/np.sqrt(NUMPIXELS)) * distance - 2.0))
    return value

## Function that creates a solution space
def createSolutionSpace():
    solutionSpace = loadSolutionSpace()
    # create zeros matrix
    '''solutionSpace   = np.zeros((NUMPIXELS, NUMPIXELS))
    # generate number of solutions with value 1
    #numSolutions    = np.random.randint(MINSOLUTIONS, 2)
    numSolutions    = 5
    # generate number of solutions with local maximum value <0.1
    #numLocalMaxima  = np.random.randint(1, 3)
    numLocalMaxima  = 0
    # create solutions list
    solutions       = []
    for i in range(numSolutions):
        # generating random coordinates
        x   = np.random.randint(0, NUMPIXELS-1)
        y   = np.random.randint(0, NUMPIXELS-1)
        # setting solution
        solutionSpace[x,y]  = 1.0
        # adding the solution to list
        solutions.append((x,y))
    for j in range(numLocalMaxima):
        # generating random coordinates
        x   = np.random.randint(0, NUMPIXELS-1)
        y   = np.random.randint(0, NUMPIXELS-1)
        # setting solution
        solutionSpace[x,y]  = np.random.uniform(0.4,0.7)
        # adding the solution to list
        solutions.append((x,y))
    # adapting surface
    for x in range(0,NUMPIXELS):
        for y in range(0,NUMPIXELS):
            # searching distance from nearest solution
            value       = solutionSpace[x,y]
            for solution in solutions:
                value   = max(value,landscapeFunction((x,y), solution))
            # setting the new value
            solutionSpace[x,y]  = value
    # adding some noise
    noise           = 0.07 * np.random.random((NUMPIXELS, NUMPIXELS))
    solutionSpace   = solutionSpace+noise
    # cutting exceeding limits
    solutionSpace[solutionSpace>1.0] = 1.0
    solutionSpace[solutionSpace<0.0] = 0.0'''
    ##############3 creating a defined solution space#############3
    # create zeros matrix
    ''' solutionSpace   = np.zeros((NUMPIXELS, NUMPIXELS))
    # generate number of solutions with value 1
    numSolutions    = 5
    numLocalMaxima  = 0
    # create solutions list
    solutions       = []
    # generating random coordinates
    x1   = 20
    y1   = 20
    x2   = 40
    y2   = 40
    x3   = 70
    y3   = 30
    solutions=[(x1,y1),(x2,y2),(x3,y3)]
    for i in solutions:
        # setting solution
        solutionSpace[i]  = 1.0
    # adapting surface
    for x in range(0,NUMPIXELS):
        for y in range(0,NUMPIXELS):
            # searching distance from nearest solution
            value       = solutionSpace[x,y]
            for solution in solutions:
                value   = max(value,landscapeFunction((x,y), solution))
            # setting the new value
            solutionSpace[x,y]  = value
    # adding some noise
    noise           = 0.07 * np.random.random((NUMPIXELS, NUMPIXELS))
    solutionSpace   = solutionSpace+noise
    # cutting exceeding limits
    solutionSpace[solutionSpace>1.0] = 1.0
    solutionSpace[solutionSpace<0.0] = 0.0'''
    # returning space
    return solutionSpace
#
# Function loading solution space for experience agents
def createSolutionSpaceExp():
    solutionSpace = loadSolutionSpaceExp()
    # returning space
    return solutionSpace
#    
if __name__ == "__main__":
    # import
    import matplotlib.pyplot as plt
    # create solution space
    solutionSpace0 = createSolutionSpace()
    # plotting solution space
    fig,ax  = plt.subplots(1)
    ax.matshow(solutionSpace0)
    plt.show()
    #dumping = dumpSolutionSpace(solutionSpace0)
