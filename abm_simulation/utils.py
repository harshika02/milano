# standard imports
import matplotlib.pyplot as plt
import os
import sys
import multiprocessing

# custom imports
from abm_simulation.constants import *



## Function that plots an arrow from x and y values
# @param x array of x coordinates of agent position
# @param y array of y coordinates of agent position
# @param j step
# @param lastPoint is the last point of the agent position
def plotArrow(x, y, j, lastPoint=False):
    # setting arrow size
    if lastPoint:
        arrowSize   = NUMPIXELS/50
    else:
        arrowSize   = 0
    # computing params for plot
    x0      = x[j]
    dx      = x[j+1] - x0
    y0      = y[j]
    dy      = y[j+1] - y0
    #drawing a line between two points
    if dx == 0 and dy ==  0:
        pass
    else:
        plt.arrow(x0,y0,dx,dy,head_width=arrowSize, head_length=arrowSize, fc='r', ec='r')

## Function that makes sure that the folder exists
# @param path path to be created
def createFolder(path):
    # checking if the folder exists
    if os.path.exists(path):
        pass
    # if not then create folder
    else:
        os.mkdir(path) 


## Function that prints also to file
# @param args Arguments to be printed
# @param debug Sets if the log is for debug or not; True --> it's debug
def _print(*args, debug=False):
    if (debug and DEBUG_ENABLE) or (not debug):
        if LOG_ENABLE:
            # generating file name
            logFileName = LOG_FILENAME % (multiprocessing.current_process().pid)
            # opening file in appending 
            with open(logFileName, 'a+') as f:
                print(*args, file=f)
    else:
        # logging nothing
        pass
    # print to console
    if PRINT_ENABLE:
        print(*args)    # printing to the standard out



# resetting the log file
if LOG_ENABLE:
    with open(LOG_FILENAME, "w+") as f:
        pass


if __name__ == "__main__":
    # import
    from solution_space import createSolutionSpace
    # create solution space
    solutionSpace0 = createSolutionSpace ()
    # plotting solution space
    fig,ax  = plt.subplots(1)
    ax.matshow(solutionSpace0)
    arrowPlot = plotArrow([4,10],[2,34],0, True)
    plt.show()


