# standard imports
import numpy as np
import random
import os

# custom imports
from learning.failure import Failure
from learning.success import Success
from agents.constants import *
from agents.utils import generateExplorationCurve
from abm_simulation.utils import _print

# model imports
from models.base import databaseSession
from models.agent import Agent as ModelAgent
from models.success import Success as ModelSuccess
from models.project import Project as ModelProject
from models.session import Session as ModelSession
from models.steps import IdeaGenerationStep as ModelIdeaGenerationStep
from models.failure import Failure as ModelFailure


## class that implements normal agent
class Agent:
    ## Constructor method
    # @param name name of the agent
    # @param limit Maximum limit of the solution space
    # @param maxNumSessions Maximum session number
    def __init__(self, name, limit, maxNumSessions):
        # storing the name of the agent
        self.name   = name
        # giving a standard initial position
        self.position      = np.array([0,0])
        # storing the limit in the solution space
        self.limit  = limit
        # setting the maximum step size
        self.maxStepSize    = int(self.limit/3)
        # resetting the step counter
        self.stepNum        = 0
        # generating agent originality curve
        self.originalityCurve   = generateExplorationCurve(1000)
        # setting up memory size
        self.maxFailureMemSize  = np.random.randint(3,20)
        self.maxSuccessMemSize  = np.random.randint(3,20)
        # setting up the historical positions of the agent
        self.historyX       = []
        self.historyY       = []
        # radius of the failure circle for agents
        self.rCircle        = 0.0
        # store max number of sessions
        self.maxNumSessions = maxNumSessions
        # initialize agent
        self.initAgent(isExperienced = False)
        # storing in the database
        self.storeInDatabase()
    #
    ## Method that initializes the agent based on experience
    # @param isExperienced Boolean that says whether the agent is experienced or not
    def initAgent(self, isExperienced):
        # store passed value
        self.isExperienced = isExperienced
        ##################################
        # INIT ATTRIBUTES
        # reputation of an agent
        self.reputation     = 0.0
        # accepted solution of an agent
        self.acceptedSolutions  = 0
        # proposed solution of an agent
        self.proposedSolutions  = 0
        # creating a counter for storing number of session
        self.numSession         = 0
        # creating familiarity for storing the agents (keys) who are working on the same session (value)
        self.familiarity        = {}
        # creating dic for storing previous agreed values of other agents, where agents (keys) have agreement value (value)
        self.prevAgree          = {}
        # creating trust for storing the agents (keys)  and trust value (value)
        self.trust              = {}
        # creating influencers dict for storing the agents (keys)  and influence bias (value)
        self.influencers        = {}
        # creating influencers dict for storing the agents (keys)  and influence value (value)
        self.influencersValue   = {}
        # radius of the failure circle for agents
        self.rCircle        = 0.0
        # creating recalled successes and failures for every session
        self.recalledSuccesses  = {}
        self.recalledFailures   = {}
        ##################################
        # INIT ATTRIBUTES BASED ON EXPERIENCE
        if self.isExperienced:
            # previous sessions are stored that will be needed in computing delta time for recall
            self.prevSessions   = self.maxNumSessions
            # if exp agent then change its max sessions to double (example 20 where 10 from prev peojects and 10 current)
            self.maxNumSessions = self.maxNumSessions * 2
            # calculating slight decrese in the self-efficacy when joining a new team
            f = (self.selfEfficacy / self.prevSessions)
            self.selfEfficacy   = self.selfEfficacy - f
        else:
            # previous sessions are stored that will be needed in computing delta time for recall
            self.prevSessions   = 0
            # init the lists of failures and successes
            self.failures       = []
            self.successes      = []
            # at 50 units of experience an agent retires    #### REMOVE /2 #####
            self.experience     = np.random.randint(0,MAX_AGE_LIMIT/4)
            # normalising experience between 0 and 1
            self.normExp        = self.experience / MAX_AGE_LIMIT/4
    #
    ## Method that formats the print string
    # @return Print string
    def __str__(self):
        return "Agent(name='%s')" % (self.name)
    # 
    ## Method for setting the position of agent
    # @param position position (x,y) of the agent
    def setPosition(self,position):
        self.position = np.array(position, dtype=int)
    #
    ## Method that gives back the position of agents
    # @return x and y cordinates of the position
    def getPosition(self):
        return self.position
    #
    ## Method for setting the proposed solution for agent
    # @param (x,y) position of the agent
    def setProposedSolution(self,position):
        self.proposedSolution = position
    #
    ## Method that gives back the proposed solution position for agent
    # @return x and y cordinates passed
    def getProposedSolution(self):
        return self.proposedSolution
    #
    ## Method for computing the next step
    def run(self):
        # init the while loop control variable
        result = False
        numCycles = 0
        while not result:
            # generate random step (-0.5 to 0.5)
            randomStep      = self.generateRandomStep()
            # add success bias
            successesBias = np.array([0.0,0.0])
            for success in self.recalledSuccesses[self.currentSession]:
                # computing success bias
                successesBias   += self.computeSuccessBias(success)
            # computing influence bias
            sumInfluencersBias = np.array([0.0,0.0])
            self.updateInfluencerBias()
            influencers = self.getInfluenceBias()
            for agentId, bias in influencers.items():
                sumInfluencersBias +=  bias
            # summing the two biases and step
            step  = randomStep + successesBias + sumInfluencersBias
             # storing the step without influencer bias
            stepWithoutIB = randomStep + successesBias
            # adjust steps
            step            = self.getStepSize(step)
            intendedStep    = self.getStepSize(randomStep)
            stepSB          = self.getStepSize(stepWithoutIB)
            # adding the step 
            position        = self.computeNewPosition(step)
            positionSB      = self.computeNewPosition(stepSB)
            intendedPosition        = intendedStep + self.intendedPosition
            # checking limits of positions
            correctedPosition       = self.correctPosition(position)
            correctedIntendedPosition   = self.correctPosition(intendedPosition)
            correctedPositionSB         = self.correctPosition(positionSB)
            # correcting steps after the limit checks
            actualStep      = correctedPosition - self.position
            successBiasStep = correctedPositionSB - self.positionSB
            intendedStep    = correctedIntendedPosition - self.intendedPosition
            # check if new temp position is allowed
            result      = self.isPositionAllowed(correctedPosition)
            # updating the counter
            numCycles += 1
            if numCycles >= 100:
                # allowing agent to stay inside the failure
                # getting out of the loop
                result  = True
        # storing in the ORM
        self.storeStepInOrm(
            position            = correctedPosition,
            intendedPosition    = correctedIntendedPosition,
            positionSB          = correctedPositionSB,
            stepNumber          = self.stepNum,
            actualStep          = actualStep,
            successBiasStep     = successBiasStep,
            intendedStep        = intendedStep,
            successesBias       = successesBias,
            sumInfluencersBias  = sumInfluencersBias
        )
        # store the attributes
        self.setPosition(correctedPosition)
        self.intendedPosition   = correctedIntendedPosition
        # storing the point
        self.storeHistoricalPosition(correctedPosition)
        # step number counter increases by 1
        self.stepNum += 1
    #
    ## Method thatgets step size
    # @param step normalised step actualStep
    # @return step actualStep modulated by originality curve
    def getStepSize(self, step):
        newStep     = [0,0]
        # adjusting
        newStep     = step * self.maxStepSize*self.originalityCurve[self.stepNum+1]
        # casting to integer
        newStep     = np.array(newStep, dtype=int)
        return newStep
    #
    ## Method that stores historical position
    # @param position Position to be stored
    def storeHistoricalPosition(self, position):
        # appending position to the list of past values
        self.historyX.append(position[0])
        self.historyY.append(position[1])
    #
    ## Method that computes new position
    # @param step Computed step that is to be added
    # @return np array  position (x,y) of newly computed position
    def computeNewPosition(self, step):
        # adding the step
        position = [0,0]
        position[0]         = np.round(self.position[0]+step[0], decimals=0)
        position[1]         = np.round(self.position[1]+step[1], decimals=0)
        # assembling the result
        result      = np.array(position, dtype=int)
        # returning result
        return result
    #
    ## Method that corrects the position
    # @param position Position (x,y) to be checked
    # @return corrected position (x,y) within limits
    def correctPosition(self,position):
        # Function that verifies if coordinate is in range
        # @param coordinate Coordinate to be checked
        # @return Updated coordinate value
        def verifyCoordinate(coordinate):
            # init new coordinate value
            newValue    = 0
            # checks limits
            if coordinate > self.limit-1:
                newValue    = self.limit-2
            elif coordinate < 0:
                newValue    = 0
            else:
                newValue    = coordinate
            # returning the value
            return newValue
        # checks if the 'x' is in the limits of the solution space
        x   = verifyCoordinate(position[0])
        # checks if the 'y' is in the limits of the solution space
        y   = verifyCoordinate(position[1])
        # assembling the result
        result  = np.array([int(x),int(y)], dtype=int)
        return result
    #
    ## Method that generates a random step
    # @return Random couple of floats between -0.5 and 0.5
    def generateRandomStep(self):
        return (np.random.random(2)-0.5)*2
    #
    ## Method that computes the bias related to given success
    # @param success Success under evaluation
    # @return Computed bias actualStep
    def computeSuccessBias(self, success):
        # init the result
        bias    = np.array([0.0,0.0])
        # computing the bias
        currentSessionNum = self.currentSession + self.prevSessions
        bias    = success.computeSuccessBias(self.getPosition(),self.normExp,currentSessionNum,self.maxNumSessions)
        # returning result
        return bias
    #
    ## Method for deleting the least recalled failure
    def deleteLeastRecalledFailure(self):
        # init temparory variable
        minRecallFailure    = self.failures[0]
        # loop for going through all the failures in the list
        for failure in self.failures:
            if failure.recallCounter > minRecallFailure.recallCounter:
                minRecallFailure = failure
        # getting the index of the min recalled failure
        index = self.failures.index(minRecallFailure)
        # deleting the least recalled failures
        self.failures.pop(index)
    #
    ## Method for deleting the least recalled success
    def deleteLeastRecalledSuccess(self):
        # init temparory variable
        minRecallSuccess    = self.successes[0]
        # loop for going through all the successes in the list
        for success in self.successes:
            if success.recallCounter > minRecallSuccess.recallCounter:
                minRecallSuccess = success
        # getting the index of the min recalled success
        index = self.successes.index(minRecallSuccess)
        # deleting the least recalled success
        self.successes.pop(index)
    #
    ## Method to add and remove failures if the memory of the agent is full
    # @param sessionFeedback value of the failure 
    # @param radius circle radius of the failure: calculated by radius = 1.7e^(-11)+(5-1.7e^(-11))/(1+(x/0.24)^(2.58)) ## old
    # new formula of radius modified as below
    # @param sessionNum session number of the failure event
    def addFailure(self,sessionFeedback,radius,sessionNum):
        #check if the size of the array is reached
        if len(self.failures) <= self.maxFailureMemSize:
            # create failure
            x = sessionFeedback
            k = 5*(np.exp(-10))
            radius = k+(5-k)/(1+(x/0.25)**(2.5))
            #  adding the failure to the db
            failure  = Failure(sessionFeedback, self.position, radius, sessionNum)
            _print("Agent '%s' has a new failure: %s" % (self.name,str(failure)), debug=False)
            self.failures.append(failure)
            # linking to the session
            self.linkLearningToSession(failure)
            # linking to agent
            self.linkFailureToAgent(failure)
        else:
            # forget less recalled failure
            self.deleteLeastRecalledFailure()
    #
    ## method to add and remove success if the memory of the agent is full
    # @param sessionFeedback value of the success
    # @param sessionNum session number of the success event
    def addSuccess(self,sessionFeedback,sessionNum):
        removedSuccess         = []
        #check if the size of the array is reached
        if len(self.successes) <= self.maxSuccessMemSize:
            # create success
            success  = Success(sessionFeedback, self.position, sessionNum)
            _print("Agent '%s' has a new success: %s" % (self.name , str(success)), debug=False)
            self.successes.append(success)
            # linking to the session
            self.linkLearningToSession(success)
            # linking to agent
            self.linkSuccessToAgent(success)
        else:
            # forget less recalled success
            self.deleteLeastRecalledSuccess()
    #
    ## Method that checks whether the position is allowed
    # @param proposedPosition New proposed solution
    # @return Boolean that says whether position allowed: True --> allowed
    def isPositionAllowed(self,proposedPosition):
        # init the result
        result  = True
        # check if inside any failure
        for failure in self.recalledFailures[self.currentSession]:
            if failure.isAgentInside(proposedPosition):
                result = False
                _print("Agent '%s' in failure." % (self.name), debug=True)
        # returning result
        return result
    #
    ## Method for checking forgotten failures
    def checkForgottenFailures(self):
        toBeDeletedIds = []
        for i in range(len(self.failures)):
            #_print(i, len(self.failures))
            failure     = self.failures[i]
            if failure.isForgotten():
                _print("Agent '%s' has forgotten a failure: %s" % (self.name, str(failure)), debug=False)
                toBeDeletedIds.append(i)
                self.storeForgottenEvent(failure)
        # deleting the failures
        for i in reversed(toBeDeletedIds):
            self.failures.pop(i)
    #
    ##  Method for checking forgotten successes
    def checkForgottenSuccesses(self):
        toBeDeletedIds = []
        for i in range(len(self.successes)):
            #_print(i, len(self.successes))
            success     = self.successes[i]
            if success.isForgotten():
                _print("Agent '%s' has forgotten a success: %s" % (self.name, str(success)), debug=False)
                toBeDeletedIds.append(i)
                self.storeForgottenEvent(success)
        # deleting the successes
        for i in reversed(toBeDeletedIds):
            self.successes.pop(i)
    #
    ## Method for storing feedback
    # @param sessionFeedback value of the solution
    # @param sessionNum session number of the activity
    def storeFeedback(self,sessionFeedback,sessionNum):
        # It checks success or failure
        if sessionFeedback>=0 and sessionFeedback<=0.3:
            # call func for adding failure
            self.addFailure(sessionFeedback,self.rCircle,sessionNum)
        elif sessionFeedback > 0.6 and sessionFeedback<=1.0:
            # call func for adding success
            self.addSuccess(sessionFeedback,sessionNum)
        else:
            pass
    #
    ## Method that stores the value of the solution proposed by the agent
    # @param solutionValue Value of the solution proposed by the agent
    def storeSolutionValue(self, solutionValue):
        self.solutionValue      = solutionValue 
    #
    ## Method that checks whether agent is to retire
    # @return boolean, true when agent is still working, false when retires
    def isAgentRetired(self):
        # init the result
        result      = False
        # checks if the experience is less than 50
        if self.experience > MAX_AGE_LIMIT:
            print ("Agent '%s' retires" % (self.name))
            result      = True
        else:
            result      = False
        # returning result
        return result
    #
    ## Method called before the beginning of a session
    # @param currentSession number of the current session number
    # @param agents list of agents 
    def onSessionStart(self,currentSession,agents,effectiveCommunication,virtuality):
        self.agents     = agents
        # reset number of steps counter
        self.stepNum = 0
        # storing the current session number
        self.currentSession = currentSession
        # checking which failures are to be deleted
        self.checkForgottenFailures()
         # checking which successes are to be deleted
        self.checkForgottenSuccesses()
        # checking the recalled failures and successes
        self.checkRecalledEvents()
        # reset history to empty list
        self.historyX = []
        self.historyY = []
        # setting the intended position
        self.intendedPosition = self.position
        # setting the intended position
        self.positionSB = self.position
        # setting the team vituality value for an agent to calculate trust reduction
        self.virtuality = virtuality
        # setting the conflict value of an agent
        self.effectiveCommunication = effectiveCommunication
    #
    ## Method that does operations at the end of the session
    # @param sessionFeedback Value of the solution proposed by the agent
    # @param sessionNum updates at the end of the session
    def onSessionEnd(self, sessionFeedback, sessionNum):
        # update experience
        self.experience     = self.experience + 1
        # method called for storing feedback
        # self.storeFeedback(sessionFeedback,sessionNum)
        # update the number of session for the agent
        self.numSession += 1
        # update familiarity
        self.updateFamiliarity()
    #
    ## Method that checks the recalled events
    def checkRecalledEvents(self):
        # increasing the current session number by 10 for experienced agent
        currentSessionNum = self.currentSession + self.prevSessions
        # failures
        recalledFailures    = []
        for failure in self.failures:
            if failure.isRecalled(currentSessionNum, self.maxNumSessions):
                recalledFailures.append(failure)
                # adding to ORM information
                self.storeRecalledEvent(failure)
        self.recalledFailures[self.currentSession]  = recalledFailures
        # successes
        recalledSuccesses    = []
        for success in self.successes:
            if success.isRecalled(currentSessionNum, self.maxNumSessions):
                recalledSuccesses.append(success)
                # adding to ORM information
                self.storeRecalledEvent(success)
        self.recalledSuccesses[self.currentSession]  = recalledSuccesses
    # 
    #########################################
    #         SELF EFFICACY METHODS 
    #########################################
    ## Method for setting the self efficacy for agent
    # @param isMaxVoteAgent boolean if it is a max vote agent or not
    def updateSelfEfficacy(self,isIncreaseSE=False):
        if isIncreaseSE:
            if self.selfEfficacy < 1:
                # increasing the self efficacy of the max vote agent/s
                self.selfEfficacy +=  self.increaseSelfEfficacyDelta()
            else:
                pass
        else:
            # decreasing the self efficacy of the agents
            self.selfEfficacy -=  self.decreaseSelfEfficacyDelta()
        # calling the method to correct the self efficacy range
        self.correctSelfEfficacyRange()
    #
    ## Method for corrects self efficacy in range
    def correctSelfEfficacyRange(self):
        # correct the self efficacy if it is more than 1 to 0.9
        if self.selfEfficacy >= 1:
            self.selfEfficacy = 0.9
        # correct the self efficacy if it is less than 0 to 0
        elif self.selfEfficacy < 0:
            self.selfEfficacy = 0.0
        else:
            pass
    #
    ## Method that gives back the self efficacy for agent
    # @returns float value of the change in self efficacy
    def getSelfEfficacy(self):
        return self.selfEfficacy
    #
    ## Method to compute self efficacy change
    # @return float value of the  change in self-efficacy of an agent
    def increaseSelfEfficacyDelta(self):
        # computing the curve based s curve where y= x/(x + e^(1-7x))
        # x is self efficacy of the agent 
        x                   = self.selfEfficacy
        Numerator           = x
        Denominator         = x + (np.exp(1-(7*x)))
        # y is change in self efficacy value
        y                   = Numerator / Denominator
        delta               = y * 0.1
        return delta
    #
    def decreaseSelfEfficacyDelta(self):
        # computing the curve based s curve where y= x'/(x' + e^(1-7x')) where x' =(1-x)
        # new delta for decrease y =((0.25)^x)0.05
        # x is self efficacy of the agent 
        x                   = self.selfEfficacy
        # fitting and inversing the curve
        #X                   = 1 - x
        #Numerator           = X
        #Denominator         = X + (np.exp(1-(7*X)))
        # y is change in self efficacy value
        y                   = np.power(0.25,x)
        delta               = y * 0.05
        return delta
    #
    ########################################
    #         FAMILIARITY METHODS 
    #########################################
    ## Method for updating familiarity
    def updateFamiliarity(self):
        for agent in self.agents:
            # checking if the agent is self then pass
            if agent == self:
                pass
            else:
                # the two agents have been together before ie the other agent's id already exist, then increasing the familiarity as +1
                if agent.ormObject.id in self.familiarity.keys():
                    self.familiarity[agent.ormObject.id] += 1
                # the two agents have never been together ie the other agent's id doesn't exist,then creating the familiarity as 1
                else:
                    self.familiarity[agent.ormObject.id] = 1
    #
    ## Method that gives back the familiarity value for agent
    # @return a dictionary where keys are teh agent orm id and values are the familiarity scores
    def getFamiliarity(self,agent):
        if agent.ormObject.id in self.familiarity.keys():
            familiarity     =  self.familiarity[agent.ormObject.id]
        else:
            familiarity     = 0
        return familiarity
    # 
    ########################################
    #         REPUTATION METHODS 
    #########################################
    ## Method for increasing accepted solution
    def increaseAcceptedSolutions(self):
        self.acceptedSolutions += 1
    #
    ## Method for increasing proposed solution
    def increaseProposedSolutions(self):
        self.proposedSolutions += 1
    ## Method that gives back the reputation for agent
    # @returns float value of the reputation
    def getReputation(self):
        return self.reputation
    #
    ## Method for computing reputation of agent
    def setReputation(self):
        if self.proposedSolutions == 0:
            self.reputation = self.reputation
        else:
            self.reputation = self.acceptedSolutions/self.proposedSolutions
            if self.reputation >= 1:
                self.reputation = 0.9
            else:
                pass
        #print("new reputation agent ", self.name, self.reputation)
    #
    #########################################
    #         TRUST METHODS 
    #########################################
    ## Method for computing trust of the other agents for  an agent
    # @param agents list of agents
    def computeTrust(self):
        for agent in self.agents:
            if agent == self:
                self.trust[agent.ormObject.id] = 1
            else:
                # compute trust if the agents has been there for more than 3 sessions otherwise 0
                if self.numSession >= 2:
                    # trust of that agent is the mean of its familiarity and reputation values
                    # 20 is given as the maxSession value for the simulation
                    familiarityValue    = self.getFamiliarity(agent)
                    familiarityValue    = familiarityValue/20
                    self.trust[agent.ormObject.id]   = np.average([agent.reputation,familiarityValue],weights=[0.75,0.25])
                    # trust reduction based on virtuality
                    self.trust[agent.ormObject.id] = self.trust[agent.ormObject.id]*self.getTrustReductionFactor(self.virtuality)
                else:
                    self.trust[agent.ormObject.id]   = 0
    #
    ## Method for reducing trust based on virtuality of a session
    # @param virtuality team virtuality value
    # @return trustReductionFactor a float value that reduces trust
    def getTrustReductionFactor(self,virtuality):
        # the trust remains same when face to face collaboration
        if 0<= virtuality <= 1:
            trustReductionFactor = 1
        elif 1< virtuality <=2:
            trustReductionFactor = 0.9
        elif 2< virtuality <=3:
            trustReductionFactor = 0.8
        # the trust remains on 70% when all virtual collaboration
        elif 3< virtuality <=4:
            trustReductionFactor = 0.7
        else:
            print("INVALID ENTRY")
        return trustReductionFactor
    #       
    #########################################
    #         INFLUENCER METHODS 
    #########################################
    ## Method for computing influencer bias for an agent
    def getInfluenceBias(self):
        return self.influencers
    #
    ## Method for computing influencer bias for all agents
    # @return influence bias
    def updateInfluencerBias(self):
        # influencer = self.findInfluencer()
        for agent in self.agents:
            if agent == self:
                self.influencers[agent.ormObject.id]    = 0.0
            else:
                self.influencers[agent.ormObject.id]    = self.computeInfluencerBias(agent)
    #
    ## Method for computing influencer bias of one agent
    # @return influence bias
    def computeInfluencerBias(self,influencer):
        # computing the height of the intensity curve which is the trust value ofthe influencer
        self.computeTrust() 
        trust   = self.trust[influencer.ormObject.id]
        # distance between the Ax-Ix and Ay-Iy (agent and influencer)
        deltaX  = influencer.position[0]-self.position[0]
        deltaY  = influencer.position[1]-self.position[1]
        # get influenece value
        influenceValue = self.getInfluenceCurve(trust,influencer)
        # computing the angle for the influence bias step
        if deltaX == 0:
            # checking condition
            if deltaY == 0:
                # the angle and the influence value =0 if two agents are in the same position
                angle   = 0
                influenceValue   = 0
            elif deltaY > 0:
                angle   = np.pi/2.0
            elif deltaY < 0:
                angle   = -np.pi/2.0
        else:
            # computing the value
            angle   = np.arctan(float(deltaY)/float(deltaX))
            # correcting the quadrant (arctan works only in two)
            if deltaX < 0:
                angle    += np.pi
        influenceBiasX    = influenceValue * np.cos(angle)
        influenceBiasY    = influenceValue * np.sin(angle)
        # distance between the influencer position and agent position
        ibias= np.array([influenceBiasX,influenceBiasY])
        _print("Agent position: %s" % (str(self.position)), debug=True)
        _print("Influencer position: %s" % (str(influencer.position)), debug=True)
        _print("Computed influence bias: %s" % (str(ibias)), debug=True)
        return ibias
    #
    ## Method for computing influencer values for an agent for the agent
    # @param agent influence value of that agent
    def getInfluenceValues(self,agent):
        return self.influencersValue[agent.ormObject.id]
    #
    ## Method for computing influencer values for all agents
    # @return influence bias
    def updateInfluencerValues(self):
        self.computeTrust()
        for agent in self.agents:
            trust   = self.trust[agent.ormObject.id]
            if agent == self:
                self.influencersValue[agent.ormObject.id]    = 0.0
            else:
                self.influencersValue[agent.ormObject.id]    = self.getInfluenceCurve(trust,agent)
    #
    ## Method for computing intensity curve
    # @param trust where trust(reputation,familiarity)
    # @return Intensity curve value
    def getInfluenceCurve(self,trust,influencer):
        if influencer == self:
            influenceCurve = 0
        else:
            # computing the influence curve where y= x^1.5
            # x is delta self efficacy of influencer and agent
            deltaSE             = influencer.selfEfficacy - self.selfEfficacy
            if deltaSE>0:
                x   = deltaSE
                y   = np.power(x,1.5)
            else: 
                y   = 0.0 
            # trust is the float value between 0 and 1 depending on reputation and familiarity values
            influenceCurve      = (((0.3)*y) + ((0.4)*trust) + ((0.3)*influencer.selfEfficacy))
        if influenceCurve >= self.reduceInfluenceFactor(influencer):
            influenceCurve      = influenceCurve - self.reduceInfluenceFactor(influencer) + 0.5*(self.effectiveCommunication)**2
        else:
            influenceCurve = 0.0 + 0.5*(self.effectiveCommunication)**2
        return influenceCurve
    #
    ## Method for selecting the number of influencers
    # @return the number of agents with the highest influence value
    '''def selectInfluencers(self):
        influencersByValueSorted = sorted(self.influencersValue.items(), key=lambda x: x[1],reverse=True)[0:self.influencerNumber]
        return dict(influencersByValueSorted)'''
    #
    ## Method for calculating the reduction factor for the influence value
    # @param agent
    # @return influenceReduceFactor reduced factor for the influence
    def reduceInfluenceFactor(self,agent):
        # getting the conflict value
        conflictValue = self.calculateConflictFactor(self.effectiveCommunication,MAXCURVESLOPE,agent)
        # calculating the influence reduction factor
        influenceReduceFactor = 0.5*(conflictValue**2)
        return influenceReduceFactor
    #
    #########################################
    #           AGREEMENT VALUE METHODS 
    #########################################
    ## Method for updating previous agreements
    def updatePreviousAgreements(self,newValue,agent):
        #for agent in self.agents:
        # checking if the agent is self then pass
        if agent == self:
            pass
        else:
            # the two agents have been together before ie the other agent's id already exist, then increasing the prev agreement value
            if agent.ormObject.id in self.prevAgree.keys():
                self.prevAgree[agent.ormObject.id] = newValue
            # the two agents have never been together ie the other agent's id doesn't exist,then creating the key as new value 
            else:
                self.prevAgree[agent.ormObject.id] = newValue
    #
    ## Method that gives back the previous agreement value for agent of other agent
    # @return a float agreement value
    def getPreviousAgreements(self,agent):
        if agent.ormObject.id in self.prevAgree.keys():
            prevAgree     =  self.prevAgree[agent.ormObject.id]
        else:
            prevAgree     = 0
        return prevAgree
    #
    #########################################
    #           CONFLICT FACTOR METHODS
    #########################################
    #
    ## Method that calculates the conflict value
    # @param effectiveCommunication value of the communication eefectiveness
    # @param maxCurveSlope max curve slope when the delta SE is 0
    # @param agent agent to get the difference in the SE of the two agent
    # @return conflict a conflict float value 
    def calculateConflictFactor(self,effectiveCommunication,maxCurveSlope,agent):
        # taking the absolute value of the delta SE
        deltaSelfEfficacy       = abs(self.selfEfficacy - agent.selfEfficacy)
        sumSelfEfficacy         = self.selfEfficacy + agent.selfEfficacy
        # when SE1 and SE2 are high (i.e. both agents have high SE), and Delta is low 
        if sumSelfEfficacy >= 0.8 and deltaSelfEfficacy < 0.5:
            # calculating the shape of the curve
            curveShape          = maxCurveSlope + (deltaSelfEfficacy*2)
            # calculating the conflict factor
            conflictFactor      = curveShape/((curveShape-1)+ np.exp((curveShape/2)*effectiveCommunication))
        # when SE1 and SE2 are high (i.e. one agent have high SE), and Delta is high 
        elif sumSelfEfficacy >= 0.8 and deltaSelfEfficacy >= 0.5:
            # calculating the shape of the curve
            curveShape          = (maxCurveSlope+2) + (deltaSelfEfficacy*2)
            # calculating the conflict factor
            conflictFactor      = curveShape/((curveShape-1)+ np.exp((curveShape)*effectiveCommunication))
        # when SE1 and SE2 are high (i.e. both agents have low SE), and Delta is low 
        elif sumSelfEfficacy <= 0.8 and deltaSelfEfficacy < 0.5:
            # calculating the shape of the curve
            curveShape          = (maxCurveSlope+1) + (deltaSelfEfficacy*2)
            # calculating the conflict factor
            conflictFactor      = curveShape/((curveShape-1)+ np.exp((curveShape/1.5)*effectiveCommunication))
        # when SE1 and SE2 are low (i.e. one agent have little higher SE than other), and Delta is high 
        else:
            # calculating the shape of the curve
            curveShape          = (maxCurveSlope+1) + (deltaSelfEfficacy*2)
            # calculating the conflict factor
            conflictFactor      = curveShape/((curveShape-1)+ np.exp((curveShape/1.5)*effectiveCommunication))
        # getting a random number
        randomNum           = np.random.random(1)
        # getting the probalility of the conflict to occur by checking if a random number is less that conflict factor
        if randomNum < conflictFactor:
            conflict = conflictFactor
        else:
            conflict = 0
        return conflict  
    #
    #########################################
    #           ORM METHODS 
    #########################################
    ## Method that creates a new agent instance in the database
    def storeInDatabase(self):
        # creating the ORM object
        agent       = ModelAgent()
        agent.name  = self.name
        # storing the ORM object in Python object
        self.ormObject  = agent
        # adding the object to the session
        databaseSession.add(agent)
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()
    #
    ## Method that links a learning event to the related session
    # @param event Event to be stored
    def linkLearningToSession(self, event):
        # getting the current session
        currentSession  = self.ormObject.sessions[-1]
        # connecting the two elements
        event.ormObject.createdSession  = currentSession
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()
    #
    ## Method that links the failure to the agent
    # @param failure Failure object to be linked
    def linkFailureToAgent(self, failure):
        # adding the agent
        self.ormObject.failures.append(failure.ormObject)
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()
    #
    ## Method that links the success to the agent
    # @param success Success object to be linked
    def linkSuccessToAgent(self, success):
        # adding the agent
        self.ormObject.successes.append(success.ormObject)
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()
    #
    ## Method that records when an event is forgotten
    # @param event Event that is forgotten
    def storeForgottenEvent(self, event):
        # getting the current session
        currentSession  = self.ormObject.sessions[-1]
        # connecting the two elements
        event.ormObject.forgottenSession  = currentSession
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()
    #
    ## Method that records when an event is recalled
    # @param event Event that is recalled
    def storeRecalledEvent(self, event):
        # getting the current session
        currentSession  = self.ormObject.sessions[-1]
        # connecting the two elements
        event.ormObject.recalledSession.append(currentSession)
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()
    #
    ## Method that stores the step in the ORM
    # @param position Position of the agent after the step
    # @param intendedPosition Intended position of the agent after the step
    # @param positionSB Position of the agent after only success bias
    # @param stepNumber Number of the current step
    # @param actualStep Final step
    # @param successBiasStep Step after only success bias
    # @param intendedStep Intended step before bias and corrections
    # @param successesBias Bias due to the successes
    # @param influenceBias Bias due to the influencer
    def storeStepInOrm(self,position,intendedPosition,positionSB,stepNumber,actualStep,successBiasStep,intendedStep,successesBias,sumInfluencersBias):
        # getting the current session
        currentSession  = self.ormObject.sessions[-1]
        # creating new step
        step = ModelIdeaGenerationStep()
        step.number             = stepNumber
        step.position           = position
        step.positionSB         = positionSB
        step.intendedPosition   = intendedPosition
        step.actualStep         = actualStep
        step.successBiasStep    = successBiasStep
        step.intendedStep       = intendedStep
        step.successesBias      = tuple(successesBias)
        step.sumInfluencersBias = tuple(sumInfluencersBias)
        # adding step to the session
        currentSession.iGSteps.append(step)
        # adding step to the agent
        self.ormObject.iGSteps.append(step)
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()
    #
        

if __name__ ==  "__main__":
    pass
