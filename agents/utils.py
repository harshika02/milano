# standard imports
import numpy as np


## function to generate agents way to explore solution space
# @param numSteps maximum number of steps in the curve
# @return curve (numpy array)
def generateExplorationCurve(numSteps):
    # generating a random base line
    baseline    = np.round(np.random.uniform(0.0,0.4),3)
    sigma       = np.round(np.random.uniform(0.6,0.8),2)
    maxHeight   = np.round(np.random.uniform(0.5,1.0),2)
    nu          = 0.0
    # creating curve
    x           = np.arange(1,numSteps+1, 1)
    x_prime     = x/7.0     # x expansion
    y           = (1.0/(x_prime*sigma*np.sqrt(2*np.pi)))*np.exp(-np.power(np.log(x_prime-nu),2)/(2*np.power(sigma,2)))
    # avoiding nan, replacing with 0
    y[np.isnan(y)] = 0.0
    # adding the baseline to the curve
    y += baseline
    # print (y[0])
    # print ("base line",baseline)
    # print ("Max Y", np.max(y), np.min(y))
    # print ("sigma",sigma)
    # normalising to 1 the maximum
    y           = maxHeight * y / np.max(y)
    # returning results
    return y

if __name__ == "__main__":
    # imports
    import matplotlib.pyplot as plt
    # generate curve
    explorationCurve0 = generateExplorationCurve(20)
    # plotting the curve
    plt.plot(explorationCurve0)
    plt.show()
