# standard imports


# custom imports
from model_analysis.model_extractor import DataExtractor
from model_analysis.model_extractor_IS import DataExtractorIdeaSelection
from model_analysis.model_analyser import DataAnalyser
#from model_analysis.model_analyser_IS import DataAnalyserIdeaSelection
from model_analysis.constants import *


if __name__ == '__main__':
    # declaring extractor and analyser
    dataExtractor   = DataExtractor()
    #dataAnalyser    = DataAnalyser()
    dataExtractorIS = DataExtractorIdeaSelection()
    #dataAnalyserIS  = DataAnalyserIdeaSelection()

    # running experiment 1
    if EXPERIMENT1_ENABLE:
        # experiment 1
        dataExtractor.extractExperiment1Dataset()
        #dataAnalyser.analyseExperiment1()
    # running experiment 2
    if EXPERIMENT2_ENABLE:
        # experiment 2
        dataExtractor.extractExperiment2Dataset()
        #dataExtractor.extractingLowestSEagentData()
        #dataAnalyser.analyseExperiment2()
    # running experiment 3
    if EXPERIMENT3_ENABLE:
        # experiment 3
        dataExtractor.extractExperiment3Dataset()
        #dataAnalyser.analyseExperiment3()
    # running experiment 4
    if EXPERIMENT4_ENABLE:
        # experiment 4
        dataExtractor.extractExperiment4Dataset()
        #dataAnalyser.analyseExperiment4()
    # running experiment 5
    if EXPERIMENT5_ENABLE:
        # experiment 5
        dataExtractor.extractExperiment5Dataset()
        #dataAnalyser.analyseExperiment5()
        #dataAnalyser.plotBars()
    #if EXPERIMENT6_ENABLE:
        # experiment 6
        #dataExtractor.extractExperiment5Dataset()
        #dataAnalyser.analyseExperiment6()
    #if EXPERIMENT7_ENABLE:
        # experiment 7
        #dataAnalyser.analyseExperiment7()
        #dataAnalyser.plotBars()
    #if EXPERIMENT8_ENABLE:
        # experiment 8
        #dataExtractor.extractExperiment5Dataset()
        #dataAnalyser.analyseExperiment8()
    #if EXPERIMENT9_ENABLE:
        # experiment 9
        #dataAnalyser.analyseExperiment9()
        #dataAnalyser.plotBars()
    ############################################
    ##### Idea selection experiments
    ############################################
    if EXPERIMENT1_IS_ENABLE:
        # experiment 1 for idea selction
        dataExtractorIS.extractISExperiment1Dataset()
        #dataAnalyserIS.analyseISExperiment1_2Dataset()
    if EXPERIMENT2_IS_ENABLE:
        # experiment 2 for idea selction
        dataExtractorIS.extractISExperiment2Dataset()
        #dataAnalyserIS.analyseISExperiment2Dataset()
    if EXPERIMENT3_IS_ENABLE:
        # experiment 3 for idea selction
        dataExtractorIS.extractISExperiment3Dataset()
        #dataAnalyserIS.analyseISExperiment3Dataset()
    if EXPERIMENT4_IS_ENABLE:
        # experiment 4 for idea selction
        dataExtractorIS.extractISExperiment4Dataset()
        #dataAnalyserIS.analyseISExperiment4Dataset()
    if EXPERIMENT5_IS_ENABLE:
        # experiment 5 for idea selction
        dataExtractorIS.extractISExperiment5Dataset()
        #dataAnalyserIS.analyseISExperiment5Dataset()
        #dataAnalyserIS.plotBars()
        