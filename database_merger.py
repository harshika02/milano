# standard imports
import sqlite3
import os
from shutil import copyfile


## Class that implements the merging of multiple databases
class DatabaseMerger():
    OUTPUT_DATABASE_FILENAME = "project.db"
    #
    ## Constructor method
    def __init__(self):
        pass
    #
    ## Method that merges databases
    def mergeDatabases(self):
        # init attributes
        self.databaseCount = 0
        # get the list of databases
        self.getDatabasesList()
        # check if to merge
        if len(self.databases) <= 1:
            print("Nothing to merge.")
            return
        # select main database
        self.prepareDatabases()
        # get tables names
        self.tables = getTableNames(self.OUTPUT_DATABASE_FILENAME)
        # compute list of databases that have correct schemas
        self.checkDatabases()
        # check if still something to merge
        if len(self.databases) <= 1:
            print("Nothing to merge, inconsistencies in schema.")
            return
        # connect to main database
        self.initMainConnection()
        # attach databases
        self.attachDatabases()
        # merge databases
        for i in range(len(self.databasesAliases)):
            self.mergeDatabase(self.databasesAliases[i])
        # close connection to main database
        self.closeMainConnection()
    #
    ## Method that gives list of all available databases
    def getDatabasesList(self):
        # get list of all files in folder
        self.databases = []
        for fileName in os.listdir():
            if fileName.endswith(".db"):
                self.databases.append(fileName)
    #
    ## Method that prepares the databased for merger
    def prepareDatabases(self):
        # get main database
        mainDatabaseFile = self.databases[0]
        # pop out of list
        self.databases.pop(0)
        # check if exists already the main database
        if os.path.exists(self.OUTPUT_DATABASE_FILENAME):
            #os.remove(self.OUTPUT_DATABASE_FILENAME)
            raise Exception
        # rename the main file
        copyfile(mainDatabaseFile, self.OUTPUT_DATABASE_FILENAME)
        #os.rename(mainDatabaseFile, self.OUTPUT_DATABASE_FILENAME)
    #
    ## Method that filters databases with different schema
    def checkDatabases(self):
        # init list of databases
        newDatabases = []
        # check schemas for each database
        for i in range(0, len(self.databases)):
            # get list of tables
            temp = getTableNames(self.databases[i])
            # check if the schema is same
            if (self.tables != temp):
                print("Tables do not match in non-primary database: %s" % self.databases[i])
                continue
            else:
                newDatabases.append(self.databases[i])
        # replace attribute
        self.databases = newDatabases
    #
    ## Method that inits connection to main database
    def initMainConnection(self):
        # init connection to main database
        self.mainConnection = sqlite3.connect(self.OUTPUT_DATABASE_FILENAME)
        self.mainCursor = self.mainConnection.cursor()
    #
    ## Method that inits connection to main database
    def closeMainConnection(self):
        # close connection to main database
        self.mainCursor.close()
        self.mainConnection.close()
    #
    ## Method that attaches databases to main one
    def attachDatabases(self):
        # init list of aliases
        self.databasesAliases = []
        # attach databases
        for i in range(0, len(self.databases)):
            # get name of database
            databaseName = self.databases[i]
            print(("Attaching database: %s") % databaseName)
            # create new name for database
            databaseAlias = 'db%d' % (self.databaseCount)
            # attach database
            self.mainCursor.execute("ATTACH DATABASE ? as ? ;", (databaseName, databaseAlias))
            # store alias in list
            self.databasesAliases.append(databaseAlias)
            # update counter
            self.databaseCount += 1
    #
    ## Method that merges database to main one
    def mergeDatabase(self, databaseAlias):
        # iterate on tables
        for i in range(0, len(self.tables)):
            # get name of table
            tableName = self.tables[i]
            # get columns in tables
            columns = self.getColumnNames(tableName)
            # merge the table
            self.mergeTable(databaseAlias, tableName, columns)
    #
    ## Method that gets the column names of a table
    # @param tableName Name of the table
    # @return List of the column names
    def getColumnNames(self, tableName):
        # prepare statement
        statement = "PRAGMA table_info(%s);" % str(tableName)
        # get list of columns
        self.mainCursor.execute(statement)
        temp = self.mainCursor.fetchall()
        # clean list of columns
        columns = []
        for i in range(0, len(temp)):
            columns.append(temp[i][1])
            '''
            if (("id" in temp[i][1]) | ("ID" in temp[i][1])):
                continue
            else:
                columns.append(temp[i][1])
            '''
        # return result
        return columns
    #
    ## Method that merges a table
    def mergeTable(self, databaseAlias, tableName, columns):
        # compose name of alias' table
        dbNameTableName = databaseAlias + "." + tableName
        # get elements in alias table
        statement = "SELECT %s FROM %s;" % (getListFromString(columns), dbNameTableName)
        self.mainCursor.execute(statement)
        records = self.mainCursor.fetchall()
        # insert new records
        print(databaseAlias, tableName)
        for record in records:
            # insert the record
            fieldsPlaceholders = ",".join(["?"] * len(columns))
            statement = "INSERT INTO %s VALUES (%s)" % (tableName, fieldsPlaceholders)
            try:
                self.mainCursor.execute(statement, record)
            except Exception as e:
                print(str(e))
        # commit the modifications
        self.mainConnection.commit()
    #



# Function that gets the table names of a database
# @param dbName the name of the database file (i.e. "example.db")
# @return a string array of the table names
def getTableNames(databaseName):
    # connect to database
    connection = sqlite3.connect(databaseName)
    cursor = connection.cursor()
    # get tables list
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    temp = cursor.fetchall()
    # close the connection
    cursor.close()
    connection.close()
    # clean tables list
    tables = []
    for i in range(0, len(temp)):
        tables.append(temp[i][0])
    # return list
    return tables

## Function that generates a string from a list
def getListFromString(_list):
    # init result
    result = ""
    # check if empty list
    if len(_list) == 0:
        pass
    else:
        # convert elements of list in strings
        result = "`%s`" % _list[0]
        for element in _list[1:]:
            result += ",`%s`" % (element)
    # return result
    return result


if __name__ == "__main__":
    databaseMerger = DatabaseMerger()
    databaseMerger.mergeDatabases()