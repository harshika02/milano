# standard imports
import numpy as np

# custom imports
from learning.constants import *
from learning.recall import RecallCurve
from learning.utils import computeDeltaTime
from learning.constants import *

# model imports
from models.base import databaseSession
from models.agent import Agent as ModelAgent
from models.success import Success as ModelSuccess
from models.project import Project as ModelProject
from models.session import Session as ModelSession
from models.steps import IdeaGenerationStep as ModelIdeaGenerationStep
from models.failure import Failure as ModelFailure

## failure class 
class Failure():
    ## constructor method
    # @param value is the failure value
    # @param position is the failure position
    # @param radius is the failure circle radius
    # @param numSession is the session number of the failure
    def __init__(self, value, position, radius, numSession):
        self.value      = value
        self.position   = position
        self.radius     = radius
        self.numSession = numSession
        self.recallCounter  = 0
        # init the recall curve
        self.recallCurve = RecallCurve(self.value)
        # storing in the database
        self.storeInDatabase()
    #
    ## Method that computes the difference between the center of the failure and the position of the agent
    # @param agentPosition the position (x,y) of the agent
    def isAgentInside(self,agentPosition):
        # init the result
        result  = False
        # computing the deltas
        deltaX  = agentPosition[0]-self.position[0]
        deltaY  = agentPosition[1]-self.position[1]
        # computing the distance
        d       = np.sqrt(np.power(deltaX, 2)+np.power(deltaY, 2))
        # computing the outcome
        if d >= 0 and d <= self.radius:
            result  = True
        else:
            result  = False
        # returning the result
        return result
    #
    ## Method that computes if the failure is recalled
    # @param currentSession Current number of the session running
    # @param maxNumSessions Maximum session number
    # @return Boolean that says whether Failure is recalled (True) or not
    def isRecalled(self, currentSession, maxNumSessions):
        # taking a random number
        randomNumber= np.random.random(1)[0]
        # computing deltaTime
        deltaTime   = computeDeltaTime(currentSession, self.numSession, maxNumSessions)
        # computer recall probability
        recallProbability   = self.recallCurve.computeRecallProbability(deltaTime)
        # get probability of recall
        if randomNumber>=0 and randomNumber <= recallProbability:
            # recall counter is reset to 0
            self.recallCounter  = 0
            return False
        else:
            # recall counter is increased by one
            self.recallCounter  = self.recallCounter  + 1
            return True
    ## Method for checking whether the failure is forgotten (permanently lost)
    # @return boolean, True when failure has to be forgotten
    def isForgotten(self):
        # init result
        result = False
        # check if the value of recall counter is more than max recall counter value
        if self.recallCounter >= MAX_RECALL_COUNTER:
            # True when it needs to be forgotten
            result = True
        return result
        #
    def __str__(self):
        return "Failure(value=%f, position=(%d,%d))" % (self.value, int(self.position[0]),int(self.position[1]))
    
    #########################################
    #           ORM METHODS 
    #########################################
    ## Method that creates a new failure instance in the database
    def storeInDatabase(self):
        # create a session
        failure             = ModelFailure()
        failure.position    = self.position
        failure.radius      = self.radius
        # storing the ORM object in Python object
        self.ormObject      = failure
        # adding the object to the session
        databaseSession.add(failure)
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()




if __name__ == "__main__":
    # testing failure
    failure = Failure(0.1,(1,4),0.5,0)
    failures = []
    failures.append(failure)
