# standard imports
import numpy as np
    

## class used for implementing the recall curve
class RecallCurve():
    LOW_PLATEAU_THRESHOLD       = 0.25  # point from where the curve of recall failure starts, after 0.25 value
    HIGH_PLATEAU_THRESHOLD      = 0.75  # point from where the cutve of recall success starts, after 0.75 value
    MAX_FAILURE_RECALL          = 0.45  # max recall probability of the worst failure
    MIN_FAILURE_RECALL          = 0.15  # min recall probability of the worst failure
    MAX_SUCCESS_RECALL          = 0.35  # max recall probability of the best success
    MIN_SUCCESS_RECALL          = 0.25  # min recall probability of the best success
    MAX_PLATEAU_RECALL          = 0.25  # max recall probability of the low threshold (failure/success point)
    MIN_PLATEAU_RECALL          = 0.10  # min recall probability of the low threshold (failure/success point)
    IMMEDIATE_RECALL            = 0.20  # Immediate/recent/short term recall threshold
    LONG_RECALL                 = 0.60  # long term recall threshold
    #
    ## constructor method
    # @param value is the value of the event (success or failure)
    def __init__(self, value):
        self.value  = value
    #
    ## method for computing recall probability
    # @param deltaTime difference between current trail number and the event session number divided by total trials(between 0-1)
    # @return recall probability
    def computeRecallProbability(self,deltaTime):
        # computing the curve
        (A,B,C,D)   =  self.computeRecallCurve(deltaTime) 
        # computing the slope on the recall curve in region 1 (value<=0.25)
        if self.value <= self.LOW_PLATEAU_THRESHOLD:
            slope   = (B-A)/self.LOW_PLATEAU_THRESHOLD
            recall  = (slope * self.value) + A
        # computing the slope on the recall curve in region 1 (value>=0.75)
        elif self.value >= self.HIGH_PLATEAU_THRESHOLD:
            slope   = (D-C) / (1-self.HIGH_PLATEAU_THRESHOLD)
            dx      = self.value - self.HIGH_PLATEAU_THRESHOLD
            recall  = slope * dx + C
        # computing the slope on the recall curve in region 1 (value between 0.25 and 0.75)
        else:
            recall  = B
        return recall
    #
    ## Method that computes the recall curve and returns major points
    # @param deltaTime difference between current trail number and the event session number divided by total trials(between 0-1)
    # @return Tuple of the four major points that describe the curve
    def computeRecallCurve(self, deltaTime):
        if deltaTime <= 0.2:
            #computing A (A= the first point of the curve for recent time recall)
            A   = self.MAX_FAILURE_RECALL
            #computing B (B= the first point of the curve recent time recall)
            B   = self.MAX_PLATEAU_RECALL
            #computing C (C= the first point of the curverecent time recall)
            C   = self.MAX_PLATEAU_RECALL
            #computing D (D= the first point of the curve recent time recall)
            D   = self.MAX_SUCCESS_RECALL
        elif 0.2<deltaTime<0.6:
            #computing A for 0.2<dt<0.6
            a    = np.array([[self.IMMEDIATE_RECALL,1],[self.LONG_RECALL,1]])
            b    = np.array([self.MAX_FAILURE_RECALL,self.MIN_FAILURE_RECALL])
            x    = np.linalg.solve(a,b)
            m    = x[0]
            q    = x[1]
            A    = m * deltaTime + q
            #computing B for 0.2<dt<0.6
            a    = np.array([[self.IMMEDIATE_RECALL,1],[self.LONG_RECALL,1]])
            b    = np.array([self.MAX_PLATEAU_RECALL,self.MIN_PLATEAU_RECALL])
            x    = np.linalg.solve(a,b)
            m    = x[0]
            q    = x[1]
            B    = m * deltaTime + q
            #computing C for 0.2<dt<0.6
            C    = B
            #computing D for 0.2<dt<0.6
            a    = np.array([[self.IMMEDIATE_RECALL,1],[self.LONG_RECALL,1]])
            b    = np.array([self.MAX_SUCCESS_RECALL, self.MIN_SUCCESS_RECALL])
            x    = np.linalg.solve(a,b)
            m    = x[0]
            q    = x[1]
            D    = m * deltaTime + q
        else:
            #computing A (A= the first point of the curve for long time recall)
            A   = self.MIN_FAILURE_RECALL
            #computing B (B= the first point of the curve long time recall)
            B   = self.MIN_PLATEAU_RECALL
            #computing C (C= the first point of the curve long time recall)
            C   = self.MIN_PLATEAU_RECALL
            #computing D (D= the first point of the curve long time recall)
            D   = self.MIN_SUCCESS_RECALL
        # returning the results
        return (A,B,C,D)

if __name__ == "__main__":
    # imports
    import matplotlib.pyplot as plt
    # generic curve list of values
    genericCurve = []
    # dictionary of dt and values
    curves = {}
    # creating list of 100 values for smapling 
    for i in range (100):
        value = float (i) / float (100)
        genericCurve.append(RecallCurve(value))
    # creating list of 10 dt for smapling 
    for i in range (10):
        dt = float (i) / float (10)
        # list of dt values
        newCurve = []
        for point in genericCurve:
            sampledPoint    = point.computeRecallProbability(dt)
            newCurve.append(sampledPoint)
        curves[dt] = newCurve
    # plotting of the curve
    value = np.arange(0,1.0,0.01)
    for key in curves.keys():
        curve = curves[key]
        plt.plot(value,curve)
        plt.title(key)
        plt.xlim((0.0,1.0))
        plt.ylim((0.0,1.0))
        plt.grid(True)
        plt.show()


    
    
