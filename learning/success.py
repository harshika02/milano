# standard imports
import numpy as np

# custom imports
from learning.recall import RecallCurve
from learning.utils import computeDeltaTime
from learning.constants import *
# importing NUMPIXELS
from abm_simulation.constants import *
from abm_simulation.utils import _print

# model imports
from models.base import databaseSession
from models.agent import Agent as ModelAgent
from models.success import Success as ModelSuccess
from models.project import Project as ModelProject
from models.session import Session as ModelSession
from models.steps import IdeaGenerationStep as ModelIdeaGenerationStep
from models.failure import Failure as ModelFailure

## success class
class Success():
    ## constructor method
    # @param value is the success value
    # @param position is the success position
    # @param numSession is the session number of the success
    def __init__(self,value,position,numSession):
        self.value          = value
        self.position       = position
        self.numSession     = numSession
        self.recallCounter  = 0
        self.recallCurve    = RecallCurve(self.value)
        # storing in the database
        self.storeInDatabase()
    #
    def __str__(self):
        return "Success(value=%f, position=(%d,%d))" % (self.value, int(self.position[0]),int(self.position[1]))
    #
    ## method that computes if the success is recalled
    # @param currentSession Current number of the session running
    # @param maxNumTrials Maximum session number
    # @return Boolean that says whether Success is recalled (True) or not
    def isRecalled(self, currentSession, maxNumTrials):
        # taking a random number
        randomNumber= np.random.random(1)[0]
        # computing deltaTime
        deltaTime   = computeDeltaTime(currentSession, self.numSession, maxNumTrials)
        # computer recall probability
        recallProbability   = self.recallCurve.computeRecallProbability(deltaTime)
        # get probability of recall
        if randomNumber>=0 and randomNumber <= recallProbability:
            # recall counter is reset to 0
            self.recallCounter  = 0
            return False
        else:
            # recall counter is increased by one
            self.recallCounter  = self.recallCounter  + 1
            return True
    #
    ## Method for checking whether the success is forgotten (permanently lost)
    # @return boolean, True when success has to be forgotten
    def isForgotten(self):
        # init result
        result = False
        # check if the value of recall counter is more than max recall counter value
        if self.recallCounter >= MAX_RECALL_COUNTER:
            # True when it needs to be forgotten
            _print("Recalled counter: %d" % self.recallCounter, debug=True)
            result = True
        return result
        #
    ## Method for computing success bias
    # @param agentPosition position of the agent 
    # @param agentAge professional age of the agent
    # @param deltaTime difference between the current and recelled event/ total session
    # @return success bias actualStep
    def computeSuccessBias(self, agentPosition, agentAge, currentSession, maxNumTrials):
        # computing deltaTime
        deltaTime   = computeDeltaTime(currentSession, self.numSession, maxNumTrials)
        # computing sigma
        sigma   = 0.8 - (0.2 * agentAge)
        # computing maxPeak with respect to deltaTime
        maxPeak = 1 - (0.7 * deltaTime)
        # distance between the Ax-Sx and Ay-Sy
        deltaX  = self.position[0]-agentPosition[0]
        deltaY  = self.position[1]-agentPosition[1]
        # computing the distance
        d       = np.sqrt(np.power(deltaX,2)+np.power(deltaY,2))
        # normalising the distance
        d       = d / (np.sqrt(2)*NUMPIXELS)
        # get intensity value 
        intensityValue = self.getIntensityCurve(d,sigma,maxPeak)
        # computing the angle for the bias actualStep
        if deltaX == 0:
            # checking condition
            if deltaY == 0:
                angle   = 0
            elif deltaY > 0:
                angle   = np.pi/2.0
            elif deltaY < 0:
                angle   = -np.pi/2.0
        else:
            # computing the value
            angle   = np.arctan(float(deltaY)/float(deltaX))
            # correcting the quadrant (arctan works only in two)
            if deltaX < 0:
                angle   += np.pi
        successBiasX    = intensityValue * np.cos(angle)
        successBiasY    = intensityValue * np.sin(angle)
        # distance between the recalled success position and agent position
        bias= np.array([successBiasX,successBiasY])
        _print("Agent position: %s" % (str(agentPosition)), debug=True)
        _print("Success position: %s" % (str(self.position)), debug=True)
        _print("Computed bias: %s" % (str(bias)), debug=True)
        return bias
    #
    ## method for computing intensity curve
    # @param distance Distance between the agent and the success
    # @param sigma Sigma value for the computation of the curve
    # @param maxPeak Maximum value of the intensity Curve
    # @return Intensity curve values
    def getIntensityCurve(self,distance,sigma,maxPeak):
        # computing the curve based on y= (1/((4x + 0.1) sigma (2pi)^.5) exp(−((ln(4x + 0.1)^2)/(2 * (sigma)^2 )))/0.7
        x                   = 4.0 * distance + 0.1
        mu                  = 0.0
        Numerator1          = 1.0/(x * sigma * np.sqrt(2.0 * np.pi))
        expNumerator        = -1.0 * np.power(np.log (x)-mu,2)
        expDenominator      = 2.0 * np.power(sigma,2)
        Numerator2          = np.exp(expNumerator / expDenominator)
        # assembling intensityCurve
        iCurve               = (Numerator1 * Numerator2) / 0.7
        # normalising intensityCurve
        intensityCurve         = maxPeak * (iCurve / (np.max(iCurve)))
        return intensityCurve
    
    #########################################
    #           ORM METHODS 
    #########################################
    ## Method that creates a new success instance in the database
    def storeInDatabase(self):
        # create a success
        success             = ModelSuccess()
        success.position    = self.position
        # storing the ORM object in Python object
        self.ormObject      = success
        # adding the object to the session
        databaseSession.add(success)
        # committing & flushing the database
        databaseSession.commit()
        databaseSession.flush()




if __name__ ==  "__main__":
    # imports
    import matplotlib.pyplot as plt
    from constants import *
    # testing success
    # testing intensity curve
    success = Success(0.1,(1,4),2)
    # spacing the distance between 0 and 1 at step size 0.01
    # 
    distance        = np.arange(0,1,STEPSIZE)
    # computing the curve
    maxPeak         = np.random.uniform(low=0.3, high=1.0, size=3)
    sigma           = np.random.uniform(low=0.6, high=0.8, size=3)
    successBiasIntensity0    = success.getIntensityCurve(distance,sigma[0],maxPeak[0])
    successBiasIntensity1    = success.getIntensityCurve(distance,sigma[1],maxPeak[1])
    successBiasIntensity2    = success.getIntensityCurve(distance,sigma[2],maxPeak[2])
    successBias = success.computeSuccessBias([0.3,0.8],0.1,3,10)
    # plotting the curve
    plt.plot(distance,successBiasIntensity0, 'k')
    plt.plot(distance,successBiasIntensity1, 'r')
    plt.plot(distance,successBiasIntensity2, 'g')
    plt.grid(True)
    plt.show()