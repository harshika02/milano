# standard imports



## method for cumputing deltaTime
# @param currentSession Current number of the session
# @param eventTrialNumber Session number of the event being recalled
# @param maxNumTrials Maximum session number
# @return deltaTime
def computeDeltaTime(currentSession, eventTrialNumber, maxNumTrials):
    deltaTime   = (currentSession-eventTrialNumber)/maxNumTrials
    return deltaTime
