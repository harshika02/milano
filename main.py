# standard imports
import os
from multiprocessing import Process, Queue
import tqdm 
import names
import numpy as np

# custom imports
from abm_simulation.solution_space import createSolutionSpace,createSolutionSpaceExp
from models.base import databaseName
from models.model import createDatabase
from abm_simulation.constants import *
from agents.agent import Agent
from abm_simulation.project import Project


# SETTINGS
NUM_PROJECTS_EXP    = 0
NUM_PROJECTS_ACTUAL = 200
NUM_PROJECTS        = 200
NUM_PARALLEL_JOBS   = 4
solutionSpaceExp = createSolutionSpaceExp()
solutionSpaceAll = createSolutionSpace()
#PROJECT_NUM         = 0

# self efficacies to be assigned
selfEfficacies = [
    0.42, 
    0.43,
    0.435,
    0.419, 
    0.422,
    0.128,
    0.11,
    0.137,
    0.125,
    0.122
]
NUMSESSIONS = 10
NUMAGENTS   = 6
NUM_NEW_AGENTS = 6
NUM_EXP_AGENTS = 0

## Class that implements the simulation
class SimulationManager():
    #
    ## Constructor method
    def __init__(self):
        pass
    #
    ## Method that runs the main function
    '''def runMain(self):
        # init database
        #initDatabase()
        # preparing batch of jobs
        NUM_BATCHES = int(NUM_PROJECTS / NUM_PARALLEL_JOBS)
        # create queue
        self.queue = Queue()
        self.pbarProcess = Process(target=progressBarListener, args=(self.queue,))
        self.pbarProcess.start()
        # create processes
        self.processes = []
        for j in range(NUM_PARALLEL_JOBS):
            p = Process(target=runProcess, args=(NUM_BATCHES, self.queue))
            self.processes.append(p)
        # starting the process
        for process in self.processes:
            process.start()
        # joining processes
        for process in self.processes:
            process.join()
        # killing the processes
        for process in self.processes:
            process.terminate()
        # stop process bar process
        self.pbarProcess.terminate()
    #

## Function that manages the progress bar
def progressBarListener(queue):
    # init progress bar
    pbar = tqdm.tqdm(total = NUM_PROJECTS)
    # wait for messages
    for item in iter(queue.get, None):
        pbar.update()

## Method that runs a process
def runProcess(numBatches, queue):  
    # run the projects 
    for _ in range(numBatches):
        # run project
        runProject()
        # update pbar
        queue.put(1)
    #'''


## Function that runs a project
def runProject():
    # run first X projects  (e.g. 10)
    experiencedAgents = []
    for _ in range(NUM_PROJECTS_EXP):
        # creating new agents for each project for 80 times
        agents = createNewAgents(NUMAGENTS, NUMSESSIONS, selfEfficacies)
        # creating a project 
        # giving different solution space to exp agents
        project = Project(numSessions=NUMSESSIONS, agents=agents,solutionSpace=solutionSpaceExp)
        project.onStart()
        project.run()
        project.onEnd(NUM_PROJECTS_EXP)
        # getting the experiencedAgents stored in a list
        for expAgent in project.expAgents:
            # creating a list of 3x80 exp agents
            experiencedAgents.append(expAgent)
    # run main projects (e.g. 200)
    for _ in range(NUM_PROJECTS_ACTUAL):
        indices = []
        # create new agents
        newAgents   = createNewAgents(NUM_NEW_AGENTS, NUMSESSIONS, selfEfficacies)
        # randomly selecting from the experienced agents list
        expAgentsSelected = np.random.choice(experiencedAgents, NUM_EXP_AGENTS, replace=False)
        # getting the mix agent list
        #agents      = createMixAgents(newAgents, expAgentsSelected)
        agents       = newAgents
        for ae in expAgentsSelected:
            # getting the idexes to be popping the selected experienced agent or agents from the experiencedAgents list
            index = experiencedAgents.index(ae)
            indices.append(index)
        # run projects
        project = Project(numSessions=NUMSESSIONS, agents=agents,solutionSpace=solutionSpaceAll)
        project.onStart()
        project.run()
        project.onEnd(NUM_PROJECTS_EXP)
        # deleting the indices from the list
        #newExperiencedAgentList = [v for i, v in enumerate(experiencedAgents) if i not in indices]
        # changing old list to the new list
        #experiencedAgents = newExperiencedAgentList
#
## Function for creating new agents
def createNewAgents(numAgents, numSessions, selfEfficacies):
    # creating agents
    agents         = []
    for i in range(numAgents):
        # generating agent name
        agentName   = names.get_full_name()
        # creating an agent
        agent   = Agent(                    
            name            = agentName,    
            limit           = NUMPIXELS,    
            maxNumSessions  = numSessions
        )
        # assigning a value of self efficacy
        if len(selfEfficacies)!=0:
            agent.selfEfficacy = selfEfficacies[i]
        # storing agent experience as False
        agent.isExperienced = False
        # appending the agent to the pool of agents
        agents.append(agent)
    return agents
#
## Function for creating a agents old and new
def createMixAgents(newAgents,expAgentsSelected):
    #  init mix agents list
    mixedAgents = []
    # adding the agents to the mix list
    for an in newAgents:
        # adding new agents to the mix agents list
        mixedAgents.append(an)
    for ae in expAgentsSelected:
        # adding experienced agents to the mix agents list
        mixedAgents.append(ae)
    return mixedAgents
#
## Functions that calls a method to reset experienced agents
# @param experiencedAgents a list of experienced agents
# @param NUMSESSIONS is an integer total number of sessions
def resetExpAgents(experiencedAgents,NUMSESSIONS):
    for expAgent in experiencedAgents:
        expAgent.setExperiencedAgents(NUMSESSIONS)
#
## Function that asserts new clean database is ready
def initDatabase():
    # check if database already exists
    if os.path.exists(databaseName):
        os.remove(databaseName)
    # create database
    createDatabase()


## Main function
def main():
    pass

if __name__ == "__main__":
    #simulationManager = SimulationManager()
    #simulationManager.runMain()
    runProject()
