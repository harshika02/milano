# FEATURE FLAGS
## Enabler for experiment 1
EXPERIMENT1_ENABLE      = False
## Enabler for experiment 2
EXPERIMENT2_ENABLE      = False
## Enabler for experiment 3
EXPERIMENT3_ENABLE      = False
## Enabler for experiment 4
EXPERIMENT4_ENABLE      = False
## Enabler for experiment 5
EXPERIMENT5_ENABLE      = False
## Enabler for experiment 6
EXPERIMENT6_ENABLE      = False
## Enabler for experiment 7
EXPERIMENT7_ENABLE      = False
## Enabler for experiment 8
EXPERIMENT8_ENABLE      = False
## Enabler for experiment 9
EXPERIMENT9_ENABLE      = False
###################################
# Experiments for Idea selection
###################################
## Enabler for experiment 1 idea selection
EXPERIMENT1_IS_ENABLE      = False
## Enabler for experiment 2 idea selection
EXPERIMENT2_IS_ENABLE      = False
## Enabler for experiment 3 idea selection
EXPERIMENT3_IS_ENABLE      = False
## Enabler for experiment 4 idea selection
EXPERIMENT4_IS_ENABLE      = False
## Enabler for experiment 5 idea selection
EXPERIMENT5_IS_ENABLE      = False

# names of files for each experiment
EXPERIMENTS_ROOTFOLDER      = "model_analysis/"
EXP_ROOTFOLDERNAME          = EXPERIMENTS_ROOTFOLDER + "idea_generation_output/exp_%d/"
EXP_ROOTFOLDERNAME_IS       = EXPERIMENTS_ROOTFOLDER + "idea_selection_output/exp_%d/"
EXPERIMENT1_FILENAME        = EXPERIMENTS_ROOTFOLDER + "dataset_experiment_1.csv"
EXPERIMENT2_FILENAME        = EXPERIMENTS_ROOTFOLDER + "dataset_experiment_5peak_4Influ.csv"
EXPERIMENT2_2_FILENAME      = EXPERIMENTS_ROOTFOLDER + "dataset_experiment_LowSEagents2_2.csv"
EXPERIMENT2_3_FILENAME      = EXPERIMENTS_ROOTFOLDER + "dataset_experiment_HighSEagents2_3.csv"


EXPERIMENT1_ROOT_IMAGENAME  = EXPERIMENTS_ROOTFOLDER + "experiment_2_%s.png"




COLORS      = ['b', 'r', 'g', 'k', 'c', 'y']
