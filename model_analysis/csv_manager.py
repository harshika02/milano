# standard imports


class CsvManager():
    #
    ## Constructor method
    # @param fileName Name of the CSV file
    # @param headers List of header columns
    def __init__(self, fileName, headers):
        # storing the name of the file
        self.fileName   = fileName
        # storing the headers
        self.headers    = headers
        # starting creating the file
        self.file       = open(self.fileName, "w+")
        # writing the headers to file
        self.writeRow(dict(zip(self.headers,self.headers)))
    # 
    ## Method for writing a new row in the file
    # @param values Dictionary of values to be written
    def writeRow(self, values):
        # sorting the values as the headers
        newValues = [values[key] for key in self.headers]
        # composing the line to be printed
        line    = ",".join([str(el) for el in newValues]) + "\n"
        # writing the line to file
        self.file.write(line)
    #
    ## Method for finalising the file
    def finaliseFile(self):
        # flushing
        self.file.flush()
    #
    ## Destroyer method
    def __del__(self):
        # closing the file
        self.file.close()

if __name__ == "__main__":
    csvManager = CsvManager("esempio.csv", ["Col1","Col2","Col3"])
    for i in range(10):
        # getting values
        el1 = i
        el2 = i*2
        el3 = i*4
        #creating new element
        el = {'Col1':el1,'Col2':el2,'Col3':el3}
        csvManager.writeRow(el)
