# standard imports
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.mixture import GaussianMixture
import numpy as np
from scipy import stats
import scipy.spatial.distance
from scipy.stats import norm
import statsmodels.api as sm
import tqdm 
import seaborn as sns
import ast
import os

# custom imports
#from model_analysis.constants import *
from abm_simulation.solution_space import createSolutionSpace



## Class that implements the interfaces for pulling data out of the database
class DataAnalyser():
    #
    ## Constructor method
    def __init__(self):
        pass
    #
    ## Method that performs the analysis for experiment 1
    def analyseExperiment1(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 1")
        print("################################")
        print("\n")
        # setting up the progress bar
        pbar        = tqdm.tqdm(total=5)
        # reading the file
        df = pd.read_csv(EXPERIMENT1_FILENAME)
        ####################################
        # [1] Plot - last steps values
        intendedValues  = df['IntendedValue']
        actualValues    = df['ActualValue']
        agentExperience = df['AgentExperience']
        # best fit of data
        sigma1 = np.std(actualValues)
        mu1    = np.mean(actualValues)
        sigma2 = np.std(intendedValues)
        mu2    = np.mean(intendedValues)
        # the histogram of the data
        n, bins, patches = plt.hist(actualValues, 100, density=True, facecolor='green', alpha=0.75)
        ## Adding a 'best fit' line
        #y1 = norm.pdf( bins, mu1, sigma1)
        #l1 = plt.plot(bins, y1, 'r--', linewidth=2)
        # plotting with learning
        plt.xlabel('Quality Value')
        plt.ylabel('Frequency')
        plt.title(r'$\mathrm{Histogram\ of\ With\ Learning:}\ \mu=%.3f,\ \sigma=%.3f$' %(mu1, sigma1))
        plt.grid(True)
        imageName   = EXPERIMENT1_ROOT_IMAGENAME % ("histlearning")
        plt.savefig(imageName)
        # cleaning the plot
        plt.close('all')
        #plt.show()
        # updating the progress bar
        pbar.update(1)
        ####################################
        # [2] Plot - 1 Gaussian
        n, bins, patches = plt.hist(intendedValues, 100, density=True, facecolor='green', alpha=0.75)
        ## Adding a 'best fit' line
        #y2 = norm.pdf( bins, mu2, sigma2)
        # plotting without learning
        #l2 = plt.plot(bins, y2, 'b--', linewidth=2)
        plt.xlabel('Quality Value')
        plt.ylabel('Frequency')
        plt.title(r'$\mathrm{Histogram\ of\ Without\ Learning:}\ \mu=%.3f,\ \sigma=%.3f$' %(mu2, sigma2))
        plt.grid(True)
        imageName       = EXPERIMENT1_ROOT_IMAGENAME % ("histnolearning")
        plt.savefig(imageName)
        # cleaning the plot
        plt.close('all')
        #plt.show()
        # updating the progress bar
        pbar.update(1)
        ####################################
        # [3] Plot - Double Gaussian
        # reshaping the array of actual values
        values = np.array(actualValues)
        values = values.reshape((-1,1))
        # fitting to a Gaussian Mixture distribution
        gmm = GaussianMixture(n_components=2, covariance_type='spherical')
        gmm.fit(values)
        # computing the averages
        mu1 = gmm.means_[0, 0]
        mu2 = gmm.means_[1, 0]
        # extracting the covariances
        var1, var2 = gmm.covariances_
        # extracting the weights
        wgt1, wgt2 = gmm.weights_
        """
        print(
            '''Fit:
            1: Mean {:.4}, var {:.4}, weight {:.4}
            2: Mean {:.4}, var {:.4}, weight {:.4}
            '''.format(mu1, var1, wgt1, mu2, var2, wgt2)
        )
        """
        # plotting it
        plt.hist(actualValues, bins=len(bins), alpha=.3, density=True, label='Actual values')
        plt.vlines((mu1, mu2), ymin=0, ymax=1.0, label='Fitted Means')
        plt.plot(bins, norm.pdf(bins, mu1, np.sqrt(var1)))
        plt.plot(bins, norm.pdf(bins, mu2, np.sqrt(var2)))
        plt.legend()
        plt.title('Gaussian Mixture Model')
        plt.grid(True)
        imageName   = EXPERIMENT1_ROOT_IMAGENAME % ("doublegaussian")
        plt.savefig(imageName)
        # cleaning the plot
        plt.close('all')
        #plt.show()
        # updating the progress bar
        pbar.update(1)
        ####################################
        # [4] Plot - KDE Univariate
        # fitting KDE to data
        dens = sm.nonparametric.KDEUnivariate(values)
        dens.fit()
        # evaluating fitted curve
        kde_sm = dens.evaluate(bins)
        """# plotting the results
        imageName   = EXPERIMENT1_ROOT_IMAGENAME % ("kde")
        plt.fill_between(bins, kde_sm, alpha=.5, label='KDE')
        plt.hist(values, bins=len(bins), alpha=.3, density=True, label='Actual values')
        plt.grid(True)
        plt.legend()
        plt.savefig(imageName)"""
        # plotting the distribution
        imageName   = EXPERIMENT1_ROOT_IMAGENAME % ("distribution plot")
        sns.distplot( intendedValues , color="skyblue", label="agents without learning")
        sns.distplot(actualValues , color="red", label="agents learning")
        plt.legend()
        plt.grid(True)
        plt.ylabel('Frequency')
        plt.xlabel('Values')
        plt.savefig(imageName)
        plt.show()
        # cleaning the plot
        plt.close('all')
        #plt.show()
        # updating the progress bar
        pbar.update(1)
        ####################################
        # [5] multiple line plot
        actualValueMeans        = df.groupby('SessionNumber')['ActualValue'].mean()
        intendedValuesMeans     = df.groupby('SessionNumber')['IntendedValue'].mean()
        x       = np.arange(len(actualValueMeans))
        # plotting the graph
        plt.plot(x,actualValueMeans,color='skyblue')
        plt.plot(x,intendedValuesMeans,color='olive')
        plt.xlabel('session number')
        plt.ylabel('quality of solution')
        plt.title('Agent learning from experience over sessions')
        plt.legend(['With learning from experience', 'Without learning'], loc='upper left')
        plt.grid(True)
        imageName   = EXPERIMENT1_ROOT_IMAGENAME % ("learningcomparison")
        plt.savefig(imageName)
        # cleaning the plot
        plt.close('all')
        #plt.show()
        # updating the progress bar
        pbar.update(1)
    #
    ## Method that compares 5 vs 10 sessions of exp 1
    def compareSessionLearning(self):
        df1 = pd.read_csv('dataset_experiment_1_1000_5.csv')
        df2 = pd.read_csv('dataset_experiment_1_1000_10.csv')
        ####################################
        # [6] multiple line plot
        actualValueMeans_5        = df1.groupby('SessionNumber')['ActualValue'].mean()
        intendedValuesMeans_5     = df1.groupby('SessionNumber')['IntendedValue'].mean()
        actualValueMeans_10        = df2.groupby('SessionNumber')['ActualValue'].mean()
        intendedValuesMeans_10     = df2.groupby('SessionNumber')['IntendedValue'].mean()
        x       = np.arange(len(actualValueMeans_10))
        # plotting the graph
        plt.plot(x,actualValueMeans_5,color='skyblue')
        plt.plot(x,actualValueMeans_10,color='yellow')
        plt.plot(x,intendedValuesMeans_5,color='green')
        plt.plot(x,intendedValuesMeans_10,color='red')
        plt.xlabel('session lenght')
        plt.ylabel('quality of solution')
        plt.legend(['With learning from experience session_5','Without learning session_5',\
            'With learning from experience session_10','Without learning session_10'], loc='upper left')
        plt.title('Agent learning from experience over different session length')
        plt.show
    #
    ## Method that performs the analysis for experiment 2
    def analyseExperiment2(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 2")
        print("################################")
        print("\n")
        ####################################
        # LINE PLOTS
        ####################################
        # reading the file of one peak
        df1_1  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\3peak 200prj\1Influ_3Peak.csv")
        df1_2  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\3peak 200prj\2Influ_3Peak.csv")
        df1_3  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\3peak 200prj\3Influ_3Peak.csv")
        df1_4  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\3peak 200prj\AllInflu_3Peak.csv")
        df1_5  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\3peak 200prj\NoInflu_3Peak.csv")
        # Creating data frame for 1 peak 
        dfGrpMean11 = df1_1.groupby('SessionNumber')['FinalValue'].mean()
        dfGrpMean12 = df1_2.groupby('SessionNumber')['FinalValue'].mean()
        dfGrpMean13 = df1_3.groupby('SessionNumber')['FinalValue'].mean()
        dfGrpMean14 = df1_4.groupby('SessionNumber')['FinalValue'].mean()
        dfGrpMean15 = df1_5.groupby('SessionNumber')['FinalValue'].mean()
        errorBars11  =  df1_1.groupby('SessionNumber')['FinalValue'].sem()
        errorBars12  =  df1_2.groupby('SessionNumber')['FinalValue'].sem()
        errorBars13  =  df1_3.groupby('SessionNumber')['FinalValue'].sem()
        errorBars14  =  df1_4.groupby('SessionNumber')['FinalValue'].sem()
        errorBars15  =  df1_5.groupby('SessionNumber')['FinalValue'].sem()
        x1 = np.arange(1,11)
        fig,ax1 = plt.subplots(1)
        # ploting 1 peaks 
        ax1.errorbar(x1, dfGrpMean11, yerr=errorBars11,color='c', label='1 influencer')
        ax1.errorbar(x1, dfGrpMean12, yerr=errorBars12,color='m', label='2 influencers')
        ax1.errorbar(x1, dfGrpMean13, yerr=errorBars13,color='y', label='3 influencers')
        ax1.errorbar(x1, dfGrpMean14, yerr=errorBars14,color='orange', label='All High SE')
        ax1.errorbar(x1, dfGrpMean15, yerr=errorBars15,color='pink', label='All Low SE')
        ax1.set_title('quality of solutions with 3 peaks')
        ax1.set_ylabel('solution quality with standard error bars')
        ax1.set_xlabel('session numbers')
        ax1.legend()
        ax1.set_xlim([1, 10])
        ax1.set_ylim([0.43, 0.67])
        ax1.grid('True')
        plt.show()
    #
    ## Method that performs the analysis for experiment 3 (agent positions)
    def analyseExperiment3(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 3")
        print("################################")
        print("\n")
        # path where the data of the experiment are stored
        rootPath    = "model_analysis/output/exp_3/%d/" % (4)
        # reading the file with positions
        df      = pd.read_csv(rootPath+'finalPositions.csv')
        dfnew       = pd.DataFrame({'ProjectID':df['ProjectID'],'SessionNumber': df['SessionNumber'],\
            'InfluencerPosition':df['InfluencerPosition'],\
                'LowestSEagentPosition':df['LowestSEagentPosition'],\
                    'HighestSEagentPosition':df['HighestSEagentPosition']})
        lowDisList = []
        highDisList= []
        # interating for all the projects
        for projectId in np.unique(dfnew['ProjectID']):
            # create solution space
            solutionSpace = createSolutionSpace ()
            liList = []
            hiList = []
            #  interating for all the sessions
            for sessionNumber in np.unique(dfnew['SessionNumber']):
                # plotting solution space
                '''fig,ax  = plt.subplots(1)
                ax.matshow(solutionSpace)'''
                ip = dfnew.iloc[sessionNumber]['InfluencerPosition']
                lp = dfnew.iloc[sessionNumber]['LowestSEagentPosition']
                hp = dfnew.iloc[sessionNumber]['HighestSEagentPosition']
                # converting string to integer
                influencerPoint     = self.convertStringInt(ip)
                lowSEagentPoint     = self.convertStringInt(lp)
                highSEagentPoint    = self.convertStringInt(hp)
                distanceLI          = self.calculateDistance(influencerPoint,lowSEagentPoint)
                distanceHI          = self.calculateDistance(influencerPoint,highSEagentPoint)
                '''# plot x and y using red circle markers for influencers
                plt.plot(influencerPoint[0],influencerPoint[1],'ro', label='influencer') 
                # plot x and y using black circle markers for low SE agents 
                plt.plot(lowSEagentPoint[0],lowSEagentPoint[1],'ko',label='low SE agent')  
                # plot x and y using blue circle markers for high SE agents
                plt.plot(highSEagentPoint[0],highSEagentPoint[1],'bo',label='high SE agent')
                plt.title("p:"+str(project)+"session:"+str(n+1))
                plt.legend()
                plt.show()'''
                liList.append(distanceLI)
                hiList.append(distanceHI)
            dfnew['lowDisList'] = liList
            #df2 = pd.DataFrame({'projectId':projectId,'SessionNumber':sessionNumber,'lowDisList': liList,'highDisList':hiList})
        table = pd.pivot_table(dfnew, values=['lowDisList','highDisList'], index=['SessionNumber'],\
            columns=['ProjectID'], aggfunc={'lowDisList':[np.mean,np.std],\
                'highDisList':[np.mean,np.std]})
        print(table)           
    #
    ## Method that plots trajectories for experiment 4 (agents all positions)
    def analyseExperiment4(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 4")
        print("################################")
        print("\n")
        # path where the data of the experiment are stored
        rootPath    = "model_analysis/output/exp_4/%d/" % (4)
        # reading the file with positions
        df      = pd.read_csv(rootPath+'AllPositions2influenecrs.csv')
        # reading the file with solution space
        dfSS    = pd.read_csv(rootPath+'1.csv')
        dfnew       = pd.DataFrame({'ProjectID':df['ProjectID'],'SessionNumber': df['SessionNumber'],\
            'Influencer1Positions':df['Influencer1Positions'],'Influencer2Positions':df['Influencer2Positions'],\
                'HighestSEagentPositions':df['HighestSEagentPositions']})
        # interating for all the projects
        for projectId in np.unique(dfnew['ProjectID']):            
            #  interating for all the sessions
            for sessionNumber in np.unique(dfnew['SessionNumber']):
                # creating the folder
                sessionFolder = rootPath+str(sessionNumber)+"/"
                self.createFolder(sessionFolder)
                # storing the steps for session number sessionNumber
                steps = dfnew[dfnew['SessionNumber'] == sessionNumber]
                # iterating over number of steps in a session
                for i in range(1, len(steps)):
                    # extracting influencer1 positions
                    i1Positions  = list(steps['Influencer1Positions'])
                    # extracting influencer2 positions
                    i2Positions  = list(steps['Influencer2Positions'])
                    # extracting high SE agent positions
                    hPositions  = list(steps['HighestSEagentPositions'])
                    # plotting the solution space
                    fig,ax  = plt.subplots(1)
                    ax.matshow(dfSS)
                    # extracting the first point for influencer1
                    i1position = ast.literal_eval(i1Positions[0])
                    # extracting the first point for influencer2
                    i2position = ast.literal_eval(i2Positions[0])
                    # extracting the first point for high SE agent
                    hposition = ast.literal_eval(hPositions[0])
                    # plot the first point
                    plt.plot(i1position[0], i1position[1], 'ro')
                    plt.plot(i2position[0], i2position[1], 'mo')
                    plt.plot(hposition[0], hposition[1], 'bo')
                    # plotting the last point
                    i1position = ast.literal_eval(i1Positions[i])
                    i2position = ast.literal_eval(i2Positions[i])
                    hposition = ast.literal_eval(hPositions[i])
                    # plot the last point
                    plt.plot(i1position[0], i1position[1], 'rx')
                    plt.plot(i2position[0], i2position[1], 'mx')
                    plt.plot(hposition[0], hposition[1], 'bx')
                    # plotting the lines
                    for j in range(1,i+1):
                        # extracting the points for influencer1
                        i1p0 = ast.literal_eval(i1Positions[j-1])
                        i1p1 = ast.literal_eval(i1Positions[j])
                        # extracting the points for influencer2
                        i2p0 = ast.literal_eval(i2Positions[j-1])
                        i2p1 = ast.literal_eval(i2Positions[j])
                        # plotting the line for influencer1
                        plt.plot([i1p0[0],i1p1[0]],[i1p0[1],i1p1[1]],'r')
                        # plotting the line for influencer2
                        plt.plot([i2p0[0],i2p1[0]],[i2p0[1],i2p1[1]],'m')
                        # extracting the points for high SE agent
                        hp0 = ast.literal_eval(hPositions[j-1])
                        hp1 = ast.literal_eval(hPositions[j])
                        # plotting the line for high SE agent
                        plt.plot([hp0[0],hp1[0]],[hp0[1],hp1[1]],'b')
                        plt.ylim(0,100)
                        plt.xlim(0,100)
                    # storing the image
                    imageName = sessionFolder + "2influencer_prj%d_ses%d_stp%d.png"%(projectId, sessionNumber, i)
                    plt.savefig(imageName)
                    # clearing the image
                    plt.close('all')
    #
    ## Method that plots exploration of agents and returns mean exploration index of all projects
    def analyseExperiment5(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment5")
        print("################################")
        print("\n")  
        # path where the data of the experiment are stored
        rootPath    = "model_analysis/output/exp_5/5peak_dataV2/%d/" % (13)
        #rootPath    = "output/exp_5/3peak_data_200/%d/" % (14)
        # reading the file with positions
        df      = pd.read_csv(rootPath+'AllAgentsAllPositionsAllinfluencer_5Peak_200prjV2.csv')
        # reading the file with solution space
        dfSS    = pd.read_csv(rootPath+'5peak.csv')
        #fig,ax  = plt.subplots(1)
        #ax.matshow(dfSS)
        projectsIds   = df.ProjectId.unique()
        indexes = []
        # iterating for all the projects
        for projectId in projectsIds:
            projectData   = df[df['ProjectId']==projectId][['PositionValues','PositionX','PositionY']]
            dfssCopy = np.zeros((100,100))
            # taking the position values
            positionX = projectData['PositionX']
            positionY = projectData['PositionY']
            for index, row in projectData.iterrows():
                if dfssCopy[positionX[index], positionY[index]] == 0:
                    dfssCopy[positionX[index], positionY[index]] = 1
                else:
                    pass
            # plotting of the points
            '''plt.imshow(dfssCopy, cmap='RdPu')
            plt.colorbar()
            plt.show()'''
            # geting the exploration index 
            index = self.calculateEIndex(dfssCopy)
            # geting the quality index 
            #qIndex = self.calculateIndex(self.reduceSize(dfssCopy))
            #print("e-index of",projectId,"is",index)
            # list of idexes
            indexes.append(index)
        # taking the mean of all the exp indexes
        indexMean  = np.mean(indexes)
        # taking the varaiance of all the exp indexes for all projects
        indexErr  = stats.sem(indexes)
        print("All influencer mean e-index 5 peak:",indexMean,"SEM:",indexErr)
    #
    ## Method that returns mean, variance of the quality of exploration of all projects
    def analyseExperiment6(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment6")
        print("################################")
        print("\n")  
        # path where the data of the experiment are stored
        #rootPath    = "model_analysis/output/exp_5/5peak_data/%d/" % (23)
        rootPath    = "output/exp_5/5peak_data/%d/" % (28)
        # reading the file with positions
        df      = pd.read_csv(rootPath+'AllAgentsAllPositionsNoinfluencer_5Peak.csv')
        # reading the file with solution space
        dfSS    = pd.read_csv(rootPath+'200.csv')
        # number of neighbouring cells
        neighbours = 4
        count = 0
        projectsIds   = df.ProjectId.unique()
        # getting the solution
        positionX = df['PositionX']
        positionY = df['PositionY']
        for index, row in df.iterrows():
            neighborMean = self.calculateMeanofNeighbours(neighbours,positionX[index],positionY[index],dfSS)
            if neighborMean > 0.5:
                count = count+1
        # calculating the quality exploration index 
        qIndexLocal = count/index
        # calculating the quality exploration index global for each solution space (here ss are same)
        m = dfSS.to_numpy()
        (x,y) = np.where(m>0.5)
        qIndexGlobal = count/(len(x)*len(projectsIds))
        print("mean qIndex of project is:",qIndexLocal,qIndexGlobal)
    #
    ## Method for calculating exploration and quality local and global
    def analyseExperiment7(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 7")
        print("################################")
        print("\n")  
        # path where the data of the experiment are stored
        rootPath    = "model_analysis/output/exp_5/5peak_dataV2/%d/" % (13)
        #rootPath    = "output/exp_5/1peak_data_200/%d/" % (13)
        # reading the file with positions
        df      = pd.read_csv(rootPath+'AllAgentsAllPositionsAllinfluencer_5Peak_200prjV2.csv')
        # reading the file with solution space
        dfSS    = pd.read_csv(rootPath+'5peak.csv')
        # getting the project ids
        projectsIds = df.ProjectId.unique()
        # init the values for storing exploration and quality
        expValues = []
        qGexpValues = []
        qLexpValues = []
        # interating over project ids
        for projectId in projectsIds:
            projectData     = df[df['ProjectId']==projectId][['PositionValues','PositionX','PositionY']]
            # removing the rows in df with same position values
            dfClean         =  projectData.drop_duplicates()
            # calculating exploration unique position values to the size of design space
            exp = len(dfClean['PositionValues'])/(np.size(dfSS))
            # getting the list of cells where the position value of cleaned df is greater than 0.5
            a   = np.where(dfClean['PositionValues']>0.5)
            # getting the length of PositionValues for that project
            l   = len(projectData['PositionValues'])
            # converting df to np  array
            m   = dfSS.to_numpy()
            # storing the position of teh matrix where the value is more than 0.5
            (x,y)   = np.where(m>0.5)
            #  calculating the local quality index which is the ratio of #PositionValues >0.5 to the total #PositionValues
            qlocal  = len(a[0])/l
            # calculating the global quality index which is the ratio of #PositionValues >0.5 to the total #DSS >0.5
            qglobal = len(a[0])/(len(x))
            # adding to the lists
            expValues.append(exp)
            qGexpValues.append(qglobal)
            qLexpValues.append(qlocal)
        # calculating the mean
        emean= np.mean(expValues)
        qLmean = np.mean(qLexpValues)
        qGmean = np.mean(qGexpValues)
        # calculating the error
        eErr   = stats.sem(expValues)
        qLErr  = stats.sem(qLexpValues)
        qGErr  = stats.sem(qGexpValues)
        print("exploration",emean)
        print("Local quality exploration",qLmean)
        print("Global quality exploration",qGmean)
        print("exploration error",eErr)
        print("Local quality error",qLErr)
        print("Global quality error",qGErr)
    #
    ## Method that plots exploration of agents and returns mean exploration index of sessions
    def analyseExperiment8(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 8")
        print("################################")
        print("\n")  
        # path where the data of the experiment are stored
        #rootPath    = "model_analysis/output/exp_5/5peak_dataV2/%d/" % (13)
        #rootPath    = "output/exp_5/1peak_data_200/%d/" % (13)
        # reading the file with positions
        #df      = pd.read_csv(rootPath+'AllAgentsAllPositionsAllinfluencer_5Peak_200prjV2.csv')
        df       = pd.read_csv(r'C:\Users\Harshika\Documents\repositories\socog-model\model_analysis\output\exp_5\slope_exp1agent\AllAgentsAllPositions_5peakMix_1exp.csv')
        # reading the file with solution space
        #dfSS    = pd.read_csv(rootPath+'5peak.csv')
        dfSS    = pd.read_csv(r'C:\Users\Harshika\Documents\repositories\socog-model\model_analysis\output\exp_5\slope_exp1agent\5 mix peak.csv')
        #fig,ax  = plt.subplots(1)
        #ax.matshow(dfSS)
        # getting unique projects numbers
        projectsIds   = df.ProjectId.unique()
        eindexValues         = {}
        # iterating for all the projects
        for projectId in projectsIds:
            projectData     = df[df['ProjectId']==projectId][['PositionValues','PositionX','PositionY','SessionNumber']]
            # getting unique session numbers
            sessNums   = projectData.SessionNumber.unique()
            # making a copy dataframe of solution space
            # dfssCopy = np.zeros((100,100))
            # iterating for all the sessions in a project
            for sessNum in sessNums:
                dfssCopy = np.zeros((100,100))
                sessData = projectData[projectData['SessionNumber']==sessNum][['PositionValues','PositionX','PositionY']]   
                # taking the position values
                positionX = sessData['PositionX']
                positionY = sessData['PositionY']
                for index, row in sessData.iterrows():
                    if dfssCopy[positionX[index], positionY[index]] == 0:
                        dfssCopy[positionX[index], positionY[index]] = 1
                    else:
                        pass
                # plotting of the points
                '''plt.imshow(dfssCopy, cmap='RdPu')
                plt.title("exploration plot for session "+ str(sessNum)+"in project "+str(projectId))
                plt.colorbar()
                plt.show()'''
                # geting the exploration index 
                eindex = self.calculateEIndex(dfssCopy)
                #if sessNum in eindexValues.keys():
                try:
                    eindexValues[sessNum].append(eindex)
                #else:
                except KeyError:
                    eindexValues[sessNum] = [eindex]
        # list of mean values for sessions for plotting
        y =[]
        # list of standard errors values for sessions for plotting
        e =[]
        for s in df.SessionNumber.unique():
            sessEindexValueMean = np.mean(eindexValues[s])
            sessEindexValueStdError  = stats.sem(eindexValues[s])
            print(sessEindexValueMean,sessEindexValueStdError)
            #print(sessEindexValueStdError)
            y.append(sessEindexValueMean)
            e.append(sessEindexValueStdError)
        print('exploration rate %',y)
        #  plotting
        x = np.arange(1,11)
        plt.errorbar(x, y, yerr=e,color='green', label='All influencers')
        plt.grid('True')
        plt.legend()
        plt.ylabel('exploration rate with standard error bars')
        plt.xlabel('session numbers')
        plt.show()
    #
    ## Method that computes distance from center of mass for the points
    def analyseExperiment9(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 9")
        print("################################")
        print("\n") 
        # path where the data of the experiment are stored
        rootPath    = "model_analysis/output/exp_5/5peak_dataV2/%d/" % (13)
        #rootPath    = "output/exp_5/1peak_data_200/%d/" % (13)
        # reading the file with positions
        df      = pd.read_csv(rootPath+'AllAgentsAllPositionsAllinfluencer_5Peak_200prjV2.csv')
        # reading the file with solution space
        dfSS    = pd.read_csv(rootPath+'5peak.csv')
        # getting unique projects numbers
        projectsIds   = df.ProjectId.unique()
        distanceDispersion = []
        # iterating for all the projects
        for projectId in projectsIds:
            distances = []
            projectData     = df[df['ProjectId']==projectId][['PositionValues','PositionX','PositionY','SessionNumber']]
            # removing the rows in df with same position values
            dfClean         =  projectData.drop_duplicates()
            positionX = dfClean['PositionX']
            positionY = dfClean['PositionY']
            positions= []
            centroid = (int(sum(positionX) / len(positionX)),int(sum(positionY) / len(positionY)))
            for index, row in dfClean.iterrows():
                position = [positionX[index],positionY[index]]
                # computing the deltas
                deltaX = positionX[index] - centroid[0]
                deltaY = positionY[index] - centroid[1]
                # computing the distance
                distance = np.sqrt(np.power(deltaX,2) + np.power(deltaY,2))
                # adding to the list
                distances.append(distance)
            distanceDispersion.append(np.std(distances))
        print('Mean and SEM SD of distances for 3 influencers',np.mean(distanceDispersion),stats.sem(distanceDispersion))
    #
    ## Method to plot exploration bars 
    def plotBars(self):
        df = pd.DataFrame({'1 peak':[12.277,11.434,11.358,11.416,12.068],\
                              '3 peaks':[10.147,11,11.123,10.946,11.771],\
                                  '5 peaks':[10.096,9.918,9.929,10.854,11.297],\
                                      '1peakErr':[0.363,0.384,0.31,0.366,0.327],\
                                          '3peakErr':[0.297,0.346,0.354,0.327,0.401],\
                                              '5peakErr':[0.325,0.326,0.316,0.341,0.381]})
        df[['1 peak','3 peaks','5 peaks']].plot(kind='bar',yerr=df[['1peakErr','3peakErr','5peakErr']].values.T,error_kw=dict(ecolor='k'))
        plt.xticks(np.arange(5), ('1 influencer', '2 Influencers', '3 influencers', 'All influencers','No influencer'),rotation='horizontal')
        plt.ylabel('values with standard error bars')
        plt.grid('True')
        plt.title('Spread of the Final Solutions')
        plt.ylim(8,13)
        #plt.xlim(0,10)
        plt.tight_layout()
        plt.legend(loc=9)
        plt.show()                                         
    #
    ##################################################
    ## Methood for converting string positions to integers
    def convertStringInt(self,stringPoint):
        x,y =[int(el) for el in stringPoint[1:-1].split(" ") if el!=""]
        point = (x,y)
        return (point)
    #
    ## Method for calculating the distance between influencer and low or high SE agent
    # @param position1 position of the influencer
    # @param position2 position of the agent
    # @return distance distance between the two agents
    def calculateDistance(self,position1,position2):
        # computing the deltas
        deltaX = position1[0] - position2[0]
        deltaY = position1[1] - position2[1]
        # computing the distance
        distance = np.sqrt(np.power(deltaX,2) + np.power(deltaY,2))
        return distance
    #
    ## Method to reduce the size of the solution space
    # @param dataframe 
    # @returns eIndex exploration index ie the number of non zeros in a matrix of reduced size
    def calculateEIndex(self,dataFrame):
        # path where the data of the experiment are stored
        #rootPath    = "model_analysis/output/exp_5/%d/" % (0)
        #dataFrame   = pd.read_csv(rootPath+'1.csv')
        # converting the dataframe to nd array
        #m = dataFrame.to_numpy()
        m = dataFrame
        # giving the matrix reducing factor
        period = 5
        indexes= np.arange(0,100,period)
        result = np.zeros((int(100/period),int(100/period)))
        for i in range(len(indexes)):
            for j in range(len(indexes)):
                # getting the maximum of the 5X5 sub matrix ie 1
                result[i][j]=np.amax(m[indexes[i]:indexes[i]+period,indexes[j]:indexes[j]+period])
            # plotting the result
            '''plt.imshow(result, cmap='viridis')
            plt.colorbar()
            plt.show()'''
        eIndex = np.count_nonzero(result)/result.size
        return eIndex
    #
    ## Method that gives a neighbours of the a passed position
    # @param dataframe 
    # @param n number of neighbour cells around the position p
    # @param rowNumber row number of the position
    # @param columnNumber column number of the position
    # @returns meanResult mean of the resultant matrix of reduced size 
    def calculateMeanofNeighbours(self, n, rowNumber, columnNumber, dataframe):
        # converting the dataframe to nd array
        m = dataframe.to_numpy()
        # cutting the matrix portion
        m_cut = m[int(rowNumber-n/2):int(rowNumber+n/2),\
                  int(columnNumber-n/2):int(columnNumber+n/2)]
        meanResult= np.mean(m_cut)
        return meanResult
    #
    ## Method that creates a folder if it doesn't exist
    def createFolder(self, folderPath):
        # checking if the folder exists
        if os.path.exists(folderPath):
            pass
        else:
            os.mkdir(folderPath)
    #


    if __name__ == "__main__":
        pass
