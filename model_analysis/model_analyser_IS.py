# standard imports
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn.datasets import make_blobs
import numpy as np
from scipy import stats
from scipy.stats import norm
import statsmodels.api as sm
import tqdm 
import seaborn as sns
import ast
import os

# custom imports
from model_analysis.constants import *
from abm_simulation.solution_space import createSolutionSpace



## Class that implements the interfaces for pulling data out of the database
class DataAnalyserIdeaSelection():
    # reading the file of five peak of selected agents 
    '''df1_1  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_1\5peak_200prjV2\1_Influenecr_5peak_IS_selectedAgents.csv")
    df1_2  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_1\5peak_200prjV2\2_Influenecr_5peak_IS_selectedAgents.csv")
    df1_3  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_1\5peak_200prjV2\3_Influenecr_5peak_IS_selectedAgents.csv")
    df1_4  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_1\5peak_200prjV2\All_Influenecr_5peak_IS_selectedAgents.csv")
    df1_5  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_1\5peak_200prjV2\No_Influenecr_5peak_IS_selectedAgents.csv")
    # reading the file of five peak of final solution
    df2_1  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_2\5peakV2\1_influencers_200_5peakV2.csv")
    df2_2  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_2\5peakV2\2_influencers_200_5peakV2.csv")
    df2_3  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_2\5peakV2\3_influencers_200_5peakV2.csv")
    df2_4  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_2\5peakV2\All_influencers_200_5peakV2.csv")
    df2_5  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_2\5peakV2\No_influencers_200_5peakV2.csv")
    #dfAll  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_2\all_final quality.csv")
    # reading the file of five peak of all agent solutions
    df3_1  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_3\5_PeakV2\1_influencers_200_5peakV2.csv")
    df3_2  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_3\5_PeakV2\2_influencers_200_5peakV2.csv")
    df3_3  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_3\5_PeakV2\3_influencers_200_5peakV2.csv")
    df3_4  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_3\5_PeakV2\All_influencers_200_5peakV2.csv")
    df3_5  = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_3\5_PeakV2\No_influencers_200_5peakV2.csv")
    '''
    ## Constructor method
    def __init__(self):
        pass
    #
    ## Method that performs the analysis for experiment 1 for the Final solution qualities
    def analyseISExperiment1_1Dataset(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 1_1 IS")
        print("################################")
        print("\n")
        x1 = np.arange(1,11)
        fig,ax1 = plt.subplots(1)
        # ploting 1 peaks data 
        # Grouping by session number standard error mean of solutions proposed by selected ahgents
        dfGrpMean31     = self.df1_1.groupby('SessionNumber')['SolutionSA'].mean()
        errorBars31     = self.df1_1.groupby('SessionNumber')['SolutionSA'].sem()
        dfGrpMean32     = self.df1_2.groupby('SessionNumber')['SolutionSA'].mean()
        errorBars32     = self.df1_2.groupby('SessionNumber')['SolutionSA'].sem()
        dfGrpMean33     = self.df1_3.groupby('SessionNumber')['SolutionSA'].mean()
        errorBars33     = self.df1_3.groupby('SessionNumber')['SolutionSA'].sem()
        dfGrpMean34     = self.df1_4.groupby('SessionNumber')['SolutionSA'].mean()
        errorBars34     = self.df1_4.groupby('SessionNumber')['SolutionSA'].sem()
        dfGrpMean35     = self.df1_5.groupby('SessionNumber')['SolutionSA'].mean()
        errorBars35     = self.df1_5.groupby('SessionNumber')['SolutionSA'].sem()
        # plotting
        ax1.errorbar(x1, dfGrpMean31, yerr=errorBars31,color='c', label='1 influencer')
        ax1.errorbar(x1, dfGrpMean32, yerr=errorBars32,color='m', label='2 influencers')
        ax1.errorbar(x1, dfGrpMean33, yerr=errorBars33,color='y', label='3 influencers')
        ax1.errorbar(x1, dfGrpMean34, yerr=errorBars34,color='orange', label='All High SE')
        ax1.errorbar(x1, dfGrpMean35, yerr=errorBars35,color='pink', label='All Low SE')
        ax1.set_title('Selected agents proposed solution quality')
        ax1.set_ylabel(' Proposed solution quality with standard error bars')
        ax1.set_xlabel('Session numbers')
        ax1.legend()
        ax1.set_xlim([1, 10])
        ax1.set_ylim([0.54, .68])
        ax1.grid('True')
        plt.show()
    #
    ## Method that performs the analysis for experiment 1 for the Final solution vs proposed solution qualities
    def analyseISExperiment1_2Dataset(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 1_2 IS")
        print("################################")
        print("\n")
        # Grouping by session number mean of solutions proposed by selected ahgents
        # Grouping by session number standard error mean of solutions proposed by selected ahgents
        dfGrpMean11     = self.df1_1.groupby('SessionNumber')['SolutionSA'].mean()
        errorBars11     = self.df1_1.groupby('SessionNumber')['SolutionSA'].sem()
        dfGrpMean12     = self.df1_2.groupby('SessionNumber')['SolutionSA'].mean()
        errorBars12     = self.df1_2.groupby('SessionNumber')['SolutionSA'].sem()
        dfGrpMean13     = self.df1_3.groupby('SessionNumber')['SolutionSA'].mean()
        errorBars13     = self.df1_3.groupby('SessionNumber')['SolutionSA'].sem()
        dfGrpMean14     = self.df1_4.groupby('SessionNumber')['SolutionSA'].mean()
        errorBars14     = self.df1_4.groupby('SessionNumber')['SolutionSA'].sem()
        dfGrpMean15     = self.df1_5.groupby('SessionNumber')['SolutionSA'].mean()
        errorBars15     = self.df1_5.groupby('SessionNumber')['SolutionSA'].sem()
        # x axis: 1- 10 session numbers
        x1 = np.arange(1,11)
        # ploting 5 cases in one figure 
        ax1 = plt.subplot2grid(shape=(2,6), loc=(0,0), colspan=2)
        ax2 = plt.subplot2grid((2,6), (0,2), colspan=2)
        ax3 = plt.subplot2grid((2,6), (0,4), colspan=2)
        ax4 = plt.subplot2grid((2,6), (1,1), colspan=2)
        ax5 = plt.subplot2grid((2,6), (1,3), colspan=2)
        # plotting final solution 
        ax1.errorbar(x1, self.df2_1['FinalSolutionValue'], yerr=self.df2_1['FinalSolutionValueErr'],color='tab:orange', label='Final chosen solution')
        # plotting mean of the proposed solutions by selected agents 
        ax1.errorbar(x1, dfGrpMean11, yerr=errorBars11,color='tab:blue', label='Proposed solution (mean)')
        ax1.set_title('1 influencer')
        ax2.errorbar(x1, self.df2_2['FinalSolutionValue'], yerr=self.df2_2['FinalSolutionValueErr'],color='tab:orange', label='Final chosen solution')
        ax2.errorbar(x1, dfGrpMean12, yerr=errorBars12,color='tab:blue', label='Proposed solution (mean)')
        ax2.set_title('2 influencers')
        ax3.errorbar(x1, self.df2_3['FinalSolutionValue'], yerr=self.df2_3['FinalSolutionValueErr'],color='tab:orange', label='Final chosen solution')
        ax3.errorbar(x1, dfGrpMean13, yerr=errorBars13,color='tab:blue', label='Proposed solution (mean)')
        ax3.set_title('3 influencers')
        ax4.errorbar(x1, self.df2_4['FinalSolutionValue'], yerr=self.df2_4['FinalSolutionValueErr'],color='tab:orange', label='Final chosen solution')
        ax4.errorbar(x1, dfGrpMean14, yerr=errorBars14,color='tab:blue', label='Proposed solution (mean)')
        ax4.set_title('All influencers')
        ax5.errorbar(x1, self.df2_5['FinalSolutionValue'], yerr=self.df2_5['FinalSolutionValueErr'],color='tab:orange', label='Final chosen solution')
        ax5.errorbar(x1, dfGrpMean15, yerr=errorBars15,color='tab:blue', label='Proposed solution (mean)')
        ax5.set_title('No influencers')
        # storing all the axes in a list 
        axs = [ax1,ax2,ax3,ax4,ax5]
        # assign y limit,labels and grid to all axes
        for a in axs:
            a.set_ylim(0.55,0.75)
            #a.set_xlim(0,9)
            a.grid('True')
            a.set_ylabel('Solution quality with error bars')
            a.set_xlabel('Session numbers')
        # plotting legend common to all the figures
        handles, labels = ax1.get_legend_handles_labels()
        plt.figlegend( labels, loc = 'center', ncol=5, labelspacing=0. )
        plt.tight_layout()
        plt.show()
    #
    ## Method that performs the analysis and plot final solution vs difference in max and min proposed solution qualities
    def analyseISExperiment1_3Dataset(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 1_3 IS")
        print("################################")
        print("\n")
        # Creating data frame for 1 peak
        # Grouping by session number standard error max of solutions proposed by selected ahgents
        dfGrpMax11     = self.df1_1.groupby('SessionNumber')['SolutionSA'].max()
        dfGrpMax12     = self.df1_2.groupby('SessionNumber')['SolutionSA'].max()
        dfGrpMax13     = self.df1_3.groupby('SessionNumber')['SolutionSA'].max()
        dfGrpMax14     = self.df1_4.groupby('SessionNumber')['SolutionSA'].max()
        dfGrpMax15     = self.df1_5.groupby('SessionNumber')['SolutionSA'].max()
        # Grouping by session number standard error min of solutions proposed by selected ahgents
        dfGrpMin11     = self.df1_1.groupby('SessionNumber')['SolutionSA'].min()
        dfGrpMin12     = self.df1_2.groupby('SessionNumber')['SolutionSA'].min()
        dfGrpMin13     = self.df1_3.groupby('SessionNumber')['SolutionSA'].min()
        dfGrpMin14     = self.df1_4.groupby('SessionNumber')['SolutionSA'].min()
        dfGrpMin15     = self.df1_5.groupby('SessionNumber')['SolutionSA'].min()
        # diference in max proposed solutions and final solution
        dMax11         = dfGrpMax11-self.df2_1['FinalSolutionValue']
        dMax12         = dfGrpMax12-self.df2_2['FinalSolutionValue']
        dMax13         = dfGrpMax13-self.df2_3['FinalSolutionValue']
        dMax14         = dfGrpMax14-self.df2_4['FinalSolutionValue']
        dMax15         = dfGrpMax15-self.df2_5['FinalSolutionValue']
        # diference in min proposed solutions and final solution
        dMin11         = self.df2_1['FinalSolutionValue']-dfGrpMin11
        dMin12         = self.df2_2['FinalSolutionValue']-dfGrpMin12
        dMin13         = self.df2_3['FinalSolutionValue']-dfGrpMin13
        dMin14         = self.df2_4['FinalSolutionValue']-dfGrpMin14
        dMin15         = self.df2_5['FinalSolutionValue']-dfGrpMin15
        # x axis: 1- 10 session numbers
        x1 = np.arange(1,11)
        # ploting 5 cases in one figure 
        ax1 = plt.subplot2grid(shape=(2,6), loc=(0,0), colspan=2)
        ax2 = plt.subplot2grid((2,6), (0,2), colspan=2)
        ax3 = plt.subplot2grid((2,6), (0,4), colspan=2)
        ax4 = plt.subplot2grid((2,6), (1,1), colspan=2)
        ax5 = plt.subplot2grid((2,6), (1,3), colspan=2)
        # plotting final solution 
        # diference in max proposed solutions and final solution
        # diference in min proposed solutions and final solution
        ax1.errorbar(x1, self.df2_1['FinalSolutionValue'], yerr=self.df2_1['FinalSolutionValueErr'],color='m', label='Final chosen solution')
        ax1.bar(x1-0.2, dMax11, width=0.2, color='orange', align='center')
        ax1.bar(x1, dMin11, width=0.2, color='c', align='center')
        ax1.set_title('1 influencer')
        ax2.errorbar(x1, self.df2_2['FinalSolutionValue'], yerr=self.df2_2['FinalSolutionValueErr'],color='m', label='Final chosen solution')
        ax2.bar(x1-0.2, dMax12, width=0.2, color='orange', align='center')
        ax2.bar(x1, dMin12, width=0.2, color='c', align='center')
        ax2.set_title('2 influencers')
        ax3.errorbar(x1, self.df2_3['FinalSolutionValue'], yerr=self.df2_3['FinalSolutionValueErr'],color='m', label='Final chosen solution')
        ax3.bar(x1-0.2, dMax13, width=0.2, color='orange', align='center')
        ax3.bar(x1, dMin13, width=0.2, color='c', align='center')
        ax3.set_title('3 influencers')
        ax4.errorbar(x1, self.df2_4['FinalSolutionValue'], yerr=self.df2_4['FinalSolutionValueErr'],color='m', label='Final chosen solution')
        ax4.bar(x1-0.2, dMax14, width=0.2, color='orange', align='center')
        ax4.bar(x1, dMin14, width=0.2, color='c', align='center')
        ax4.set_title('All influencers')
        ax5.errorbar(x1, self.df2_5['FinalSolutionValue'], yerr=self.df2_5['FinalSolutionValueErr'],color='m', label='Final chosen solution')
        ax5.bar(x1-0.2, dMax15, width=0.2, color='orange', align='center',label='Difference in max proposed and final solution')
        ax5.bar(x1, dMin15, width=0.2, color='c', align='center',label='Difference in min proposed and final solution')
        ax5.set_title('No influencers')
        # storing all the axes in a list 
        axs = [ax1,ax2,ax3,ax4,ax5]
        # assign y limit,labels and grid to all axes
        # assign y limit,labels and grid to all axes
        for a in axs:
            a.set_ylim(0,0.75)
            a.grid('True')
            a.set_ylabel('Solution quality with error bars')
            a.set_xlabel('Session numbers')
        # plotting legend common to all the figures
        handles, labels = ax5.get_legend_handles_labels()
        plt.figlegend( labels, loc = 'center', ncol=5, labelspacing=0. )
        plt.tight_layout()
        plt.show()
    #
    ## Method that performs the analysis for experiment 1 for the self-efficacies
    def analyseISExperiment1_4Dataset(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 1_4 IS")
        print("################################")
        print("\n")
        x1 = np.arange(1,11)
        fig,ax1 = plt.subplots(1)
        # ploting 1 peaks data 
        # Grouping by session number standard error mean of self-efficacies
        dfGrpMean31     = self.df3_1.groupby('SessionNumber')['Self-efficacy'].mean()
        errorBars31     = self.df3_1.groupby('SessionNumber')['Self-efficacy'].sem()
        dfGrpMean32     = self.df3_2.groupby('SessionNumber')['Self-efficacy'].mean()
        errorBars32     = self.df3_2.groupby('SessionNumber')['Self-efficacy'].sem()
        dfGrpMean33     = self.df3_3.groupby('SessionNumber')['Self-efficacy'].mean()
        errorBars33     = self.df3_3.groupby('SessionNumber')['Self-efficacy'].sem()
        dfGrpMean34     = self.df3_4.groupby('SessionNumber')['Self-efficacy'].mean()
        errorBars34     = self.df3_4.groupby('SessionNumber')['Self-efficacy'].sem()
        dfGrpMean35     = self.df3_5.groupby('SessionNumber')['Self-efficacy'].mean()
        errorBars35     = self.df3_5.groupby('SessionNumber')['Self-efficacy'].sem()
        ax1.errorbar(x1, dfGrpMean31, yerr=errorBars31,color='c', label='1 influencer')
        ax1.errorbar(x1, dfGrpMean32, yerr=errorBars32,color='m', label='2 influencers')
        ax1.errorbar(x1, dfGrpMean33, yerr=errorBars33,color='y', label='3 influencers')
        ax1.errorbar(x1, dfGrpMean34, yerr=errorBars34,color='orange', label='All High SE')
        ax1.errorbar(x1, dfGrpMean35, yerr=errorBars35,color='pink', label='All Low SE')
        ax1.set_title('self-efficacy of agents')
        ax1.set_ylabel('self-efficacy with error bars')
        ax1.set_xlabel('Session numbers')
        ax1.legend()
        ax1.set_xlim([1, 10])
        #ax1.set_ylim([0.0, 5])
        ax1.grid('True')
        plt.show()
    #
    ## Method for ploting and analysing agreement in teams 
    def analyseISExperiment2Dataset(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 2 IS")
        print("################################")
        print("\n")
        # getting the normalised data
        # normalising for 1 influencer
        df1 = self.normaliseData(self.df2_1['AgreementValue'],self.df2_2['AgreementValue'],self.df2_3['AgreementValue'],\
            self.df2_4['AgreementValue'],self.df2_5['AgreementValue'],self.df2_1['AgreementValue'])
        # normalising for 2 influencers
        df2 = self.normaliseData(self.df2_1['AgreementValue'],self.df2_2['AgreementValue'],self.df2_3['AgreementValue'],\
            self.df2_4['AgreementValue'],self.df2_5['AgreementValue'],self.df2_2['AgreementValue'])
        # normalising for 3 influencers
        df3 = self.normaliseData(self.df2_1['AgreementValue'],self.df2_2['AgreementValue'],self.df2_3['AgreementValue'],\
            self.df2_4['AgreementValue'],self.df2_5['AgreementValue'],self.df2_3['AgreementValue'])
        # normalising for All influencers
        df4 = self.normaliseData(self.df2_1['AgreementValue'],self.df2_2['AgreementValue'],self.df2_3['AgreementValue'],\
            self.df2_4['AgreementValue'],self.df2_5['AgreementValue'],self.df2_4['AgreementValue'])
        # normalising for No influencers
        df5 = self.normaliseData(self.df2_1['AgreementValue'],self.df2_2['AgreementValue'],self.df2_3['AgreementValue'],\
            self.df2_4['AgreementValue'],self.df2_5['AgreementValue'],self.df2_5['AgreementValue'])
        #  x axis values: session numbers
        x1 = np.arange(1,11)
        # ploting 5 cases in one figure 
        ax1 = plt.subplot2grid(shape=(2,8), loc=(0,0), colspan=2)
        ax2 = plt.subplot2grid((2,8), (0,3), colspan=2)
        ax3 = plt.subplot2grid((2,8), (0,6), colspan=2)
        ax4 = plt.subplot2grid((2,8), (1,1), colspan=2)
        ax5 = plt.subplot2grid((2,8), (1,4), colspan=2)
        # plotting
        ax1.errorbar(x1, self.df2_1['FinalSolutionValue'], yerr=self.df2_1['FinalSolutionValueErr'],color='tab:orange', label='Final chosen solution')
        ax11 = ax1.twinx()
        ax11.errorbar(x1, df1, yerr=self.normSEM(self.df2_1['AgreementValue'],\
            df1,self.df2_1['AgreementValueErr']),color='tab:blue', label='1 influencer')
        ax1.set_title('1 influencer')
        ax2.errorbar(x1, self.df2_2['FinalSolutionValue'], yerr=self.df2_2['FinalSolutionValueErr'],color='tab:orange', label='Final chosen solution')
        ax22 = ax2.twinx()
        ax22.errorbar(x1, df2, yerr=self.normSEM(self.df2_2['AgreementValue'],\
            df2,self.df2_2['AgreementValueErr']),color='tab:blue', label='2 influencers')
        ax2.set_title('2 influencers')
        ax3.errorbar(x1, self.df2_3['FinalSolutionValue'], yerr=self.df2_3['FinalSolutionValueErr'],color='tab:orange', label='Final chosen solution')
        ax33 = ax3.twinx()
        ax33.errorbar(x1, df3, yerr=self.normSEM(self.df2_3['AgreementValue'],\
            df3,self.df2_3['AgreementValueErr']),color='tab:blue', label='3 influencers')
        ax3.set_title('3 influencers')
        ax4.errorbar(x1, self.df2_4['FinalSolutionValue'], yerr=self.df2_4['FinalSolutionValueErr'],color='tab:orange', label='Final chosen solution')
        ax44 = ax4.twinx()
        ax44.errorbar(x1, df4, yerr=self.normSEM(self.df2_4['AgreementValue'],\
            df4,self.df2_4['AgreementValueErr']),color='tab:blue', label='All High SE')
        ax4.set_title('All influencers')
        ax5.errorbar(x1, self.df2_5['FinalSolutionValue'], yerr=self.df2_5['FinalSolutionValueErr'],color='tab:orange', label='Final chosen solution')
        ax55 = ax5.twinx()
        ax55.errorbar(x1, df5, yerr=self.normSEM(self.df2_5['AgreementValue'],\
            df5,self.df2_5['AgreementValueErr']),color='tab:blue', label='Agreement')
        ax5.set_title('No influencers')
        # storing all the axes in a list 
        axs = [ax1,ax2,ax3,ax4,ax5]
        axTwin = [ax11,ax22,ax33,ax44,ax55]
        # assign y limit,labels and grid to all axes
        for a in axs:
            a.grid('True')
            a.set_ylabel('solution quality',color='tab:orange')
            a.set_ylim(0.58,0.74)
            a.tick_params(axis='y', colors='tab:orange')
            a.set_xlabel('Session numbers')
        # assign y limit,labels to all twined y axes
        for ax in axTwin:
            ax.set_ylabel('normalised agreement values with error bars',color='tab:blue')
            ax.tick_params(axis='y', colors='tab:blue')
            ax.set_ylim(0,1.1)
        # plotting legend common to all the figures
        '''handles1, labels1 = ax5.get_legend_handles_labels()
        handles2, labels2 = ax55.get_legend_handles_labels()
        labels=(labels1,labels2)
        plt.figlegend(labels, loc = 'upper center', ncol=5, labelspacing=0.0)'''
        plt.tight_layout()
        plt.show()
    #
    ## Method for ploting and analysing final explored values (spread)
    def analyseISExperiment3Dataset(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 3 IS")
        print("################################")
        print("\n")
        # init the variables
        project = 0
        distanceDispersion = []
        # getting the position x and y as list
        z   =list(zip(self.df3_5['PositionX'].tolist(),self.df3_5['PositionY'].tolist()))
        # # converting list into arrary
        position = np.array(z)
        # getting the solution space
        dfSS = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_3\5_PeakV2\5peakSolutionSpace.csv")
        # converting the dataframe to array
        solutionSpace  = dfSS.to_numpy()
        # iterating for 10 sessions position values
        for i in range(0,len(position),10):
            # taking 10 sessions or one project
            pos = position[i:i+10]
            # increasing teh project by 1
            project += 1
            # calculating the centroid
            centroid = (int(sum(pos[0]) / len(pos[0])),int(sum(pos[1]) / len(pos[1])))
            distances=[]        
            for p in pos:
                # computing the distances
                distance= self.calculateDistance(centroid,p)
                # adding to the list
                distances.append(distance)
            distanceDispersion.append(np.std(distances))
            #print('dispersion for project ',project,'is ',distanceDispersion)
        print('Mean and SEM SD of distances for no influencer',np.mean(distanceDispersion),stats.sem(distanceDispersion))
    #
    ## Method for ploting and analysing final explored values (clusters)
    def analyseISExperiment4Dataset(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 4 IS")
        print("################################")
        print("\n")
        # getting the position x and y as list
        z   =list(zip(self.df3_5['PositionX'].tolist(),self.df3_5['PositionY'].tolist()))
        # init the variables
        project = 0
        numClusters=[]
        # # converting list into arrary
        position = np.array(z)
        # getting the solution space
        dfSS = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_3\5_PeakV2\5peakSolutionSpace.csv")
        # converting the dataframe to array
        solutionSpace  = dfSS.to_numpy()
        # iterating for 10 sessions position values
        for i in range(0,len(position),10):
            # taking 10 sessions or one project
            pos = position[i:i+10]
            # increasing teh project by 1
            project += 1
            #  calculating the bandwidth
            b=estimate_bandwidth(pos)
            # calculating clusters based on the positions of the solutions for each project
            ms = MeanShift(bandwidth=b, bin_seeding=True).fit(pos)
            # getting the labels for each position
            labels = ms.labels_
            # getting the centers of each cluster
            cluster_centers = ms.cluster_centers_
            # getting the unique labels
            labels_unique = np.unique(labels)
            # calculating the uniquie labels for each positions to give the number of clusters
            n_clusters_ = len(labels_unique)
            ############## plotting clusters ##############
            from itertools import cycle
            fig,ax  = plt.subplots(1)
            colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
            for k, col in zip(range(n_clusters_), colors):
                my_members = labels == k
                cluster_center = cluster_centers[k]
                ax.plot(pos[my_members, 0], pos[my_members, 1], col + '.')
                ax.plot(cluster_center[0], cluster_center[1], 'o',alpha =0.5,\
                    markerfacecolor=col,markeredgecolor='k', markersize=14)
            # plotting solution space
            ax.matshow(solutionSpace)
            plt.title('Estimated number of clusters for no influencer: %d' % n_clusters_)
            plt.show()
            # storing the number of clusters in each project
            numClusters.append(n_clusters_)
        # importing library
        import collections
        # counting the occurance of each clusters
        clusters = collections.Counter(numClusters)
        print('1 influencer mode of clusters',clusters)
        print('1 influencer mean of clusters',np.mean(numClusters))
        # converting the values of a dic to a list
        val=list(clusters.values())
        # calculating the standard deviation of teh values (occurance of the clusters numbers)
        print('no influencer std of clusters',np.std(val))
    #
    ## Method for ploting and analysing local quality index
    def analyseISExperiment5_1Dataset(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 5_1 IS")
        print("################################")
        print("\n")
        # getting the final solution column to list
        fv      = self.df3_1['FinalSolutionValue'].tolist()
        # converting list to array
        fValue  = np.array(fv)
        # getting the solution space
        dfSS    = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_3\5_PeakV2\5peakSolutionSpace.csv")
        # converting the dataframe to array
        solutionSpace  = dfSS.to_numpy()
        # init the variables
        localValues     =[]
        # iterating for 10 sessions position values at a time
        for i in range(0,len(fValue),10):
            # taking 10 sessions or one project
            fSolutions = fValue[i:i+10]
            # getting the list of cells where the final value was greater than 0.6
            a   = np.where(fSolutions>0.6)
            # getting the length of the total values
            l   = len(fSolutions)
            # calculating the local quality index
            localQuality    = (len(a[0])/l)*100
            # adding to the list
            localValues.append(localQuality)
        # calculating the mean for all the projects
        localMean   = np.mean(localValues)
        # calculating the error all the projects
        lErr  = stats.sem(localValues)
        # printing the values
        print("Local quality exploration 1 influencer",localMean)
        print("Local quality error 1 influencer",lErr)
    #
    ## Method for ploting and analysing global quality index
    def analyseISExperiment5_2Dataset(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 5_2 IS")
        print("################################")
        print("\n")
        # getting the final solution column to list
        posX      = self.df3_5['PositionX'].tolist()
        posY      = self.df3_5['PositionY'].tolist()
        # converting list to array
        x  = np.array(posX)
        y  = np.array(posY)
        # getting the solution space
        dfSS    = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_3\5_PeakV2\5peakSolutionSpace.csv")
        # converting the dataframe to nd array
        solutionSpace = dfSS.to_numpy()
        # number of neighbouring cells
        neighbours = 4
        # init the variables
        qualityIndexes     =[]
        # iterating for 10 sessions position values at a time
        for i in range(0,len(x),10):
            count = 0
            # taking 10 sessions or one project
            positionX = x[i:i+10]
            positionY = y[i:i+10]
            dfNew = pd.DataFrame({'positionX':positionX,'positionY':positionY})
            for index, row in dfNew.iterrows():
                # calculating the quality quality exploration index for solution space
                neighborMean = self.calculateMeanofNeighbours(neighbours,positionX[index],positionY[index],solutionSpace)
                if neighborMean > 0.6:
                    count = count+1
            (a,b) = np.where(solutionSpace>0.6)
            qIndex = (count/(len(a)))*100
            qualityIndexes.append(qIndex)      
        print("mean quality Index for no influencer is ",np.mean(qualityIndexes),stats.sem(qualityIndexes))
    #
    ## Method for ploting scatter plot for agreement and final value
    def analyseISExperiment6Dataset(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 6 IS")
        print("################################")
        print("\n")
        # getting the file
        df = pd.read_csv(r"C:\Users\Harshika\Documents\socog-model\model_analysis\idea_selection\exp_2\all_final quality5peak.csv")
        # getting unique session numbers
        categories = np.unique(df['SessionNumber'])
        import itertools
        # list of colors
        colors = itertools.cycle(["greenyellow", "gold", "darkorange","red", "blue", "hotpink","olive", "teal", "green","purple"])
        # iterating for all the categories
        for category in categories:
            # dataframe for that unique session number
            data     = df[df['SessionNumber']==category][['FinalSolutionValue','AgreementValue']]
            # plotting for that unique session number
            plt.scatter(data['AgreementValue'],data['FinalSolutionValue'],data= data,c=next(colors),label=str(category))
        # fitting a line to the points
        m, b = np.polyfit(df['AgreementValue'],df['FinalSolutionValue'],1)
        # plotting a line
        plt.plot(df['AgreementValue'], m*(df['AgreementValue']) + b,label='reg line')
        # getting the correlation coefficient
        r,p = stats.kendalltau(df['AgreementValue'],df['FinalSolutionValue'])
        # plotting details
        plt.ylabel('final solution value')
        plt.xlabel('agreement')
        plt.title('kendall r='+str(np.round(r,2))+' p-value= '+str(np.round(p,3)))
        plt.legend()
        plt.grid('True')
        plt.show()
    #
    ## Method for ploting correlation matrix for all the outome variables wrt no of influencers
    def analyseISExperiment7Dataset(self):
        print("\n\n")
        print("################################")
        print("Analysing Data for Experiment 7 IS")
        print("################################")
        print("\n")
        # init lists
        qIndex              = [0.176,0.178,0.177,0.186,0.169]
        lIndex              = [69.3,69.9,70.7,76.2,70.6]
        clusterDistribution = [35.47,34.9,33.3,32.3,38.7]
        spread              = [8.55,8.07,8.33,7.07,9.56]
        contribution        = [1.301,1.305,1.451,1.519,1.320]
        fQuality            = [0.664,0.667,0.666,0.697,0.670]
        influence           = [16.8,22.1,27.6,30.6,12.3]
        influenceDis        = [1/6,2/6,3/6,1,0]
        # making dataframe to store all the values
        df = pd.DataFrame({'global quality index':qIndex,'local quality index':lIndex,\
            'solution cluster distribution':clusterDistribution,'spread':spread,\
                'contribution distribution':contribution,'final solution quality':fQuality,\
                    'influence':influence,'assigned influence distribution':influenceDis})
        # calculating correlation 
        d       = df.corr(method='kendall')
        dfCorr  = d.round(2)
        # plotting the correaltion matrix
        # Exclude duplicate correlations by masking uper right values
        mask = np.zeros_like(dfCorr, dtype=np.bool)
        mask[np.triu_indices_from(mask)] = True
        # Set background color / chart style
        sns.set_style(style = 'white')
        # Set up  matplotlib figure
        f, ax = plt.subplots()
        # Add diverging colormap
        cmap = sns.diverging_palette(10, 250, as_cmap=True)
        # plotting heat map
        xlabel  = ('global quality index','local quality index','solution cluster distribution',\
            'spread','contribution distribution','final solution quality',\
                'influence','assigned influence distribution')
        ylabel  = ('global quality index','local quality index','solution cluster distribution',\
            'spread','contribution distribution','final solution quality',\
                'influence','assigned influence distribution')
        # Draw correlation plot
        sns.heatmap(dfCorr,annot=True, fmt=".1f",xticklabels=xlabel, yticklabels=ylabel,\
            mask=mask, cmap='plasma',square=True,linewidths=.5, cbar_kws={"shrink": .5}, ax=ax)
        plt.tight_layout()
        plt.show()
    #
    ## Method to plot bars
    def plotBars(self):
        fig,ax = plt.subplots()
        ax2 = ax.twinx()
        x = np.arange(1,6)
        y1      = [8.55,8.07,8.33,7.07,9.56]
        y1Err   = [0.27,0.246,0.28,0.227,0.305]
        y2      = [14.4,14.71,15.07,15.36,14.38]
        y2Err   = [0.0881,0.0958,0.0946,0.0945,0.0928]
        ax.bar(x,y1,yerr=y1Err,color='tab:orange',width=0.25,label='final solutions spread')
        ax.set_xticks(np.arange(1,6))
        ax.set_xticklabels(['1 influencer', '2 Influencers', '3 influencers', 'All influencers','No influencer'])
        ax.set_ylabel('values with error bar')
        ax.grid('True')
        ax.tick_params(axis='y', labelcolor='tab:orange')
        ax2.tick_params(axis='y', labelcolor='tab:blue')
        ax.legend(loc=2)
        #ax.set_title('5 peaks: contribution distri')
        ax.set_ylim(6,10)
        ax2.bar(x+0.25,y2,yerr=y2Err,color='tab:blue',width=0.25,label='generated solutions spread')
        ax2.set_ylabel("values with error bar")
        ax2.legend(loc='upper center')
        ax2.set_ylim(12,16)
        fig.tight_layout()
        plt.show()
    #
    ## Method to plot bars for solution 1 and 3 count and quality
    def plotSolutionBars(self):
        fig,ax = plt.subplots(2)
        #x       = ['1 influencer', '2 Influencers', '3 influencers', 'All influencers','No influencer']
        x = np.arange(1,6)
        y11      = [1445/2000,1343/2000,1240/2000,949/2000,1131/2000]
        y13      = [555/2000,657/2000,760/2000,1051/2000,869/2000]
        y21      = [0.644,0.649,0.642,0.667,0.643]
        y23      = [0.718,0.704,0.704,0.725,0.704]
        y21er    = [0.0043,0.0044,0.0047,0.0050,0.0050]
        y23er    = [0.0057,0.0054,0.0050,0.0040,0.0047]
        #df = pd.DataFrame({'y1':y1,'y1Err':y1Err,'y2':y2,'y2Err':y2Err})
        #df[['y1']].plot(kind='bar',yerr=df[['y1Err']], color='green', ax=ax, width=0.25, position=1,rot= 0)
        #df[['y2']].plot(kind='bar',yerr=df[['y2Err']], color='red', ax=ax2, width=0.25, position=0,rot= 0)
        ax[0].bar(x,y11,color='tab:orange',width=0.25,label='One solutions')
        ax[0].bar(x+0.25,y13,color='tab:blue',width=0.25,label='Three solutions')
        ax[0].set_xticks(np.arange(1,6))
        ax[0].set_xticklabels(['1 influencer', '2 Influencers', '3 influencers', 'All influencers','No influencer'])
        ax[0].set_ylabel('counts')
        ax[0].grid('True')
        ax[0].legend()
        #ax[0].set_ylim(0.0025,0.0035)
        ax[1].bar(x,y21,yerr=y21er,color='tab:orange',width=0.25,label='One solutions quality')
        ax[1].bar(x+0.25,y23,yerr=y23er,color='tab:blue',width=0.25,label='Three solutions quality')
        ax[1].set_xticks(np.arange(1,6))
        ax[1].set_xticklabels(['1 influencer', '2 Influencers', '3 influencers', 'All influencers','No influencer'])
        ax[1].set_ylabel("solution quality with error bars")
        ax[1].grid('True')
        ax[1].legend()
        ax[1].set_ylim(0.6,0.74)
        fig.tight_layout()
        plt.show()
    #
    ## Method for calculating the distance between influencer and low or high SE agent
    # @param position1 position of the influencer
    # @param position2 position of the agent
    # @return distance distance between the two agents
    def calculateDistance(self,position1,position2):
        # computing the deltas
        deltaX = position1[0] - position2[0]
        deltaY = position1[1] - position2[1]
        # computing the distance
        distance = np.sqrt(np.power(deltaX,2) + np.power(deltaY,2))
        return distance
    #
    ####################################
    ## Method to normalise column data
    # @param coulmns od all the 5 data set
    # @returns normalised column 
    def normaliseData(self,columnName1,columnName2,columnName3,columnName4,columnName5,columnName):
        maxValue    = max(columnName1.max(),columnName2.max(),columnName3.max(),columnName4.max(),columnName5.max())
        normColumn  = columnName/maxValue
        return normColumn
    #
    ## Method to normalise column data standard error
    # @param coulmn column of the data set
    # @returns normalised SEM column 
    def normSEM(self,orgValue, normValue, biasColumn):
        factor = orgValue.mean()/ normValue.mean()
        reducedBias = biasColumn/factor
        return reducedBias
    #
    ## Method that gives a neighbours of the a passed position
    # @param dataframe 
    # @param n number of neighbour cells around the position p
    # @param rowNumber row number of the position
    # @param columnNumber column number of the position
    # @returns meanResult mean of the resultant matrix of reduced size 
    def calculateMeanofNeighbours(self, n, rowNumber, columnNumber, dataframe):
        m= dataframe
        # cutting the matrix portion
        m_cut = m[int(rowNumber-n/2):int(rowNumber+n/2),\
                  int(columnNumber-n/2):int(columnNumber+n/2)]
        meanResult= np.mean(m_cut)
        return meanResult
    #
    if __name__ == "__main__":
        pass
