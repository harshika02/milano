# standard imports
import pandas as pd
from sqlalchemy.orm import sessionmaker
import sqlalchemy
import tqdm
import numpy as np
import os
from scipy import stats

# custom imports
from models.failure import Failure as ModelFailure
from models.success import Success as ModelSuccess
from models.agent import Agent as ModelAgent
from models.session import Session as ModelSession
from models.steps import IdeaGenerationStep as ModelIdeaGenerationStep
from models.project import Project as ModelProject
from models.agent import AgentCharacteristics as ModelAgentCharacteristics
#from models.base import databaseSession
from models.model import *
from models.base import engine
from models.base import *

from model_analysis.constants import *
from model_analysis.csv_manager import CsvManager


## Class that implements the interfaces for pulling data out of the database
class DataExtractor():
    ## Constructor method
    def __init__ (self):
        # creating a new database session
        DatabaseSession = sessionmaker(bind=engine)
        self.dbSession  = DatabaseSession()
    #
    ## Method that pulls the data from the database and stores them in a csv file
    def extractExperiment1Dataset(self):
        print("\n\n")
        print("################################")
        print("Extracting Data for Experiment 1")
        print("################################")
        print("\n")
        # init the result
        record      = {'ProjectId':[],'SessionNumber':[],'AgentId':[],\
            'AgentExperience':[],'AgentSelfEfficacy':[],'AgentReputation':[],\
                'FinalValue':[],'ValueExp':[]}
        # getting the projects
        projects    = self.dbSession.query(ModelProject).all()
        # setting up the progress bar
        pbar        = tqdm.tqdm(total=len(projects))
        for project in projects:
            # getting all the sessions of that project
            for session in project.sessions:
                # getting all the agents of that session
                for agent in session.agents:
                    # getting the steps realted to agent id and session id
                    steps    = self.dbSession.query(ModelIdeaGenerationStep).filter\
                        (ModelIdeaGenerationStep.sessionId==session.id).filter(ModelIdeaGenerationStep.agentId==agent.id)
                    if len(steps.all()) == 0:
                        continue
                    # getting the last step
                    lastStep = steps.order_by(sqlalchemy.desc(ModelIdeaGenerationStep.number)).first()
                    # computing the positions
                    positionLearning          = lastStep.position
                    positionExpLearning       = lastStep.positionSB
                    positionNoLearning        = lastStep.intendedPosition
                    # getting the value from solution space to actual position of all the learning
                    positionLearningValue     = project.solutionSpace[positionLearning[0],positionLearning[1]]
                    # getting the value from solution space to position of only experience learning
                    positionExpLearningValue  = project.solutionSpace[positionExpLearning[0],positionExpLearning[1]]
                    # getting the value from solution space to intented position without any learning
                    positionNoLearningValue   = project.solutionSpace[int(positionNoLearning[0]),int(positionNoLearning[1])] 
                    # getting the experience
                    agentSessionLink    = self.dbSession.query(ModelAgentCharacteristics).filter\
                        (ModelAgentCharacteristics.session_id==session.id).filter(ModelAgentCharacteristics.agent_id==agent.id).first()       
                    # writing to dictionary
                    record['ProjectId'].append(project.id)
                    record['SessionNumber'].append(session.number)
                    record['AgentId'].append(agent.id)
                    record['AgentExperience'].append(agentSessionLink.experience)
                    record['AgentSelfEfficacy'].append(agentSessionLink.selfEfficacy)
                    record['AgentReputation'].append(agentSessionLink.reputation)
                    record['FinalValue'].append(positionLearningValue)
                    record['ValueExp'].append(positionExpLearningValue)
            # updating the progress bar
            #pbar.update(1)
        # converting the file to dataframe
        df = pd.DataFrame(data=record)
        # exporting the file
        df.to_csv(EXPERIMENT1_FILENAME, index=False)
    #
    ## Method that pulls the data from the database and stores the average values in a csv file
    def extractExperiment2Dataset(self):
        print("\n\n")
        print("################################")
        print("Extracting Data for Experiment 2")
        print("################################")
        print("\n")
        # init the result
        headers = ['SessionNumber', 'AgentId', 'FinalValue','Self-efficacy']
        # init the csv manager
        csvManager = CsvManager(EXPERIMENT2_FILENAME, headers)
        # getting the projects
        projects    = self.dbSession.query(ModelProject).all()
        projects.pop(128)
        for project in projects:
            # getting all the sessions of that project
            for session in project.sessions:
                # list of agents who are not influencers
                #otherAgents =  []
                # getting all the agents of that session
                for agent in session.agents:
                    # getting the steps realted to agent id and session id
                    steps    = self.dbSession.query(ModelIdeaGenerationStep).filter\
                        (ModelIdeaGenerationStep.sessionId==session.id).filter\
                            (ModelIdeaGenerationStep.agentId==agent.id)
                    if len(steps.all()) == 0:
                        continue
                    # getting the last step
                    lastStep = steps.order_by(sqlalchemy.desc(ModelIdeaGenerationStep.number)).first()
                    # computing the positions
                    positionLearning          = lastStep.position
                    positionLearningValue     = project.solutionSpace[positionLearning[0],positionLearning[1]]
                    # getting the agent session link
                    agentSessionLink    = self.dbSession.query(ModelAgentCharacteristics).filter\
                        (ModelAgentCharacteristics.session_id==session.id).filter\
                            (ModelAgentCharacteristics.agent_id==agent.id).first()
                    # creating the new line
                    newLine = {}
                    newLine['SessionNumber'] = session.number
                    newLine['AgentId'] = agent.id
                    newLine['FinalValue'] = positionLearningValue
                    newLine['Self-efficacy'] = agentSessionLink.selfEfficacy
                    # writing the new line
                    csvManager.writeRow(newLine)
        # finalising the file
        csvManager.finaliseFile()
    #
    ## Method for finding the lowest and highest SE agent
    def extractingLowestSEagentData(self):
        print("################################")
        print("Extracting Data for Experiment 2.3")
        print("################################")
        print("\n")
        # create folder for experiment
        experimentFolderPath    = EXP_ROOTFOLDERNAME % (2)
        self.createFolder(experimentFolderPath)
        # creating the folder for this experiment export
        newFolderPath   = self.createExperimentFolder(experimentFolderPath)
        # declaring the record for CSV
        record      = {'SessionNumber':[],'FinalValue':[],'LearningFailure':[],\
            'LearningSuccess':[],'SocialLearning':[],'finalErr':[],'failureErr':[],\
                'successesBiasErr':[],'influencerBiasErr':[]}
        # getting the projects
        projects    = self.dbSession.query(ModelProject).all()
        # list to store high self efficacy agent other than influencer
        lowSEagentLinks    = []
        #agentSessionLinks   = []
        projects.pop()
        for project in projects[-169:]:
            # getting all the sessions of that project
            for session in project.sessions:
                # list of agents who are not influencers
                lowestSEagentLink = None
                lowestSEagent     = None
                # getting all the agents of that session
                for agent in session.agents:
                    agentSessionLink    = self.dbSession.query(ModelAgentCharacteristics).filter\
                        (ModelAgentCharacteristics.session_id==session.id).filter\
                            (ModelAgentCharacteristics.agent_id==agent.id).first()
                    if lowestSEagentLink is None:
                        lowestSEagentLink = agentSessionLink
                    elif agentSessionLink.selfEfficacy < lowestSEagentLink.selfEfficacy:
                        lowestSEagentLink = agentSessionLink
                        lowestSEagent     = agent
                if lowestSEagentLink is None:
                    print(project.id, session.id)
                else:
                    lowSEagentLinks.append(lowestSEagentLink)
        # getting solution space
        solutionSpace = project.solutionSpace
        # getting the session ids
        sessionIds = [link.session_id for link in lowSEagentLinks]
        # getting the agent ids
        agentIds = [link.agent_id for link in lowSEagentLinks]
        # getting the possible values for session numbers
        selectedSessions  = [session.number for session in self.dbSession.query(ModelSession).all() if session.id in sessionIds]
        sessionNumberEnum = np.unique(selectedSessions)
        ################################
        ##### extracting the steps of lowest SE agent
        ################################
        # dictionary containing the final values; keys are sessionNumbers
        positionLearningValues      = {}
        # dictionary containing the success bias value keys are sessionNumbers
        successesBiasValues         = {}
        # dictionary containing the influencer bias value keys are sessionNumbers
        influencerBiasValues        = {}
        # dictionary containing the failure value keys are sessionNumbers
        failureValues               = {}
        for link in lowSEagentLinks:
            # getting the steps realted to agent id and session id
            steps    = self.dbSession.query(ModelIdeaGenerationStep).filter\
                (ModelIdeaGenerationStep.sessionId==link.session_id).filter\
                    (ModelIdeaGenerationStep.agentId==link.agent_id)
            # getting the session from the link
            session  = self.dbSession.query(ModelSession).filter(ModelSession.id==link.session_id).first()
            # getting the project from the session id
            project  = self.dbSession.query(ModelProject).filter(ModelProject.id==session.projectId).first()
            if len(steps.all()) == 0:
                continue
            # getting the last step
            lastStep = steps.order_by(sqlalchemy.desc(ModelIdeaGenerationStep.number)).first()
            # getting the failure
            failures = self.dbSession.query(ModelFailure).filter\
                (ModelFailure.id==link.session_id).filter\
                    (ModelFailure.agentId==link.agent_id).all()
            # computing the positions
            positionLearning          = lastStep.position
            # [1] POSITION LEARNING VALUE (from INFLUENCER)
            # computing it
            positionLearningValue     = project.solutionSpace[positionLearning[0],positionLearning[1]]
            # checking if key already exists
            if session.number in positionLearningValues.keys():
                positionLearningValues[session.number].append(positionLearningValue)
            else:
                positionLearningValues[session.number] = [positionLearningValue]
            successesBiasSum    = 0.0
            influencerBiasSum   = 0.0
            for step in steps:
                # computing the root of the squares of the biases at every step
                successesBiasSum     += np.sqrt(np.power(step.successesBias[0],2) + np.power(step.successesBias[1],2))
                influencerBiasSum    += np.sqrt(np.power(step.sumInfluencersBias[0],2) + np.power(step.sumInfluencersBias[1],2))
            # [4] BIAS SUCCESS
            # checking if key exists
            if session.number in successesBiasValues.keys():
                successesBiasValues[session.number].append(successesBiasSum)
            else:
                successesBiasValues[session.number] = [successesBiasSum]
            # [5] BIAS INFLUENCER
            # checking if key exists
            if session.number in influencerBiasValues.keys():
                influencerBiasValues[session.number].append(influencerBiasSum)
            else:
                influencerBiasValues[session.number] = [influencerBiasSum]
       # computing the values for same session numbers
        for sessionNum in sessionNumberEnum:
            # getting the sessions
            selSessionIds   = [session.id for session in self.dbSession.query(ModelSession).all() if (session.id in sessionIds) and (session.number==sessionNum)]
            # init the selected sessions list
            selectedSessions    = []
            # iterating one by one because of SQL limitations
            for selectedSessionId in selSessionIds:
                # getting the session
                #session    = self.dbSession.query(ModelSession).filter(ModelSession.id==selectedSessionId).filter(ModelSession.recalledFailures.any(ModelFailure.agentId.in_(agentIds))).all()
                session    = self.dbSession.query(ModelSession).filter(ModelSession.id==selectedSessionId).first()
                # skipping if no session found
                if session is None:
                    continue
                # checking if any of recalled failures is associated with one of the agents
                found   = False
                for recalledFailure in session.recalledFailures:
                    if recalledFailure.agentId in agentIds:
                        found   = True
                        break
                # appending the value
                if found:
                    selectedSessions.append(session)
            # line below is commented because of SQL limitations
            #selSessions = self.dbSession.query(ModelSession).filter(ModelSession.id.in_(selSessionIds)).filter(ModelSession.recalledFailures.any(ModelFailure.agentId.in_(agentIds))).all()
            # [4] GETTING THE FAILURE PERCENTAGE IN THAT SESSION
            # getting the failures
            failures = []
            for session in selectedSessions:
                for failure in [f for f in session.recalledFailures if f.agentId in agentIds]:
                    failures.append(failure)
            # getting the limits
            xlim,ylim           = np.shape(solutionSpace)
            totalFailuresPixels = self.computeFailureArea(failures,xlim,ylim)
            # getting the number of failure area pixels
            numFailurePixels    = len(totalFailuresPixels)
            # getting the total number of pixels
            failureValue = numFailurePixels/(xlim*ylim)
            if sessionNum in failureValues.keys():
                failureValues[sessionNum].append(failureValue)
            else:
                failureValues[sessionNum] = [failureValue]
            # getting the mean value total number of pixels for that session
            failureMean = np.mean(failureValues[sessionNum])
            # computing the error value
            numPixelsPerFailure  = []
            for failure in failures:
                # computing the number of pixels related to a failure
                numPixels = len(self.computeFailureArea([failure],xlim,ylim))
                # appending the value
                numPixelsPerFailure.append(numPixels/(xlim*ylim))
            failureAreaErr     = stats.sem(numPixelsPerFailure)
            # computing the final value
            finalValue  = np.mean(positionLearningValues[sessionNum])
            # getting the error value
            finalErr    = stats.sem(positionLearningValues[sessionNum])
            # computing the average of the biases
            successesBiasAvg    = np.mean(successesBiasValues[sessionNum])
            # getting error value
            successesBiasErr    = stats.sem(successesBiasValues[sessionNum])
            influencerBiasAvg   = np.mean(influencerBiasValues[sessionNum])
            # getting the std error value
            influencerBiasErr   = stats.sem(influencerBiasValues[sessionNum])
            # storing the values
            record['SessionNumber'].append(sessionNum)
            record['FinalValue'].append(finalValue)
            record['LearningFailure'].append(failureMean)
            record['LearningSuccess'].append(successesBiasAvg)
            record['SocialLearning'].append(influencerBiasAvg)
            record['finalErr'].append(finalErr)
            record['failureErr'].append(failureAreaErr)
            record['successesBiasErr'].append(successesBiasErr)
            record['influencerBiasErr'].append(influencerBiasErr)
        # converting the file to dataframe
        df = pd.DataFrame(data=record)
        # generating file name
        fileName    = newFolderPath + "/" + "3exp_200_1peak.csv"
        # exporting the filen
        df.to_csv(fileName, index=False)
    #
    ## Method that pulls the data from the database and stores them in a csv file for position plotting          
    def extractExperiment3Dataset(self):
        print("\n\n")
        print("################################")
        print("Extracting Data for Experiment 3")
        print("################################")
        print("\n")
        # create folder for experiment
        experimentFolderPath    = EXP_ROOTFOLDERNAME % (3)
        self.createFolder(experimentFolderPath)
        # creating the folder for this experiment export
        newFolderPath   = self.createExperimentFolder(experimentFolderPath)
        # init the result
        record      = {'ProjectID':[],'SessionNumber':[],\
            'InfluencerPosition':[],'Influencer1Position':[],'Influencer2Position':[],\
                'LowestSEagentPosition':[]}
        # getting the projects
        #projects    = self.dbSession.query(ModelProject).all()
        # getting only one project
        projects    = self.dbSession.query(ModelProject).first()
        #for project in projects:
        # getting all the sessions of that project
        for session in project.sessions:
            # link of low SE agents
            lowestSEagentLink = None
            lowestSEagent     = None
            # link of high SE agents who are not influencers
            #highSEagentLink = None
            #highSEagent     = None
            # influencer as none
            influencer      = None
            # getting all the agents of that session
            for agent in session.agents:
                agentSessionLink    = self.dbSession.query(ModelAgentCharacteristics).filter\
                    (ModelAgentCharacteristics.session_id==session.id).filter\
                        (ModelAgentCharacteristics.agent_id==agent.id).first()
                # finding the influencer
                if agentSessionLink.isInfluencer == False:
                    # finding the high SE agent
                    if highSEagentLink is None:
                        highSEagentLink = agentSessionLink
                        highSEagent     = agent
                    # finding the low SE agent
                    if lowestSEagentLink is None:
                        lowestSEagentLink = agentSessionLink
                        lowestSEagent     = agent
                    elif  agentSessionLink.selfEfficacy < lowestSEagentLink.selfEfficacy:
                        lowestSEagentLink = agentSessionLink
                        lowestSEagent     = agent
                    elif  agentSessionLink.selfEfficacy > highSEagentLink.selfEfficacy:
                        highSEagentLink = agentSessionLink
                        highSEagent     = agent
                else:
                    influencer = agent
                if lowestSEagent is None:
                    pass
                else:
                    lowestSEagentPosition = self.getStepPosition(lowestSEagent,session)
                if highSEagent is None:
                    pass
                else:
                    highSEagentPosition = self.getStepPosition(highSEagent,session)
                if influencer is None:
                    pass
                else:
                    influencerPosition = self.getStepPosition(influencer,session)
            # storing the values
            record['ProjectID'].append(project.id)
            record['SessionNumber'].append(session.number)
            record['InfluencerPosition'].append(influencer1Position)
            record['InfluencerPosition'].append(influencer2Position)
            record['LowestSEagentPosition'].append(lowestSEagentPosition)
        # converting the file to dataframe
        df = pd.DataFrame(data=record)
        # generating file name
        fileName    = newFolderPath + "/" + "finalPositions.csv"
        # exporting the file
        df.to_csv(fileName, index=False)
    #
    ## Method for extracting all the steps for agents in the sessions for trajectories and 2 influenecrs
    def extractExperiment4Dataset(self):
        print("\n\n")
        print("################################")
        print("Extracting Data for Experiment 4")
        print("################################")
        print("\n")
        # create folder for experiment
        experimentFolderPath    = EXP_ROOTFOLDERNAME % (4)
        self.createFolder(experimentFolderPath)
        # creating the folder for this experiment export
        newFolderPath   = self.createExperimentFolder(experimentFolderPath)
        # init the result
        record      = {'ProjectID':[],'SessionNumber':[],\
            'Influencer1Positions':[],'Influencer2Positions':[],\
                'HighestSEagentPositions':[]}
        # getting the projects
        #projects    = self.dbSession.query(ModelProject).all()
        # getting the only one project
        project    = self.dbSession.query(ModelProject).first()
        # getting all the sessions of that project
        for session in project.sessions[1:]:
            # link of low SE agents
            #lowestSEagentLink = None
            #lowestSEagent     = None
            # link of high SE agents who are not influencers
            highSEagentLink = None
            highSEagent     = None
            # influencer1 as none
            influencer1      = None
            # influencer2 as none
            influencer2      = None
            # getting all the agents of that session
            for agent in session.agents[1:]:
                agentSessionLink    = self.dbSession.query(ModelAgentCharacteristics).filter\
                (ModelAgentCharacteristics.session_id==session.id).filter\
                    (ModelAgentCharacteristics.agent_id==agent.id).first()
                # finding the influencers
                if str(agent.id) in agentSessionLink.selectedInfluencers.keys():
                    if influencer1 is None:
                        influencer1 = agent
                    else:
                        influencer2 = agent
                else:
                    # finding the high SE agent
                    if highSEagentLink is None:
                        highSEagentLink = agentSessionLink
                        highSEagent     = agent
                    # finding the low SE agent
                    elif agentSessionLink.selfEfficacy < highSEagentLink.selfEfficacy:
                        highSEagentLink = agentSessionLink
                        highSEagent     = agent
            # get all the steps of the influencer1
            i1Steps    = self.dbSession.query(ModelIdeaGenerationStep).filter\
                (ModelIdeaGenerationStep.sessionId==session.id).filter\
                (ModelIdeaGenerationStep.agentId==influencer1.id).all()
            # get all the steps of the influencer2
            i2Steps    = self.dbSession.query(ModelIdeaGenerationStep).filter\
                (ModelIdeaGenerationStep.sessionId==session.id).filter\
                (ModelIdeaGenerationStep.agentId==influencer2.id).all()
            # get all the steps of the low SE agent
            '''lSteps    = self.dbSession.query(ModelIdeaGenerationStep).filter\
                (ModelIdeaGenerationStep.sessionId==session.id).filter\
                (ModelIdeaGenerationStep.agentId==lowestSEagent.id).all()'''
            # get all the steps of the high SE agent
            hSteps    = self.dbSession.query(ModelIdeaGenerationStep).filter\
                (ModelIdeaGenerationStep.sessionId==session.id).filter\
                (ModelIdeaGenerationStep.agentId==highSEagent.id).all()
            # creating list to store positions of influencer, low and high SE agent
            i1PositionList  = []
            i2PositionList  = []
            #lPositionList   = []
            hPositionList   = []
            # writing the steps in the record
            for stepNum in range(len(hSteps)):
                # project id
                record['ProjectID'].append(project.id)
                record['SessionNumber'].append(session.number)
                # influencer step
                record['Influencer1Positions'].append(list(i1Steps[stepNum].position))
                record['Influencer2Positions'].append(list(i2Steps[stepNum].position))
                #record['LowestSEagentPositions'].append(list(lSteps[stepNum].position))
                record['HighestSEagentPositions'].append(list(hSteps[stepNum].position))
        # storing the solution space
        solutionSpace   = pd.DataFrame(project.solutionSpace)
        fileName        = newFolderPath + "/" + str(project.id) + ".csv"
        solutionSpace.to_csv(fileName, index=False, header=False)
        # converting the file to dataframe
        df = pd.DataFrame(data=record)
        # generating file name
        fileName    = newFolderPath + "/AllPositionsAllinfluenecrs.csv"
        # exporting the file
        df.to_csv(fileName, index=False)
        #
    ## Method that pulls the data of all the agent final values
    def extractExperiment5Dataset(self):
        print("\n\n")
        print("################################")
        print("Extracting Data for Experiment 5")
        print("################################")
        print("\n")
        # create folder for experiment
        experimentFolderPath    = EXP_ROOTFOLDERNAME % (5)
        self.createFolder(experimentFolderPath)
        # creating the folder for this experiment export
        newFolderPath   = self.createExperimentFolder(experimentFolderPath)
        # generating file name
        fileName    = newFolderPath + "/AllAgentsAllPositions_5peak_4Influ.csv"
        # init the result
        headers     = ['ProjectId','SessionNumber','AgentId','PositionValues','PositionX','PositionY']
        # init the csv manager
        csvManager  = CsvManager(fileName, headers)
        # getting the projects
        projects    = self.dbSession.query(ModelProject).all()
        projects.pop(128)
        # taking all the projects
        for project in projects:
            # getting all the sessions of that project
            for session in project.sessions:
                # getting all the agents of that session
                for agent in session.agents:
                    # getting the steps realted to agent id and session id
                    steps    = self.dbSession.query(ModelIdeaGenerationStep).filter\
                        (ModelIdeaGenerationStep.sessionId==session.id).filter(ModelIdeaGenerationStep.agentId==agent.id)
                    if len(steps.all()) == 0:
                        continue
                    # getting the last step
                    for step in steps:
                        # computing the positions
                        positionLearning          = step.position
                        # getting the value from solution space to actual position of all the learning
                        positionValue     = project.solutionSpace[positionLearning[0],positionLearning[1]]
                        # composing new line
                        newLine = {}
                        newLine['ProjectId'] = project.id
                        newLine['SessionNumber'] = session.number
                        newLine['AgentId'] = agent.id
                        newLine['PositionValues'] = positionValue
                        newLine['PositionX'] = positionLearning[0]
                        newLine['PositionY'] = positionLearning[1]
                        # writing the new line
                        csvManager.writeRow(newLine)
        # storing the solution space
        solutionSpace   = pd.DataFrame(project.solutionSpace)
        fileName        = newFolderPath + "/" + str(project.id) + ".csv"
        solutionSpace.to_csv(fileName, index=False, header=False)
        # finalising the file
        csvManager.finaliseFile()
    #
    ## Method to extract trust and influence values for an agent
    def extractExperiment6Dataset(self):
        print("\n\n")
        print("################################")
        print("Extracting Data for Experiment 6")
        print("################################")
        print("\n")
         # create folder for experiment
        experimentFolderPath    = EXP_ROOTFOLDERNAME % (6)
        self.createFolder(experimentFolderPath)
        # creating the folder for this experiment export
        newFolderPath   = self.createExperimentFolder(experimentFolderPath)
        # init the result
        record      = {'SessionNumber':[],'trustMean':[],'trustDistribution':[],'InfluMean':[],'InfluDistribution':[]}
        # getting the projects
        projects    = self.dbSession.query(ModelProject).all()
        trustValues = {}
        trustValuesDistribution ={}
        influValues = {}
        influValuesDistribution ={}
        for project in projects:
            # getting all the sessions of that project
            for session in project.sessions:
                # getting all the agents of that session
                for agent in session.agents:
                    # getting the trust
                    agentSessionLink    = self.dbSession.query(ModelAgentCharacteristics).filter\
                        (ModelAgentCharacteristics.session_id==session.id).filter(ModelAgentCharacteristics.agent_id==agent.id).first()
                    # getting the trust and influence value of an agent for other agents
                    trustDic    = agentSessionLink.trust
                    influValDic = agentSessionLink.influencersValue
                    # storing only the values of teh dictionary into list
                    trustVal    = (list(trustDic.values()))
                    influVal    = (list(influValDic.values()))
                    # checking if key exists
                    # computing the mean
                    if session.number in trustValues.keys():
                        trustValues[session.number].append(np.mean(trustVal))
                    else:
                        trustValues[session.number] = [np.mean(trustVal)]
                    if session.number in influValues.keys():
                        influValues[session.number].append(np.mean(influVal))
                    else:
                        influValues[session.number] = [np.mean(influVal)]
                    # computing the std
                    if session.number in trustValuesDistribution.keys():
                        trustValuesDistribution[session.number].append(np.std(trustVal))
                    else:
                        trustValuesDistribution[session.number] = [np.std(trustVal)]
                    if session.number in influValuesDistribution.keys():
                        influValuesDistribution[session.number].append(np.std(influVal))
                    else:
                        influValuesDistribution[session.number] = [np.std(influVal)]
        sessions  = [session.number for session in self.dbSession.query(ModelSession).all()]
        sessionNumbers = np.unique(sessions)
        for sessNum in sessionNumbers:
            # getting mean value for all the 200 sessions numbers
            trust                   = np.mean(trustValues[sessNum])
            trustDistribution       = np.mean(trustValuesDistribution[sessNum])
            influence               = np.mean(influValues[sessNum])
            influenceDistribution   = np.mean(influValuesDistribution[sessNum])
            # storing the values
            record['SessionNumber'].append(sessNum)
            record['trustMean'].append(trust)
            record['trustDistribution'].append(trustDistribution)
            record['InfluMean'].append(influence)
            record['InfluDistribution'].append(influenceDistribution)         
        # converting the file to dataframe
        df = pd.DataFrame(data=record)
        # generating file name
        fileName    = newFolderPath + "/" + "trust_influence__5peaks.csv"
        # exporting the filen
        df.to_csv(fileName, index=False)         
    #
    ######################################################
    ######################################################
    ##  Method for getting steps for agents in sessions
    # @param agent agent whose steps are needed
    # @param session session in which there are steps
    # @return finalPosition final position of the agent
    def getStepPosition(self,agent,session):
        # get the position of the influencer
        steps    = self.dbSession.query(ModelIdeaGenerationStep).filter\
            (ModelIdeaGenerationStep.sessionId==session.id).filter\
                (ModelIdeaGenerationStep.agentId==agent.id)
        if len(steps.all()) == 0:
            finalPosition = None
        else:
            # getting the last step
            lastStep = steps.order_by(sqlalchemy.desc(ModelIdeaGenerationStep.number)).first()
            # computing the positions
            finalPosition          = lastStep.position
        return finalPosition
    #
    ## Method that creates a folder if it doesn't exist
    def createFolder(self, folderPath):
        # checking if the folder exists
        if os.path.exists(folderPath):
            pass
        else:
            os.mkdir(folderPath)
    #
    ## Method that creates a new experiment folder
    # @param experimentFolder Base folder for the experiment
    # @return Path to new folder
    def createExperimentFolder(self, experimentFolder):
        # init loop variables
        result = False
        i = 0
        # starting to search an available folder
        while not result:
            # composing the filename
            newFolderPath   = experimentFolder+str(i)
            # checking if exists
            if os.path.exists(newFolderPath):
                # updating the counter
                i += 1
            else:
                result = True
        # creating the folder 
        self.createFolder(newFolderPath)
        # returning the path of the folder
        return newFolderPath
    #
    ## Method for computing the failure area in percentage of the total solution space area
    # @param failures List of failures
    # @param xlim limit of the solution space
    # @param ylim limit of the solution space
    # @returns failurePixels List of pixels that are affected by an error
    def computeFailureArea(self,failures,xlim,ylim):
        # init the list of failure points
        failurePixels = []
        # iterating on solution space
        for x in range(0,xlim):
            for y in range(0,ylim):
                for failure in failures:
                    # computing the distance between the centers
                    # computing the deltas
                    deltaX      = failure.position[0]-x
                    deltaY      = failure.position[1]-y
                    # computing the distance
                    distance    = np.sqrt(np.power(deltaX, 2)+np.power(deltaY, 2))
                    # checking if the point in the solution space is within failure circle
                    if distance <= failure.radius:
                        point = (int(x),int(y))
                        failurePixels.append(point)
                        break
        return failurePixels
    #
if __name__ == '__main__':
    pass
    
                    