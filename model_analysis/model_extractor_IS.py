# standard imports
import pandas as pd
from sqlalchemy.orm import sessionmaker
import sqlalchemy
import tqdm
import numpy as np
import os
from scipy import stats

# custom imports
from models.failure import Failure as ModelFailure
from models.success import Success as ModelSuccess
from models.agent import Agent as ModelAgent
from models.session import Session as ModelSession
from models.steps import IdeaGenerationStep as ModelIdeaGenerationStep
from models.project import Project as ModelProject
from models.agent import AgentCharacteristics as ModelAgentCharacteristics
from models.selection import IdeaSelection as ModelIdeaSelection
#from models.base import databaseSession
from models.model import *
from models.base import engine
from models.base import *

from model_analysis.constants import *
from model_analysis.csv_manager import CsvManager


## Class that implements the interfaces for pulling data out of the database
class DataExtractorIdeaSelection():
    ## Constructor method
    def __init__ (self):
        # creating a new database session
        DatabaseSession = sessionmaker(bind=engine)
        self.dbSession  = DatabaseSession()
    #
    ## Method that pulls the data of selected agents from the database and stores them in a csv file
    def extractISExperiment1Dataset(self):
        print("\n\n")
        print("################################")
        print("Extracting Data for Idea Selection Experiment 1")
        print("################################")
        print("\n")
        # create folder for experiment
        experimentFolderPath    = EXP_ROOTFOLDERNAME_IS % (1)
        self.createFolder(experimentFolderPath)
        # creating the folder for this experiment export
        newFolderPath   = self.createExperimentFolder(experimentFolderPath)
        # init the result
        record      = {'SessionNumber':[],'SelectedAgentId':[],'SolutionSA':[],'PositionSA_X':[],'PositionSA_Y':[]}
        # getting all the projects
        projects    = self.dbSession.query(ModelProject).all()
        # storing just one project for the extracting solution space 
        project = projects[0]
        # getting all the idea selections 
        ideaSelections    = self.dbSession.query(ModelIdeaSelection).all()
        for i in ideaSelections[-2000:]:
            # getting all the sessions related to the idea selection session
            sessionID  = i.sessionId
            session = self.dbSession.query(ModelSession).filter\
                        (ModelSession.id==sessionID).first()
            # getting all the agents of that session
            for agent in session.agents:
                if str(agent.id) in i.agreementValues.keys():
                    # getting the steps realted to agent id and session id
                    steps    = self.dbSession.query(ModelIdeaGenerationStep).filter\
                        (ModelIdeaGenerationStep.sessionId==session.id).filter(ModelIdeaGenerationStep.agentId==agent.id)
                    if len(steps.all()) == 0:
                        continue
                    # getting the last step
                    lastStep = steps.order_by(sqlalchemy.desc(ModelIdeaGenerationStep.number)).first()
                    # computing the positions
                    solutionPosition          = lastStep.position
                    # getting the value from solution space to actual position of all the learning
                    solutionValue     = project.solutionSpace[solutionPosition[0],solutionPosition[1]]
                    # writing to dictionary
                    record['SessionNumber'].append(session.number)
                    record['SelectedAgentId'].append(agent.id)
                    record['SolutionSA'].append(solutionValue)
                    record['PositionSA_X'].append(solutionPosition[0])
                    record['PositionSA_Y'].append(solutionPosition[1])
                else:
                    pass
        # converting the file to dataframe
        df = pd.DataFrame(data=record)
        # generating file name
        fileName    = newFolderPath + "/" + "5Mixpeak_D4_1exp.csv"
        # exporting the file
        df.to_csv(fileName, index=False)
    #
    ## Method that pulls the data of feedbackValue,solution and agreementValue from the database and stores them in a csv file
    def extractISExperiment2Dataset(self):
        print("\n\n")
        print("################################")
        print("Extracting Data for Idea Selection Experiment 2")
        print("################################")
        print("\n")
        # create folder for experiment
        experimentFolderPath    = EXP_ROOTFOLDERNAME_IS % (2)
        self.createFolder(experimentFolderPath)
        # creating the folder for this experiment export
        newFolderPath   = self.createExperimentFolder(experimentFolderPath)
        # init the result
        record      = {'SessionNumber':[],'AgreementValue':[],'FeedbackValue':[],'FinalSolutionValue':[],\
            'AgreementValueErr':[],'FeedbackValueErr':[],'FinalSolutionValueErr':[]}
        # getting the projects
        #projects    = self.dbSession.query(ModelProject).all()
        # setting up the progress bar
        #pbar        = tqdm.tqdm(total=len(projects))
        # dictionary containing the agreement values as values keys are sessionNumbers
        agreementValues         = {}
        feedBackValues          = {}
        finalSolutions          = {}
        finalSolutionPositions  = {}
        sessionNums             = []
        # getting all the idea selections 
        ideaSelections    = self.dbSession.query(ModelIdeaSelection).all()
        #ideaSelections.pop()
        del ideaSelections[1280:1288]
        #for i in ideaSelections[-2000:]:
        for i in ideaSelections:
            # getting all the sessions related to the idea selection session
            sessionID  = i.sessionId
            session = self.dbSession.query(ModelSession).filter\
                        (ModelSession.id==sessionID).first()
            # getting the session numbers
            sessionNums.append(session.number)
            # summing all the agreement values of a session
            sumA = np.sum(list(i.agreementValues.values()))
            # storing the sum of aggreement value of that session
            agreementValues.setdefault(session.number,[]).append(sumA)
            # storing the feedback value for that session
            feedBackValues.setdefault(session.number,[]).append(i.feedbackValue)
            # storing the solution value for that session
            finalSolutions.setdefault(session.number,[]).append(i.finalSolution)
        # getting the unique session numbers
        sessions = np.unique(sessionNums)
        for sessionNum in sessions:
            # getting the error value for agreement values
            agreementErr    = stats.sem(agreementValues[sessionNum])
            # computing the average of the agreement values for all the same sessions
            agreement       = np.mean(agreementValues[sessionNum])
            # getting the error value for feedBackValues values
            feedBackValueErr    = stats.sem(feedBackValues[sessionNum])
            # computing the average of the feedBackValues values for all the same sessions
            feedBackValue       = np.mean(feedBackValues[sessionNum])
            # getting the error value for finalSolutions values
            finalSolutionErr    = stats.sem(finalSolutions[sessionNum])
            # computing the average of the finalSolutions values for all the same sessions
            finalSolution       = np.mean(finalSolutions[sessionNum])
            # storing the values
            record['SessionNumber'].append(sessionNum)
            record['AgreementValue'].append(agreement)
            record['FeedbackValue'].append(feedBackValue)
            record['FinalSolutionValue'].append(finalSolution)
            record['AgreementValueErr'].append(agreementErr)
            record['FeedbackValueErr'].append(feedBackValueErr)
            record['FinalSolutionValueErr'].append(finalSolutionErr)
        # converting the file to dataframe
        df = pd.DataFrame(data=record)
        # generating file name
        fileName    = newFolderPath + "/" + "5peak_4influ.csv"
        # exporting the file
        df.to_csv(fileName, index=False)
    #
    ## Method that pulls the data of positions of the final solution value from the database and stores them in a csv file
    def extractISExperiment3Dataset(self):
        print("\n\n")
        print("################################")
        print("Extracting Data for Idea Selection Experiment 3")
        print("################################")
        print("\n")
        # create folder for experiment
        experimentFolderPath    = EXP_ROOTFOLDERNAME_IS % (3)
        self.createFolder(experimentFolderPath)
        # creating the folder for this experiment export
        newFolderPath   = self.createExperimentFolder(experimentFolderPath)
        # init the result
        record      = {'ProjectId':[],'SessionNumber':[],'FinalSolutionValue':[],'PositionX':[],'PositionY':[]}
        # getting the projects
        projects    = self.dbSession.query(ModelProject).all()
        # removing the last prj as it was incomplete for 5 peaks 3 exp agents 
        projects.pop(128)
        # taking all the projects
        for project in projects:
            # getting all the sessions of that project
            for session in project.sessions:
                # getting idea selection of that session id
                ideaSelection    = self.dbSession.query(ModelIdeaSelection).filter\
                    (ModelIdeaSelection.sessionId==session.id).first()
                solutionValue   = ideaSelection.finalSolution
                positionX       = ideaSelection.finalSolutionPosition[0]
                positionY       = ideaSelection.finalSolutionPosition[1]
                # writing to dictionary
                record['ProjectId'].append(project.id)
                record['SessionNumber'].append(session.number)
                record['FinalSolutionValue'].append(solutionValue)
                record['PositionX'].append(positionX)
                record['PositionY'].append(positionY)
        # converting the file to dataframe
        df = pd.DataFrame(data=record)
        # generating file name
        fileName    = newFolderPath + "/" + "5peak_4influ.csv"
        # exporting the file
        df.to_csv(fileName, index=False)
    #
    ## Method that pulls the data of 3 and 1 solutions(maxvote agents),solutions and selected agents
    def extractISExperiment4Dataset(self):
        print("\n\n")
        print("################################")
        print("Extracting Data for Idea Selection Experiment 4")
        print("################################")
        print("\n")
        # create folder for experiment
        experimentFolderPath    = EXP_ROOTFOLDERNAME_IS % (4)
        self.createFolder(experimentFolderPath)
        # creating the folder for this experiment export
        newFolderPath   = self.createExperimentFolder(experimentFolderPath)
        # init the result
        record1      = {'SessionNumber':[],'FinalSolutionValue':[]}
        record3      = {'SessionNumber':[],'FinalSolutionValue':[]}
        # getting all the idea selections 
        ideaSelections    = self.dbSession.query(ModelIdeaSelection).all()
        #ideaSelections.pop()
        del ideaSelections[1280:1288]
        #for i in ideaSelections[-2000:]:
        for i in ideaSelections:
            if len(i.maxVotedAgents)>1:
                self.extractFinalValues(i,record3)
            else:
                self.extractFinalValues(i,record1)
        # converting the file to dataframe
        df1 = pd.DataFrame(data=record1)
        df3 = pd.DataFrame(data=record3)
        print('length 1',len(record1['FinalSolutionValue']))
        print('length 3',len(record3['FinalSolutionValue']))
        # generating file name
        fileName1    = newFolderPath + "/" + "1solutions_5peak_4influ.csv"
        fileName3    = newFolderPath + "/" + "3solutions_5peak_4influ.csv"
        # exporting the file
        df1.to_csv(fileName1, index=False)
        df3.to_csv(fileName3, index=False)
    #
    ## Method for calculating the contribution of the agents
    def extractISExperiment5Dataset(self):
        print("\n\n")
        print("################################")
        print("Extracting Data for Idea Selection Experiment 5")
        print("################################")
        print("\n")
        # getting the projects
        projects    = self.dbSession.query(ModelProject).all()
        # init list for storing distribution of contribution for all project
        allContribution =[]
        # taking all the projects
        for project in projects[-200:]:
            # init the distionary to store agent and the contribution values
            countMaxVotedAgent  = {}
            # init the list to store agent ids
            agentIDs            = []
            # getting all the sessions of that project
            for session in project.sessions:
                # getting idea selection of that session id
                ideaSelection    = self.dbSession.query(ModelIdeaSelection).filter\
                    (ModelIdeaSelection.sessionId==session.id).first()
                # getting all the agents of that session
                for agent in session.agents:
                    # adding agent id to the list
                    agentIDs.append(agent.id)
                    # checking 
                    if agent.id in ideaSelection.maxVotedAgents:
                        if agent.id in countMaxVotedAgent:
                            countMaxVotedAgent[agent.id] += 1
                        else:
                            countMaxVotedAgent[agent.id] = 1
                    else:
                        pass
            # getting the uniquie agent ids
            agentIDList = np.unique(agentIDs)
            for a in agentIDList:
                # checking if the agent ids do not exsits in maxVoted agent list
                if a not in countMaxVotedAgent:
                    # making the contribution of the non exsisted agent in the list to 0
                    countMaxVotedAgent[a]=0
            # calculating the standard deviation of the values of teh dictionary
            from numpy import array
            contributionDistribution = array([countMaxVotedAgent[k] for k in countMaxVotedAgent]).std()
            # adding to the list for that project id
            allContribution.append(contributionDistribution)
        # calculation the mean of contribution
        df = pd.DataFrame(data=allContribution)
        df.to_csv("contribution_allinflu_1peaks.csv", index=False)
        # calculation the mean of contribution
        #print('contribution distribution mean all influencer 5 steep peak',np.mean(allContribution))
        #print('contribution distribution sem  all influencer 5 steep peak ',stats.sem(allContribution))             
    #
    ## Method that extracts final solution values for a session
    def extractFinalValues(self,IdeaSelection,record):
        # getting all the sessions related to the idea selection session
        sessionID  = IdeaSelection.sessionId
        session = self.dbSession.query(ModelSession).filter\
             (ModelSession.id==sessionID).first()
        solutionValue   = IdeaSelection.finalSolution
        # writing to dictionary
        record['SessionNumber'].append(session.number)
        record['FinalSolutionValue'].append(solutionValue)
        return record
    #    
    ## Method that creates a folder if it doesn't exist
    def createFolder(self, folderPath):
        # checking if the folder exists
        if os.path.exists(folderPath):
            pass
        else:
            os.mkdir(folderPath)
    #
        ## Method that creates a new experiment folder
    # @param experimentFolder Base folder for the experiment
    # @return Path to new folder
    def createExperimentFolder(self, experimentFolder):
        # init loop variables
        result = False
        i = 0
        # starting to search an available folder
        while not result:
            # composing the filename
            newFolderPath   = experimentFolder+str(i)
            # checking if exists
            if os.path.exists(newFolderPath):
                # updating the counter
                i += 1
            else:
                result = True
        # creating the folder 
        self.createFolder(newFolderPath)
        # returning the path of the folder
        return newFolderPath
    #
if __name__ == '__main__':
    pass
    
                    