# standard imports
from sqlalchemy import Column, Integer, String, ForeignKey, Float, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property

# custom imports
#from models.base import Base, agents_sessions_table, agents_projects_table
from models.base import Base, agents_projects_table, decodeTable, encodeTable, generateUuid


## Class agent to map this table
class Agent(Base):
    # details about the table
    __tablename__   = 'agents'
    # datatypes of columns:
    ## Id column is a part of a primary key
    id              = Column(String, primary_key=True, default=generateUuid)
    ## name of the agent
    name            = Column(String)
    ## List of success of the agent
    successes       = relationship("Success", backref="agent", uselist=True)
    ## List of failure of the agent
    failures        = relationship("Failure", backref="agent", uselist=True)
    ## Idea Generation steps related to the agent
    iGSteps         = relationship("IdeaGenerationStep", backref="agent", uselist=True)
    # Defining relationship between agents and session
    sessions        = relationship("Session",secondary="agents_sessions_links", back_populates="agents", uselist=True, lazy="joined")
    # Defining relationship between agents and projects
    projects        = relationship("Project",secondary=agents_projects_table, back_populates="agents", uselist=True, lazy="joined")
    ## Method for formatted printing
    def __repr__(self):
        return "<Agent(id='%s'; name='%s')>" % (self.id,self.name)
    #

## Class that implements the relations among agents and sessions
class AgentCharacteristics(Base):
    ## Name of the table
    __tablename__   = "agents_sessions_links"
    ## id column is a part of a primary key 
    id              = Column(String, primary_key=True, default=generateUuid)
    ## Id of sessions involved in the link
    session_id      = Column(String, ForeignKey('sessions.id'))
    ## Id of agents involved in the link
    agent_id        = Column(String, ForeignKey('agents.id'))
    ## Experience of the agent in specific session
    experience      = Column(Integer)
    ## Self-efficacy of the agent in specific session
    selfEfficacy    = Column(Float)
    ## Trust of the agent for other agents in specific session
    _trust          = Column("trust", String)
    ## Familiarity of the agent for other agents in specific session
    _familiarity    = Column("familiarity",String)
    ## reputation of the agent in specific session
    reputation      = Column(Float)
    ## influencers for agent in specific session
    _influencersValue    = Column("influencersValue", String)
    ## Method for formatted printing
    def __repr__(self):
        return "<AgentCharacteristics(id='%s'; agent_id='%s'; session_id='%s'; experience='%d')>" % (self.id,self.agent_id, self.session_id, self.experience)
    #
    ############################################################
    #   TRUST METHODS
    @hybrid_property
    def trust(self):
        return decodeTable(self._trust)
    #
    @trust.setter
    def trust(self, newTrust):
        self._trust = encodeTable(newTrust)
    #
    ############################################################
    #   FAMILIARITY METHODS
    @hybrid_property
    def familiarity(self):
        return decodeTable(self._familiarity)
    #
    @familiarity.setter
    def familiarity(self, newFamiliarity):
        self._familiarity = encodeTable(newFamiliarity)
    #
    ############################################################
    #   INFLUENCERS METHODS
    @hybrid_property
    def influencersValue(self):
        return decodeTable(self._influencersValue)
    #
    @influencersValue.setter
    def influencersValue(self, newInfluencersValue):
        self._influencersValue = encodeTable(newInfluencersValue)
    #
    ############################################################



if __name__ == "__main__":
    pass
