# standard imports
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, ForeignKey, Table
import numpy as np
import json
import ast
import uuid
import os

from ast import literal_eval


# define database name
#databaseName = 'project_%d.db' % (os.getpid())
#databaseName = 'project_5peak_1exp.db'
databaseName = 'all_influencers 200 1 peak.db'


# declare base to  define any number of mapped classes
engine  = create_engine("sqlite:///" + databaseName, echo=False)
Base    = declarative_base()



# creating a database session
DatabaseSession = sessionmaker(bind=engine, autoflush=False)
#databaseSession = DatabaseSession()
databaseSession = scoped_session(DatabaseSession)


## Function that generates a UUID
# @return UUID4 string
def generateUuid():
    # generate UUID
    _id = uuid.uuid4()
    # return the string
    return str(_id)

## Function that decodes a string into a pair of coordinates
# @param string String with pair of coordinates encoded
# @return array with the decoded coordinates
def decodeCoordinates(string):
    # evaluating the string
    coordinates     = literal_eval(string)
    # evaluating the string in a  list
    coordinatesArray       = np.array(coordinates)
    # returning the list of values
    return coordinatesArray

## Function that encodes a pair of coordinates to string
# @param coordinates Array of coordinates to be encoded
# @return String containing encoded coordinates
def encodeCoordinates(coordinates):
    # encoding the coordinates into string
    newString       = "(%.3f,%.3f)"%(coordinates[0],coordinates[1])
    # returning the result
    return newString

## Function that decodes a string into an array (list)
# @param string String with some agent ID encoded
# @return array with the decoded coordinates
def decodeIDs(string):
    # decode and return 
    return json.loads(string)

## Function that encodes a pair of coordinates to string
# @param coordinates Array of coordinates to be encoded
# @return String containing encoded coordinates
def encodeIDs(oldList):
    # encoding the coordinates into string
    return json.dumps(oldList)

## Function that decodes a string into a dictionary
# @param string String with dictionary pairs (keys and values)
# @return dictionary with the decoded dictionary pairs
###################### DECODE AND ENCODE ARE USED ONLY WHILE DOING ANALYTICS AND NOT DURING RUNTIME ######################
def decodeTable(string):
    # converting the string to dictionary
    dictionary       = json.loads(string)
    # returning dictionary
    return dictionary

## Function that encodes a pair of dictionary elements (key and values) to string
# @param oldTrust dictionary
# @return String containing encoded dictionary
def encodeTable(oldDictionary):
    # making a new dictionary for storing key as agent id and trust values
    #newDictionary    = {}
    # converting the dictionary to a string
    newString       = json.dumps(oldDictionary)
    # returning the result
    return newString

## Association table for recalling successes used by Success and Session Tables
recalledSuccesses_sessions_table = Table('recalledSuccesses_sessions', Base.metadata,
    Column('session_id', String, ForeignKey('sessions.id')),
    Column('success_id', String, ForeignKey('successes.id')),
)

## Association table for recalling failures used by Failure and Session Tables
recalledFailures_sessions_table = Table('recalledFailures_sessions', Base.metadata,
    Column('session_id', String, ForeignKey('sessions.id')),
    Column('failure_id', String, ForeignKey('failures.id')),
)

## Association table to agents in sessions
agents_projects_table = Table('agents_projects', Base.metadata,
    Column('project_id', String, ForeignKey('projects.id')),
    Column('agent_id', String, ForeignKey('agents.id')),
)

if __name__ == "__main__":
    pass
