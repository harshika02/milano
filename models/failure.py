# standard imports
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import backref


# custom imports
from abm_simulation.utils import _print
from models.base import Base, decodeCoordinates, encodeCoordinates, recalledFailures_sessions_table, generateUuid


## Class that implements a failure
class Failure(Base):
    # details about the table
    __tablename__   = 'failures'
    #__table_args__  = {'extend_existing': True} 
    ## id column is a part of a primary key 
    id              = Column(String, primary_key=True, default=generateUuid)
    ## Position of the failure
    _position       = Column("position", String)
    ## Session at which the failure was created
    createdSession_id = Column(String, ForeignKey('sessions.id'))
    ## Session at which the failure was forgotten
    forgottenSession_id = Column(String, ForeignKey('sessions.id'), nullable=True)
    ## Id of the agent to which the failure is related
    agentId         = Column(String, ForeignKey('agents.id'))
    ## radius of the failure circle
    radius          = Column("Failure radius", Integer)
    #
    # Defining relationship
    ## defining relationship between failure and session when it was created
    createdSession      = relationship("Session", foreign_keys=[createdSession_id],backref=backref('createdFailure'))
    ## defining relationship between failure and session when it was forgotten
    forgottenSession    = relationship("Session", foreign_keys=[forgottenSession_id],backref=backref('forgottenFailure'))
    ## defining relationship between failures and session when they were recalled
    recalledSession     = relationship("Session",secondary=recalledFailures_sessions_table,back_populates="recalledFailures",uselist=True)
    
    ## Method for formatted printing
    def __repr__(self):
        return "<Failure(id='%s'; position='%s'; radius='%d')>" % (self.id, str(self.position),self.radius)
    #
    ############################################################
    #   POSITION METHODS
    @hybrid_property
    def position(self):
        return decodeCoordinates(self._position)
    #
    @position.setter
    def position(self, newPosition):
        self._position = encodeCoordinates(newPosition)
    #



if __name__ == "__main__":
    pass

