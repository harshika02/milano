# standard imports


# model imports
from models.base import Base, decodeCoordinates, encodeCoordinates, engine
from models.agent import Agent
from models.success import Success
from models.project import Project
from models.session import Session
from models.steps import IdeaGenerationStep
from models.failure import Failure
from models.selection import IdeaSelection


# Test enabler
TEST_ENABLED = False

# creating the objects
Base.metadata.create_all(engine)


## Function that creates the database
def createDatabase():
    # create the session
    from sqlalchemy.orm import sessionmaker
    dbSession   = sessionmaker(bind=engine)
    session     = dbSession()
    # commit session
    session.commit()
    # flush session
    session.flush()


if __name__ == "__main__":
    createDatabase()