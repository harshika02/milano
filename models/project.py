# standard imports
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property

import pickle

# custom imports
from abm_simulation.utils import _print
from models.base import Base, decodeCoordinates, encodeCoordinates, agents_projects_table, generateUuid


## Class that implements the project item
class Project(Base):
    # details about the table
    __tablename__   = 'projects'
    ## id column is a part of a primary key 
    id              = Column(String, primary_key=True, default=generateUuid)
    ## Project number
    number          = Column(Integer)
    ## solution space
    _solutionSpace  = Column("solutionSpace", String)
    ## sessions related to the project
    sessions        = relationship("Session", backref="project", uselist=True)
    ## agents included in the projects
    agents          = relationship("Agent",secondary=agents_projects_table, back_populates="projects", uselist=True)
    #
    ## Method for formatted printing
    def __repr__(self):
        return "<Project(id='%s'; number='%d')>" % (self.id, self.number)
    #
    ############################################################
    #   SOLUTION SPACE
    @hybrid_property
    def solutionSpace(self):
        return pickle.loads(self._solutionSpace)
    #
    @solutionSpace.setter
    def solutionSpace(self, matrix):
        self._solutionSpace = pickle.dumps(matrix)
    #



if __name__ == "__main__":
    pass