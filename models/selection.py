# standard imports
import numpy as np
from sqlalchemy import Column, Integer, String, ForeignKey, Float
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property



# changing base directory
if __name__ == "__main__":
    import os
    import sys
    # adding the current path to the system path
    if not os.getcwd() in sys.path:
        sys.path.append(os.getcwd())

# custom imports
from abm_simulation.utils import _print
from models.base import * 


## Class Idea Generation Step to map this table
class IdeaSelection(Base):
    # details about the table
    __tablename__   = 'idea_selection'
    # datatypes of columns:
    ## id column is a part of a primary key 
    id              = Column(String, primary_key=True, default=generateUuid)
    ## Id of the related session
    sessionId   = Column(String, ForeignKey('sessions.id'))
    ## agreement value on the selected solutions
    _agreementValues= Column('selected agents agreement values',String)
    ## feedback value by the controller agent
    feedbackValue   = Column(Float)
    ## final solution value by the team
    finalSolution   = Column(Float)
    ## final solution position by the team
    _finalSolutionPosition  = Column("finalSolutionPosition", String)
    ## max Voted agents
    _maxVotedAgents   = Column("maxVotedAgents",String)
    ## Idea Selection related to the session one to one relationship
    #session = relationship("Session", back_populates="idea_selection")
    # Method for formatted printing
    def __repr__(self):
        return "<idea_selection(id='%s'; session_id='%s';\
                     agreementValues='%s';feedbackValue='%d')>" %(
                         self.id,
                         self.sessionId,
                         str(self.agreementValues),
                         self.feedbackValue
                         )
    #
    ############################################################
    #   AGREEMENT VALUE METHODS
    @hybrid_property
    def agreementValues(self):
        return decodeTable(self._agreementValues)
    #
    @agreementValues.setter
    def agreementValues(self,newAgreementValues):
        self._agreementValues = encodeTable(newAgreementValues)
    #
     ############################################################
    #   MAX VOTED AGENTS IDS METHODS
    @hybrid_property
    def maxVotedAgents(self):
        return np.array(decodeIDs(self._maxVotedAgents))
    #
    @maxVotedAgents.setter
    def maxVotedAgents(self,newMaxVotedAgents):
        self._maxVotedAgents = encodeIDs(newMaxVotedAgents)
    #
    #
    ############################################################
    #   FINAL SOLUTION POSITION METHODS
    @hybrid_property
    def finalSolutionPosition(self):
        # decoding string
        positionTuple   = decodeCoordinates(self._finalSolutionPosition)
        # casting to int
        return np.array(positionTuple, dtype=int)
    #
    @finalSolutionPosition.setter
    def finalSolutionPosition(self, newPosition):
        self._finalSolutionPosition= encodeCoordinates(newPosition)
    #



if __name__ == "__main__":
    pass



