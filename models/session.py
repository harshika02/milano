# standard imports
from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship



# changing base directory
if __name__ == "__main__":
    import os
    import sys
    # adding the current path to the system path
    if not os.getcwd() in sys.path:
        sys.path.append(os.getcwd())

# custom imports
from abm_simulation.utils import _print
from models.base import *


## Class that implements the Session item
class Session(Base):
    # details about the table
    __tablename__   = 'sessions'
    ## id column is a part of a primary key 
    id              = Column(String, primary_key=True, default=generateUuid)
    ## Number of the session
    number          = Column(Integer)
    ## Id of the related project
    projectId       = Column(String, ForeignKey('projects.id'), nullable=True)
    ## Idea Generation steps related to the session
    iGSteps         = relationship("IdeaGenerationStep", backref="sessions", uselist=True)
    # Defining relationship between recalled successes and session
    recalledSuccesses   = relationship("Success",secondary=recalledSuccesses_sessions_table, back_populates="recalledSession", uselist=True)
    # Defining relationship between recalled failures and session
    recalledFailures    = relationship("Failure",secondary=recalledFailures_sessions_table, back_populates="recalledSession", uselist=True)
    # Defining relationship between agents and session
    agents              = relationship("Agent",secondary="agents_sessions_links", back_populates="sessions", uselist=True)
    #  Idea Selection related to the session
    ideaSelectionActivity         = relationship("IdeaSelection", backref="sessions", uselist=True)
    ## Method for formatted printing
    def __repr__(self):
        return "<Session(id='%s'; number='%d')>" % (self.id, self.number)
    #



if __name__ == "__main__":
    pass
