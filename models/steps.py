# standard imports
import numpy as np
from sqlalchemy import Column, Integer, String, ForeignKey, Float
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property



# changing base directory
if __name__ == "__main__":
    import os
    import sys
    # adding the current path to the system path
    if not os.getcwd() in sys.path:
        sys.path.append(os.getcwd())

# custom imports
from abm_simulation.utils import _print
from models.base import *


## Class Idea Generation Step to map this table
class IdeaGenerationStep(Base):
    # details about the table
    __tablename__   = 'idea_generation_steps'
    # datatypes of columns:
    ## id column is a part of a primary key 
    id              = Column(String, primary_key=True, default=generateUuid)
    ## step number of IG activity
    number          = Column(Integer)
    ## position attained after step
    _position       = Column("positionWithAllLearning", String)
    ## Intended position after the step
    _intendedPosition   = Column("positionWithoutLearning", String)
    ## position attained after success bias step
    _positionSB     = Column("positionExpeienceLearning", String)
    ## step size of IG activity
    _actualStep         = Column("finalStep", String)
    ## step only with success bias of IG activity 
    _successBiasStep    =  Column("expLearningStep", String)
    ## intended step (without biases) of IG activity
    _intendedStep        = Column("withoutLearningStep", String)
    ## Bias result of the influence of previous successes
    _successesBias       = Column("experienceLearning", String)
    ## Bias result of the influencer impact
    _sumInfluencersBias  = Column("socialLearning", String)
    ## Id of the related session
    sessionId       = Column(String, ForeignKey('sessions.id'))
    ## Id of the related agents
    agentId         = Column(String, ForeignKey('agents.id'))
    #
    # Method for formatted printing
    def __repr__(self):
        return "<Step(id='%s'; number='%d'; finalStep='%s')>" % (
            self.id,
            self.number,
            str(self.actualStep)
        )
    #
    ############################################################
    #   POSITION METHODS
    @hybrid_property
    def position(self):
        # decoding string
        positionTuple   = decodeCoordinates(self._position)
        # casting to int
        return np.array(positionTuple, dtype=int)
    #
    @position.setter
    def position(self, newPosition):
        self._position = encodeCoordinates(newPosition)
    #
    ############################################################
    #   INTENDED POSITION METHODS
    @hybrid_property
    def intendedPosition(self):
        # decoding string
        positionTuple   = decodeCoordinates(self._intendedPosition)
        # casting to int
        return np.array(positionTuple, dtype=int)
    #
    @intendedPosition.setter
    def intendedPosition(self, newPosition):
        self._intendedPosition = encodeCoordinates(newPosition)
    #
    ############################################################
    #   POSITION FROM SUCCESS BIAS METHODS
    @hybrid_property
    def positionSB(self):
        # decoding string
        positionTuple   = decodeCoordinates(self._positionSB)
        # casting to int
        return np.array(positionTuple, dtype=int)
    #
    @positionSB.setter
    def positionSB(self, newPosition):
        self._positionSB = encodeCoordinates(newPosition)
    #
    ############################################################
    #   VECTOR METHODS
    @hybrid_property
    def actualStep(self):
        return decodeCoordinates(self._actualStep)
    #
    @actualStep.setter
    def actualStep(self, position):
        self._actualStep = encodeCoordinates(position)
    #
    ############################################################
     #   SUCCESS STEP METHODS
    @hybrid_property
    def successBiasStep(self):
        return decodeCoordinates(self._successBiasStep)
    #
    @successBiasStep.setter
    def successBiasStep(self, position):
        self._successBiasStep = encodeCoordinates(position)
    #
    ############################################################
    #   INTENDED STEP METHODS
    @hybrid_property
    def intendedStep(self):
        return decodeCoordinates(self._intendedStep)
    #
    @intendedStep.setter
    def intendedStep(self, position):
        self._intendedStep = encodeCoordinates(position)
    #
    ############################################################
    #   SUCCESS BIAS METHODS
    @hybrid_property
    def successesBias(self):
        return decodeCoordinates(self._successesBias)
    #
    @successesBias.setter
    def successesBias(self, position):
        self._successesBias = encodeCoordinates(position)
    #
    ############################################################
    #   INFLUENCER BIAS METHODS
    @hybrid_property
    def sumInfluencersBias(self):
        return decodeCoordinates(self._sumInfluencersBias)
    #
    @sumInfluencersBias.setter
    def sumInfluencersBias(self, position):
        self._sumInfluencersBias = encodeCoordinates(position)
    #



if __name__ == "__main__":
    pass