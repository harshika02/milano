# standard imports
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import backref


# changing base directory
if __name__ == "__main__":
    import os
    import sys
    # adding the current path to the system path
    if not os.getcwd() in sys.path:
        sys.path.append(os.getcwd())

# custom imports
from abm_simulation.utils import _print
from models.base import *



## Class that implements a success
class Success(Base):
    # details about the table
    __tablename__   = 'successes'
    ## id column is a part of a primary key 
    id              = Column(String, primary_key=True, default=generateUuid)
    ## Position of the success
    _position       = Column("position", String)
    ## Session at which the success was created
    createdSession_id = Column(String, ForeignKey('sessions.id'))
    ## Session at which the success was forgotten
    forgottenSession_id = Column(String, ForeignKey('sessions.id'), nullable=True)
    ## Id of the agent to which the success is related
    agentId             = Column(String, ForeignKey('agents.id'))
    #
    ## defining relationship between success and session when it was created
    createdSession      = relationship("Session", foreign_keys=[createdSession_id], backref=backref('createdSuccess'))
    ## defining relationship between success and session when it was forgotten
    forgottenSession    = relationship("Session", foreign_keys=[forgottenSession_id], backref=backref('forgottenSuccess'))
    ## defining relationship between successes and session when they were recalled
    recalledSession     = relationship("Session", secondary=recalledSuccesses_sessions_table, back_populates="recalledSuccesses", uselist=True)
    ## Method for formatted printing
    def __repr__(self):
        return "<Success(id='%s'; position='%s')>" % (self.id, str(self.position))
    #
    ############################################################
    #   POSITION METHODS
    @hybrid_property
    def position(self):
        return decodeCoordinates(self._position)
    #
    @position.setter
    def position(self, newPosition):
        self._position = encodeCoordinates(newPosition)
    #



if __name__ == "__main__":
    pass
